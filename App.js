import 'react-native-gesture-handler';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import SplashScreen from './component/screens/SplashScreen';
import LanguageScreen from './component/screens/LanguageScreen';
import MainScreen from './component/screens/MainScreen';
import VendorMainScreen from './component/screens/VendorMainScreen';
import ChooseUserVender from './component/screens/ChooseUserVender';
import LoginScreen from './component/screens/LoginScreen';
import OtpScreen from './component/screens/OtpScreen';
import VendorEditProfileScreen from './component/screens/VendorEditProfileScreen';
import React, { Component } from 'react';
import style from './component/utils/style';
import StringsOfLanguages from './component/utils/StringsOfLanguages';
import { View, Image } from 'react-native';
import firebase from '@react-native-firebase/app';
import { Platform } from 'react-native';

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
        }
    }
    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }

    render() {
        var initialRoutNam = 'SplashScreen';
        try {
            initialRoutNam = this.props.navigation.getParam('initialScreen', 'SplashScreen');
        } catch (error) {
            // console.log(error)
        }

        const MainStack = createStackNavigator({
            SplashScreen: {
                screen: SplashScreen, navigationOptions: {
                    headerShown: false,
                }
            },
            LanguageScreen: {
                screen: LanguageScreen, navigationOptions: {
                    headerShown: false,
                }
            },
            MainScreen: {
                screen: MainScreen, navigationOptions: {
                    headerShown: false,
                }
            },
            VendorMainScreen: {
                screen: VendorMainScreen, navigationOptions: {
                    headerShown: false,
                }
            },
            ChooseUserVender: {
                screen: ChooseUserVender, navigationOptions: {
                    headerShown: false,
                }
            },
            LoginScreen: {
                screen: LoginScreen, navigationOptions: {
                    headerShown: false,
                }
            },
            OtpScreen: {
                screen: OtpScreen, navigationOptions: {
                    headerShown: false,
                }
            },
            VendorEditProfileScreen: {
                screen: VendorEditProfileScreen, navigationOptions: {
                    headerShown: false,
                }
            },
        }, {
            headerMode: 'none',
            initialRouteName: initialRoutNam,
        });
        const MyApp = createAppContainer(MainStack);


        return (

            <View style={{ flex: 1 }}>
                <MyApp />
            </View>
        );
    }

}


