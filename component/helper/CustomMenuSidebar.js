import React, { Component } from 'react';
import { TouchableOpacity, View, Text, ScrollView, Image, Alert, ImageBackground } from 'react-native';
import { DrawerItems } from 'react-navigation-drawer';
import CommanUtils from '../utils/CommanUtils';
import style from '../utils/style';
import CardView from 'react-native-cardview'
import StringsOfLanguages from '../utils/StringsOfLanguages'
import Icon from 'react-native-vector-icons/Feather';
import { NavigationActions, StackActions } from 'react-navigation';

import colors from '../utils/colors';
import GeneralStatusBarColor from "react-native-general-statusbar";
import diamens from '../utils/diamens';

var header_bg = require('../../images/header_bg.png');
var bgImage = require('../../images/main_bg.png');
var logoutImage = require('../../images/logout.png');


export default class DrawerContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      languag: true,
      name: '',
      email: '',
      profile_pic: '',
    }
  }

  async componentDidMount() {
    var lan = await CommanUtils.getLanguage()
    this.setState({ languag: lan, });

    this.showDataFromLocal()
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      this.showDataFromLocal()
    });
  }
  async showDataFromLocal() {
    var dataFromLocal = await CommanUtils.getLoginDetails();
    this.setState({
      name: dataFromLocal.name,
      email: dataFromLocal.email,
      profile_pic: dataFromLocal.profile_pic,
    });
  }
  navigateToPrivacyPolicy() {
    this.props.navigation.navigate("WebViewScreen", {
      title: StringsOfLanguages.privacyPolicy,
      id: 9,
    })
  }
  navigateToTermsConditions() {
    this.props.navigation.navigate("WebViewScreen", {
      title: StringsOfLanguages.termsAndCondition,
      id: 8,
    })

  }

  navigateToProfile() {
    this.props.navigation.navigate("EditProfileScreen");
    this.props.navigation.closeDrawer()
  }

  navigateMainDrawer() {
    const loginScreenAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'MainScreen', params: { screen: 'MainScreen' } }),
      ],
    });
    this.props.navigation.dispatch(loginScreenAction)
  }

  showLogoutAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: StringsOfLanguages.cancel,
          onPress: () =>
            console.log('Cancel Pressed'),
        },
        {
          text: StringsOfLanguages.logout,
          onPress: () => {
            this.gotoLogin()
            CommanUtils.saveLoginStatus(false);
          }
        }
      ],
      { cancelable: false }
    );

  }

  gotoLogin() {
    const loginScreenAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'App', params: { 'initialScreen': 'ChooseUserVender' } }),
      ],
    });
    this.props.navigation.dispatch(loginScreenAction)
  }
  render() {
    let styles;
    const { languag, name, email, profile_pic } = this.state;

    if (languag) {
      styles = style.getStyle(true);
      StringsOfLanguages.setLanguage("en")
    } else {
      styles = style.getStyle(false);
      StringsOfLanguages.setLanguage("ar")
    }


    return (
      <View
        style={{ flex: 1, }}
        forceInset={{ top: 'always', horizontal: 'never' }}
      >
        <GeneralStatusBarColor
          backgroundColor={colors.statusColor}
          barStyle="light-content"
        />
        <ImageBackground style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#4384FF' }} source={header_bg}>
          <TouchableOpacity
            onPress={() => {
              this.navigateToProfile();
            }}>
            <View style={[styles.profileImgBackground, { marginTop: 20 }]}>
              <Image source={{ uri: profile_pic }} style={[styles.profileImg,]} />
            </View>
          </TouchableOpacity>
          <Text style={{  marginBottom: 10,fontFamily: 'SFUIText-Semibold', fontSize: 16, marginTop: 10, color: 'white' }}>{name}</Text>
          {/* <Text style={{ color: "gray", marginBottom: 10, color: 'white' }}>{email}</Text> */}
        </ImageBackground>
        <ImageBackground style={{ flex: 1 }} source={bgImage}>

          <ScrollView style={{flex:1}}>

            <View style={{ flex: 1 }}>
              <DrawerItems {...this.props} />

              <TouchableOpacity style={{}}
                onPress={() => { this.showLogoutAlert(StringsOfLanguages.alert, StringsOfLanguages.areSureWantToLogout); }}
              >
                <View style={[styles.flexDirect, { paddingTop: 10, paddingBottom: 10, paddingEnd: 20, paddingStart: 20, }]}>
                  <Image source={logoutImage} style={{ marginStart: 0, width: 20, height: 20, tintColor: '#4384FF', resizeMode: 'contain' }} />
                  <Text style={{ marginStart: 20, marginEnd: 30,fontFamily: 'SFUIText-Semibold', }}>{StringsOfLanguages.signOut}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}


