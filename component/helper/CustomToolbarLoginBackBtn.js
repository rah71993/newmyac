import 'react-native-gesture-handler';
import React from 'react';
import { Text, View, ImageBackground, TouchableOpacity, Image } from 'react-native';
import { withNavigation } from 'react-navigation';
import style from '../utils/style';
import colors from '../utils/colors';
import CommanUtils from '../utils/CommanUtils';
import GeneralStatusBarColor from "react-native-general-statusbar";
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';

var header_bg = require('../../images/header_bg.png');

class CustomToolbarBackBtn extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loginType: "",
        }
    }



    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var type = await CommanUtils.getLoginType();
        this.setState({ languag: lan, loginType: type });

        this.showNotificatCount()
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.showNotificatCount()
        });
    }
    async showNotificatCount() {
        var notificationCount = await CommanUtils.getNotificatCount()
        this.setState({ notificationCount: notificationCount });
    }


    async navigateToNotificationClick() {
        this.props.navigation.navigate("NotificationScreen")
    }

    render() {
        let styles;
        let backIcon;
        const { languag } = this.state;

        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            backIcon = require('../../images/back.png');
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
            backIcon = require('../../images/right-arrow.png');
        }


        return (
            <View>

                <GeneralStatusBarColor
                    backgroundColor={colors.statusColor}
                    barStyle="light-content"
                />

                <ImageBackground style={styles.navBar} source={header_bg}>

                    <View style={styles.leftContainer}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}>
                            <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={backIcon}
                                    style={[styles.ImageIconStyle], { height: 15, width: 20, tintColor: 'white' }}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <Text numberOfLines={1} style={[styles.middleContainer, { fontFamily: 'SFUIText-Regular', fontSize: diamens.toolbarTitleSize, color: 'white' }]}>
                        {`${this.props.valueFromParent}`}
                    </Text>


                </ImageBackground>
            </View>
        );
    }
}
export default withNavigation(CustomToolbarBackBtn);
