import React from 'react';
import { Text, View, ImageBackground, TouchableOpacity, Image } from 'react-native';
import { withNavigation } from 'react-navigation';
import style from '../utils/style';
import colors from '../utils/colors';
import CommanUtils from '../utils/CommanUtils';
import GeneralStatusBarColor from "react-native-general-statusbar";
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';

var iconBell = require('../../images/bell.png');
var iconMenu = require('../../images/menu.png');
var header_bg = require('../../images/header_bg.png');
var down_arrow = require('../../images/drop_down.png');

class CustomToolbar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loginType: "",
            latitude: "",
            longitude: "",
        }
    }


    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var loginTyp = await CommanUtils.getLoginType()

        this.setState({ languag: lan, loginType: loginTyp });

        this.showNotificatCount()
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.showNotificatCount()
        });
    }
    async showNotificatCount() {
        var notificationCount = await CommanUtils.getNotificatCount()
        this.setState({ notificationCount: notificationCount });
    }
    async navigateToNotificationClick() {
        this.props.navigation.navigate("NotificationScreen")
    }

    navigateToMap() {
        this.props.navigation.navigate("ChooseAdressScreen", { title: this.props.valueFromParent, latitude: this.props.latitude, longitude: this.props.longitude })
    }

    render() {
        let styles;
        let notificationView;
        const { languag, notificationCount } = this.state;

        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            // alert("english")
        } else {
            // alert("arabic "+this.state.languag)
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (notificationCount > 0) {
            notificationView = (
                <View style={[styles.rightContainer, {}]}
                    onStartShouldSetResponder={() => {
                        this.navigateToNotificationClick()
                    }}>
                    <View style={{ resizeMode: 'contain' }}>
                        <Image source={iconBell}
                            style={[styles.ImageIconStyle], { height: 20, width: 15 }}
                        />
                    </View>
                    <View style={languag? styles.notifBagEnglish:styles.notifBagArabic}>
                        {/* <Text style={{ color: colors.white, fontSize: diamens.tinySize,fontFamily: 'SFUIText-Bold',}}>{notificationCount}</Text> */}
                    </View>
                </View>
            )
        } else {
            notificationView = (
                <View style={[styles.rightContainer, {}]}
                    onStartShouldSetResponder={() => {
                        this.navigateToNotificationClick()
                    }}>
                    <View style={{ resizeMode: 'contain' }}>
                        <Image source={iconBell}
                            style={[styles.ImageIconStyle], { height: 20, width: 15 }}
                        />
                    </View>
                </View>
            )
        }

        return (
            <View>


                <GeneralStatusBarColor
                    backgroundColor={colors.statusColor}
                    barStyle="light-content"
                />

                <ImageBackground style={styles.navBar} source={header_bg}>

                    <View style={styles.leftContainer}>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.openDrawer()
                            }
                            }>
                            <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={iconMenu}
                                    style={[styles.ImageIconStyle], { height: 15, width: 20 }}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={[styles.flexDirect, { flex: 1, alignItems: 'center' }]}
                        onStartShouldSetResponder={
                            () => this.navigateToMap()
                        }>
                        <Text numberOfLines={1} style={[styles.middleContainer, { fontFamily: 'SFUIText-Regular',fontSize:diamens.toolbarTitleSize,color: 'white' }]}> {`${this.props.valueFromParent}`}</Text>
                        <Image source={down_arrow} style={[styles.ImageIconStyle], { height: 15, width: 15, tintColor: 'white', marginRight: 30 }} />
                    </View>

                    {notificationView}

                </ImageBackground>
            </View>
        );
    }
}
export default withNavigation(CustomToolbar);
