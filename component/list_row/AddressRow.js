import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';
var plus = require('../../images/delete.png');


const ServiceType = ({ item, right_icon, styles, props ,click}) => (

    <View>

        <View style={[styles.flexDirect, {marginTop:10,marginBottom:10, justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
            <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{item.houseNo} {item.address}</Text>

            <TouchableOpacity onPress={() => { click(item) }}>
                <CardView
                    style={{ backgroundColor: '#fff', height: 25, width: 25 }}
                    cardElevation={2}
                    cardMaxElevation={2}
                    cornerRadius={12}>
                    <View style={[styles.flexDirect, { padding: 0, flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
                        <Image source={plus} style={{tintColor:colors.blue, height: 12, width: 12, }} />
                    </View>
                </CardView>
            </TouchableOpacity>
        </View>

    </View>
);

export default ServiceType;