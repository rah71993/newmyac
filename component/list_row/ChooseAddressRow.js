import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';
var plus = require('../../images/delete.png');
import { CheckBox } from 'react-native-elements'


const ServiceType = ({ item,state, right_icon, styles, props }) => {

    return (
        <View>

            <TouchableOpacity style={{ marginTop: 0 }}
                onPress={() => {
                    props.navigation.navigate("Welcome", {
                        latitude: item.latitude,
                        longitude: item.longitude,
                        title: item.address,
                    })
                }}>
                <View style={[styles.flexDirect, { marginTop: 10, marginBottom: 10, justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <CheckBox
                        center
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        containerStyle={{ backgroundColor: 'transparent', padding: 0 }}
                    />
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.ternarySize }]}>{item.address}</Text>
                </View>
            </TouchableOpacity>

        </View>
    );
}

export default ServiceType;