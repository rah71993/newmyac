import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import diamens from '../utils/diamens';
import dimens, { tinySize } from '../utils/diamens';
import StringsOfLanguages from '../utils/StringsOfLanguages'

const VendorQuotesRow = ({ item, styles, langu, props }) => {
    if (langu) {
        StringsOfLanguages.setLanguage("en")
    } else {
        StringsOfLanguages.setLanguage("ar")
    }

    let paymentStatus;

    if (item.paymentStatus === "Pending") {
        paymentStatus = (
            <View></View>
        );
    } else {
        paymentStatus = (
            <View style={[langu ? styles.scheduleLeft : styles.scheduleRight]}>
                <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 10 }}>{StringsOfLanguages.paymentReceived}</Text>
            </View>
        );
    }

    return (

        <View style={[styles.row_container, {}]}>

            <View style={styles.flexDirect}>
                {paymentStatus}
                <Text style={[styles.textAlignnEnd, { flex: 1, fontFamily: 'SFUIText-Bold', color: '#4384FF', fontSize: 25, paddingEnd: 20, paddingStart: 20 }]}>SAR {item.totalAmount}</Text>
            </View>

            <View style={styles.flexDirect}>
                <Image source={{ uri: item.profileImage }} style={{ height: 60, width: 60, borderRadius: 30 }} />

                <View style={{ flex: 1 }}>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', marginLeft: 10, marginEnd: 10 }]}>{item.vendorName}</Text>

                    <View style={[styles.flexDirect, { marginTop: 5 }]}>
                        <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                            <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular',fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.serviceType} :</Text>
                            <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}>{item.serviceType}</Text>
                        </View>

                        <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                            <Text numberOfLines={1} style={[styles.textAlignn, {fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.typeOfAc} : </Text>
                            <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}> {item.acType} </Text>
                        </View>

                        <View style={{ marginLeft: 0, marginRight: 0, flex: 1, alignItems: 'center' }}>
                            <Text numberOfLines={1} style={[styles.textAlignn, {fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                            <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}> {item.noOfAc} </Text>
                        </View>


                    </View>


                </View>

            </View>

            <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: '40%', alignSelf: 'center' }]}
                onPress={() => {
                    // alert('Alert with one button');
                    props.navigation.navigate("PaymentDash", {
                        vendorServiceId: item.vendorServiceId,
                        invoiceId: item.invoiceId,
                        scheduleDate: item.scheduleDate,
                        totalAmount: item.totalAmount,
                        serviceType: item.serviceType,
                        additionalPrice: item.additionalPrice,
                        paymentStatus: item.paymentStatus,
                        vendorName: item.vendorName,
                        priceQuote: item.priceQuote,
                        acType: item.acType,
                        noOfAc: item.noOfAc,
                        profileImage: item.profileImage,
                        vendorId: item.vendorId,


                    });
                }}>
                <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 12 }}>{StringsOfLanguages.viewInvoiceCap}</Text>
            </TouchableOpacity>

        </View>
    );
}

export default VendorQuotesRow;

