import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'


const ServiceType = ({ item, right_icon, styles, props }) => (

    <View>
        <TouchableOpacity
            onPress={() => {
                props.navigation.navigate("WebViewScreen", {
                    title: item.title,
                    id: item.id,
                })
            }}>
            <CardView
                style={{ backgroundColor: '#fff', marginEnd: 20, marginStart: 20, marginTop: 20, marginBottom: 1, paddingStart: 10, paddingEnd: 10 }}
                cardElevation={2}
                cardMaxElevation={2}
                cornerRadius={5}>
                <View style={[styles.flexDirect, { padding: 10 }]}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Text style={[styles.textAlignn, { color: '#000', marginBottom: 15, marginTop: 15, fontSize: 16, fontFamily: 'SFUIText-Bold', }]}>{item.title}</Text>
                    </View>
                    <Image source={right_icon} style={{ height: 15, width: 15, alignSelf: 'center' }} />
                </View>
            </CardView>
        </TouchableOpacity>
    </View>
);

export default ServiceType;