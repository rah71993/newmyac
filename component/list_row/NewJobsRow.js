import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import colors from '../utils/colors';
import diamens, { tinySize } from '../utils/diamens';
import StringsOfLanguages from '../utils/StringsOfLanguages'

const VendorQuotesRow = ({ item, styles, langu, props }) => {
    if (langu) {
        StringsOfLanguages.setLanguage("en")
    } else {
        StringsOfLanguages.setLanguage("ar")
    }
    return (

        <View style={[styles.row_container, {}]}>

            <View style={styles.flexDirect}>
                <Image source={{ uri: item.profileImage }} style={{ height: 60, width: 60, borderRadius: 30 }} />
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={[styles.flexDirect, { marginTop: 5 }]}>
                            <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', marginLeft: 10, marginEnd: 10 }]}>{item.userName}</Text>
                            <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.colorPrimaryDark, marginLeft: 10, marginEnd: 10 }]}>{item.distance} {'km'} {StringsOfLanguages.away}</Text>
                        </View>

                        <View style={[styles.flexDirect, { marginTop: 5 }]}>
                            <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.serviceType} :</Text>
                                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}>{item.serviceType}</Text>
                            </View>

                            <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.typeOfAc} : </Text>
                                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}> {item.acType} </Text>
                            </View>

                            <View style={{ marginLeft: 0, marginRight: 0, flex: 1, alignItems: 'center' }}>
                                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}> {item.noOfAc} </Text>
                            </View>
                        </View>
                    </View>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize, marginLeft: 5, marginRight: 5, marginTop: 10 }]}> {item.created_at} </Text>
                </View>
            </View>

            <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: '40%', alignSelf: 'center' }]}
                onPress={() => {
                    // alert('Alert with one button');
                    props.navigation.navigate("NewJobDetailsScreen", {
                        vendorServiceId: item.vendorServiceId,
                        serviceType: item.serviceType,
                        acType: item.acType,
                        description: item.description,
                        distance: item.distance,
                        location: item.location,
                        latitude: item.latitude,
                        longitude: item.longitude,
                        noOfAc: item.noOfAc,
                        profileImage: item.profileImage,
                        userName: item.userName,
                        created_at: item.created_at,
                        priceQuote: item.priceQuote,
                    });
                }}>
                <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 12 }}>{StringsOfLanguages.getDetailsCap}</Text>
            </TouchableOpacity>
        </View>
    );
}

export default VendorQuotesRow;

