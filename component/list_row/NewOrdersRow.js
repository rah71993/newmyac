import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import dimens from '../utils/diamens';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import diamens from '../utils/diamens';


const VendorQuotesRow = ({ item, styles, langu, props, click }) => {
   var langValue="en"
    if (langu) {
        StringsOfLanguages.setLanguage("en")
        langValue="en"
    } else {
        StringsOfLanguages.setLanguage("ar")
        langValue="ar"
    }
    return (

        <View style={[styles.row_container, {}]}>

            <View style={styles.flexDirect}>

                <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}>{item.serviceRequestDate}</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.serviceType} :</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}>{item.serviceType}</Text>
                </View>


                <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                    <Text numberOfLines={1} style={styles.textAlignn}></Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.typeOfAc} :</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}>{item.acType}</Text>
                </View>

                <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Medium', color: colors.blue, fontSize: dimens.tinySize, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.within} {item.distance} km</Text>
                    <Text numberOfLines={1} style={[{ fontFamily: 'SFUIText-Regular', textAlign: 'center', marginTop: 7, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                    <Text numberOfLines={1} style={[{ fontFamily: 'SFUIText-Bold', textAlign: 'center', fontWeight: 'bold', fontSize: dimens.ternarySize }]}>{item.noOfAc}</Text>
                </View>

            </View>

            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>

                    <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: 130, marginBottom: 5, alignSelf: 'center' }]}
                        onPress={() => {
                            props.navigation.navigate("VendorQuotesScreen", { 'serviceRequestId': item.serviceRequestId });
                        }}>
                        <Text numberOfLines={1} style={{ marginRight: 10, marginLeft: 10, color: "white", fontFamily: 'SFUIText-Bold', fontSize: dimens.tinySize }}>{StringsOfLanguages.viewQuotesCap}</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1 }}>
                    <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: 130, marginBottom: 5, alignSelf: 'center' }]}
                        onPress={async () => {
                            click(item)

                            var isConnected = await CommanUtils.checkInternet()
                            var dataFromLocal = await CommanUtils.getLoginDetails();
                            var Authorization = "Bearer " + dataFromLocal.token
                            if (isConnected) {
                                var responceData = await ApiClient.cancelJob(item.serviceRequestId, Authorization,langValue);
                                if (responceData != undefined) {
                                    click(item)
                                }
                            }
                        }}>
                        <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }}>{StringsOfLanguages.cancelCap}</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    );
}

export default VendorQuotesRow;

