import React from 'react';
import 'react-native-gesture-handler';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import colors from '../utils/colors';
import diamens from '../utils/diamens';
import ApiClient from '../utils/ApiClientService';

var user = require('../../images/user_man.png');

const ServiceType = ({ item, styles, props, state }) => {
    return (<View>
        <CardView
            style={{ backgroundColor: item.isRead === '1' ? '#fff' : colors.greyVeryLight, marginEnd: 20, marginStart: 20, marginTop: 20, marginBottom: 1, paddingStart: 10, paddingEnd: 10 }}
            cardElevation={2}
            cardMaxElevation={2}
            cornerRadius={5}>
            <View style={[styles.flexDirect, { padding: 10 }]}>
                <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular',flex: 1, fontSize: diamens.ternarySize, marginTop: 10, marginBottom: 10 }]}>
                    {item.title}
                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular',padding: 5, color: colors.blue, fontSize: diamens.ternarySize }]}
                        onPress={async () => {
                            ApiClient.readNotification(state, item.notificationId);
                            if (state.loginType == 'user') {
                                props.navigation.navigate('MyOrders', { "orderType": item.notificationType })
                            } else {
                                props.navigation.navigate('JobHistory', { "jobType": item.notificationType })
                            }
                        }} >
                        {" " + item.date}
                    </Text>
                </Text>
            </View>
        </CardView>
    </View>
    )
};


export default ServiceType;