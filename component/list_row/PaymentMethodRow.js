import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';


const ServiceType = ({ item, right_icon, styles, props, click }) => {
    console.log(item)
    return (
        <View style={{flex:1}}>
            <TouchableOpacity
                onPress={() => {
                    click(item)
                }}>
                <CardView
                    style={{ backgroundColor: '#fff', margin:5,padding:10 }}
                    cardElevation={1}
                    cardMaxElevation={2}
                    cornerRadius={5}>
                    <View style={{}}>
                        <Image source={{ uri: item.ImageUrl }} style={{flex:1,height:50, justifyContent: 'center', alignItems: 'center', resizeMode:'contain'}} />

                        <Text numberOfLines={1} style={[ { color: '#000', marginBottom: 15, marginTop: 15, fontSize:diamens.tinySize, fontFamily: 'SFUIText-Bold', }]}>{item.PaymentMethodEn}</Text>
                    </View>
                </CardView>
            </TouchableOpacity>
        </View>
    );
}
export default ServiceType;