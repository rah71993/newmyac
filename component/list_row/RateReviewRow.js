import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
// import { Rating, AirbnbRating } from 'react-native-elements';
import { Rating, AirbnbRating } from 'react-native-ratings';
import StringsOfLanguages from '../utils/StringsOfLanguages'
var user = require('../../images/user_man.png');
import diamens from '../utils/diamens';


const ServiceType = ({ item, styles, props, state, click }) => {
    // console.log(item.profileImage)
    return (<View>
        <CardView
            style={{ backgroundColor: '#fff', marginEnd: 20, marginStart: 20, marginTop: 20, marginBottom: 1, paddingStart: 10, paddingEnd: 10 }}
            cardElevation={2}
            cardMaxElevation={2}
            cornerRadius={10}>
            <View style={[styles.flexDirect, { padding: 10 }]}>
                <Image source={{ uri: item.profileImage }} style={{ height: 40, width: 40, borderRadius: 20 }} />
                <View style={{ flex: 1, flexDirection: 'column',marginEnd:5,marginStart:5 }}>
                    <Text style={[styles.textAlignn, {fontFamily: 'SFUIText-Semibold', marginEnd: 5, marginStart: 5, fontSize: diamens.secondrySize, }]}>{item.name}</Text>
                    <View pointerEvents="none" style={[styles.flexDirect]}>
                        <Rating type='custom' ratingColor={colors.blue} ratingBackgroundColor={colors.grey} startingValue={""+item.rating} ratingCount={5} imageSize={15}  style={[styles.alignSelf, { paddingTop: 5, paddingBottom: 5 }]} />
                        <Text style={[styles.textAlignn, {fontFamily: 'SFUIText-Semibold', alignSelf: 'center', marginEnd: 5, marginStart: 5, fontSize: diamens.tinySize, color: colors.blue }]}> {item.rating}.0</Text>
                    </View>
                    <Text numberOfLines={2} style={[styles.textAlignn, {fontFamily: 'SFUIText-Regular', marginEnd: 5, marginStart: 5, fontSize: diamens.ternarySize, }]}>{item.comment}</Text>
                    <Text style={[styles.textAlignn, styles.alignSelfFlexEnd, {fontFamily: 'SFUIText-Regular', marginEnd: 5, marginStart: 5, fontSize: diamens.tinySize, color: colors.blue }]}
                        onPress={() => {
                            click(item)
                        }}>{StringsOfLanguages.readMore}</Text>
                </View>
            </View>
        </CardView>
    </View>);
}

export default ServiceType;