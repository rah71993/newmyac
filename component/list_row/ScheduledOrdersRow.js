import React from 'react';
import { View, Text, Image, TouchableOpacity, Linking } from 'react-native';
import dimens from '../utils/diamens';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';

const VendorQuotesRow = ({ item, styles, langu, props , click}) => {
    var isInvoiceSend = (item.isInvoiceSent === 'true');
    let inVoiceCancelView = null
    if (langu) {
        StringsOfLanguages.setLanguage("en")
    } else {
        StringsOfLanguages.setLanguage("ar")
    }

    if (isInvoiceSend) {
        inVoiceCancelView = (
            <TouchableOpacity disabled={!isInvoiceSend} style={[styles.btnPressSmall, { backgroundColor: !isInvoiceSend ? colors.grey : colors.colorPrimaryDark, marginTop: 20, flex: 1, marginLeft: 10, marginRight: 10 }]}
                onPress={() => {
                    // alert('Alert with one button');
                    props.navigation.navigate("PaymentDash", {
                        vendorServiceId: item.vendorServiceId,
                        invoiceId: item.invoiceId,
                        scheduleDate: item.scheduleDate,
                        totalAmount: item.totalAmount,
                        serviceType: item.serviceType,
                        additionalPrice: item.additionalPrice,
                        paymentStatus: item.paymentStatus,
                        vendorName: item.vendorName,
                        priceQuote: item.priceQuote,
                        acType: item.acType,
                        noOfAc: item.noOfAc,
                        profileImage: item.profileImage,
                        vendorId: item.vendorId,
                    });
                }}>
                <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: dimens.tinySize }}>{StringsOfLanguages.viewInvoiceCap}</Text>
            </TouchableOpacity>
        )
    } else {
        inVoiceCancelView = (
            <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, flex: 1, marginLeft: 10, marginRight: 10 }]}
                onPress={async () => {
                    click(item)

                    var isConnected = await CommanUtils.checkInternet()
                    var dataFromLocal = await CommanUtils.getLoginDetails();
                    var Authorization = "Bearer " + dataFromLocal.token
                    if (isConnected) {
                        var responceData = await ApiClient.cancelJob(item.serviceRequestId, Authorization);
                        if (responceData != undefined) {
                            click(item)
                        }
                    }
                }}>
                <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }}>{StringsOfLanguages.cancelCap}</Text>
            </TouchableOpacity>
        )
    }

    return (

        <View style={[styles.row_container, {}]}>

            <View>
                <View style={styles.flexDirect}>
                    <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, marginLeft: 10, fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}>{item.scheduleDate} / {item.scheduleTime}</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { marginRight: 10, fontFamily: 'SFUIText-Medium', color: colors.blue, fontSize: dimens.tinySize, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.within} {item.distance} km</Text>
                </View>

                <View style={styles.flexDirect}>

                    <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.serviceType} :</Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}>{item.serviceType}</Text>
                    </View>


                    <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                        <Text numberOfLines={1} style={styles.textAlignn}></Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.typeOfAc} :</Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}>{item.acType}</Text>
                    </View>

                    <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                        <Text numberOfLines={1} style={[{ fontFamily: 'SFUIText-Regular', textAlign: 'center', marginTop: 7, fontSize: dimens.ternarySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                        <Text numberOfLines={1} style={[{ textAlign: 'center', fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }]}> {item.noOfAc} </Text>
                    </View>

                </View>
            </View>
            <View style={styles.flexDirect}>
                <TouchableOpacity style={[styles.btnPressSmall, {marginTop: 20, flex: 1, marginLeft: 10, marginRight: 10 }]}
                    onPress={() => {
                        Linking.openURL(`tel:${item.vendorContact}`)
                    }}>
                    <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: dimens.tinySize }}>{StringsOfLanguages.callVendorCap}</Text>
                </TouchableOpacity>

                {inVoiceCancelView}

            </View>

        </View>
    );
}

export default VendorQuotesRow;

