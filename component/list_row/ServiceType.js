import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'


const ServiceType = ({ item, right_icon ,styles,state,props}) => (

    <View>
    <TouchableOpacity
        onPress={() => {
            // alert('Alert with one button');
            props.navigation.navigate("ServiceRequestScreen",{acTypeId:state.acTypeId,address:state.address,serviceType:item.title,serviceTypeId:item.serviceId})
        }}>
        <CardView
            style={{ backgroundColor: '#fff', marginEnd: 20, marginStart: 20, marginTop: 20, marginBottom: 1, paddingStart: 10, paddingEnd: 10 }}
            cardElevation={2}
            cardMaxElevation={2}
            cornerRadius={5}>
            <View style={[styles.flexDirect, { padding: 10 }]}>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    <Text style={[styles.textAlignn, { color: '#4384FF', fontSize: 16, fontFamily: 'SFUIText-Bold', }]}>{item.title}</Text>
                    <Text style={[styles.textAlignn, { flex: 1, marginTop: 10 }]}>{item.description}</Text>
                </View>
                <Image source={right_icon} style={{ marginStart: 10, marginEnd: 10, height: 15, width: 15, alignSelf: 'center' }} />
            </View>
        </CardView>
    </TouchableOpacity>
    </View>
);

export default ServiceType;