import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import JobHistoryScreen from '../screens/JobHistoryScreen';
import { State } from 'react-native-gesture-handler';
import ApiClient from '../utils/ApiClientService';
import CommanUtils from '../utils/CommanUtils';
import diamens, { tinySize } from '../utils/diamens'


const VendorQuotesRow = ({ item, styles, props, state, click }) => (

    <View style={[styles.row_container, {}]}>

        <View style={styles.flexDirect}>
            <Image source={{ uri: item.profileImage }} style={{ height: 60, width: 60, borderRadius: 30 }} />

            <View style={{ flex: 1 }}>


                <View style={[styles.flexDirect, { marginTop: 5 }]}>
                    <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', marginLeft: 10, marginEnd: 10 }]}>{item.userName}</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.colorPrimaryDark, marginLeft: 10, marginEnd: 10 }]}>{`${item.distance} km ${StringsOfLanguages.away}`}</Text>
                </View>

                <View style={[styles.flexDirect, { marginTop: 10 }]}>
                    <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.serviceType} :</Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}>{item.serviceType}</Text>
                    </View>

                    <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.typeOfAc} : </Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}> {item.acType} </Text>
                    </View>

                    <View style={{ marginLeft: 0, marginRight: 0, flex: 1, alignItems: 'center' }}>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.veryTinySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }]}> {item.noOfAc} </Text>
                    </View>


                </View>

                <View style={{ flex: 1, height: 1, backgroundColor: colors.greyVeryLight, marginTop: 10, marginStart: -60 }}></View>

                <View style={[styles.flexDirect, {}]}>

                    <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: diamens.tinySize }]}>{StringsOfLanguages.scheduledDate} :</Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }]}>{item.scheduleDate}</Text>
                    </View>

                    <View style={{ marginLeft: 15, flex: 1, }}>
                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: diamens.tinySize }]}>{StringsOfLanguages.scheduledTime} :</Text>
                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }]}>{item.scheduleTime}</Text>
                    </View>

                </View>
            </View>


        </View>




        <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
                <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: 130, marginBottom: 5, alignSelf: 'center' }]}
                    onPress={() => {
                        // alert('Alert with one button');
                        props.navigation.navigate("UpcomingJobDetailsScreen", {
                            title: item.userName,
                            serviceType: item.serviceType,
                            distance: item.distance,
                            location: item.location,
                            latitude: item.latitude,
                            longitude: item.longitude,
                            image_url: item.profileImage,
                            description: item.description,
                            priceQuote: item.priceQuote,
                            scheduleDate: item.scheduleDate,
                            scheduleTime: item.scheduleTime,
                            vendorServiceId: item.vendorServiceId,
                            acType: item.acType,
                            noOfAc: item.noOfAc,
                            customerContact: item.customerContact,
                        });
                    }}>
                    <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }}>{StringsOfLanguages.getDetailsCap}</Text>
                </TouchableOpacity>
            </View>

            <View style={{ flex: 1 }}>
                <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: 130, marginBottom: 5, alignSelf: 'center' }]}
                    onPress={async () => {
                        var isConnected = await CommanUtils.checkInternet()
                        var dataFromLocal = await CommanUtils.getLoginDetails();
                        var Authorization = "Bearer " + dataFromLocal.token
                        if (isConnected) {
                            var responceData = await ApiClient.cancelJob(item.vendorServiceId, Authorization);
                            if (responceData != undefined) {
                                click(item)
                            }
                        }
                    }}>
                    <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }}>{StringsOfLanguages.cancelCap}</Text>
                </TouchableOpacity>
            </View>


        </View>
    </View>
);

export default VendorQuotesRow;