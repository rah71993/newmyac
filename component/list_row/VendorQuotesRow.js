import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import diamens, { tinySize } from '../utils/diamens';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import { Rating, AirbnbRating } from 'react-native-ratings';
import colors from '../utils/colors';


const VendorQuotesRow = ({ item, styles, props }) => (
    <View style={styles.row_container}>

        <View style={styles.flexDirect} >
            <Image source={{ uri: item.profileImage }} style={{ height: 60, width: 60, borderRadius: 30 }} />

            <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold',fontSize:diamens.ternarySize }]}>{item.vendorName}</Text>
                <Text style={[styles.textAlignn, {fontFamily: 'SFUIText-Regular', marginTop: 7 ,fontSize:diamens.ternarySize}]}>{StringsOfLanguages.serviceType} :</Text>
                <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold',fontSize:diamens.ternarySize }]}>{item.serviceType}</Text>
            </View>

            <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                <View pointerEvents="none" style={[styles.flexDirect, {}]}>
                    <Rating type='custom' startingValue={item.rating} ratingCount={5} imageSize={15} ratingColor={colors.blue} ratingBackgroundColor={colors.grey} style={[styles.alignSelf, { paddingTop: 5, paddingBottom: 5 }]} />
                    <Text style={[styles.textAlignn, {fontFamily: 'SFUIText-Semibold', alignSelf: 'center', marginEnd: 5, marginStart: 5, fontSize: diamens.tinySize, color: colors.blue }]}>{item.rating}.0</Text>
                </View>
                <Text style={[styles.textAlignn,{fontFamily: 'SFUIText-Regular',fontSize:diamens.ternarySize}]}>{StringsOfLanguages.priceQuoted} :</Text>
                <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF',fontSize:diamens.ternarySize }]}> {'SAR ' + item.priceQuote} </Text>
            </View>
        </View>
        <TouchableOpacity style={[styles.btnPressSmall, styles.alignSelfFlexEnd, { marginTop: 20, width: '40%' }]}
            onPress={() =>
                // props.navigation.navigate("SuccessScheduleScreen", { vendorServiceId: item.vendorServiceId })
                props.navigation.navigate("ScheduleServiceScreen", { vendorServiceId: item.vendorServiceId })
            }>
            <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: tinySize }}>{StringsOfLanguages.selectCap}</Text>
        </TouchableOpacity>
    </View>
);

export default VendorQuotesRow;