import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import dimens from '../utils/diamens';
import StringsOfLanguages from '../utils/StringsOfLanguages'

const VendorQuotesRow = ({ item, styles, langu, props }) => {
    if (langu) {
        StringsOfLanguages.setLanguage("en")
    } else {
        StringsOfLanguages.setLanguage("ar")
    }
    return (

        <View style={[styles.row_container, {}]}>

            <View style={[langu ? styles.scheduleLeft : styles.scheduleRight]}>
                <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize }}>{StringsOfLanguages.scheduled}</Text>
            </View>

            <View style={styles.flexDirect}>
                <Image source={{ uri: item.profileImage }} style={{ height: 60, width: 60, borderRadius: 30 }} />

                <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                    <Text numberOfLines={1} style={styles.textAlignn}>{item.vendorName}</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular',marginTop: 7 }]}>{StringsOfLanguages.serviceType} :</Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold',}]}>{item.serviceType}</Text>
                </View>

                <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                    <Text numberOfLines={1} style={[styles.textAlignn,{fontFamily: 'SFUIText-Regular',}]}>{StringsOfLanguages.priceQuoted} : </Text>
                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF' }]}> {'SAR ' + item.priceQuote} </Text>
                    <TouchableOpacity style={[styles.btnPressSmall, { marginTop: 20, width: '100%' }]}
                        onPress={() => {
                            // alert('Alert with one button');
                            props.navigation.navigate("PaymentDash", {
                                vendorServiceId: item.vendorServiceId,
                                invoiceId: item.invoiceId,
                                scheduleDate: item.scheduleDate,
                                totalAmount: item.totalAmount,
                                serviceType: item.serviceType,
                                additionalPrice: item.additionalPrice,
                                paymentStatus: item.paymentStatus,
                                vendorName: item.vendorName,
                                priceQuote: item.priceQuote,
                                acType: item.acType,
                                noOfAc: item.noOfAc,
                                profileImage: item.profileImage,
                                vendorId: item.vendorId,

                            });
                        }}>
                        <Text numberOfLines={1} style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 12 }}>{StringsOfLanguages.viewInvoice}</Text>
                    </TouchableOpacity>
                </View>

            </View>
        </View>
    );
}

export default VendorQuotesRow;

