import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { MFWebView, MFSettings, MFTheme } from 'myfatoorah-reactnative';
import InvoiceDetailsScreen from '../screens/InvoiceDetailsScreen';
import PaymentSuccessScreenD from '../screens/PaymentSuccessScreenD';
import MainScreen from '../screens/MainScreen';
import PaymentMyFTW from './PaymentMyFTW';
import URL from '../utils/Constraints';

const Stack = createStackNavigator();

export default class PaymentDash extends React.Component {
  componentDidMount() {

    let base_url = URL.baseUrlPaymentLive;
    let token = URL.tokenPaymentLive;

    let theme = new MFTheme('#4384FF', 'white', 'Payment', 'Cancel')
    MFSettings.sharedInstance.setTheme(theme)
    MFSettings.sharedInstance.configure(base_url, token);
  }

  render() {

    const vendorServiceId = this.props.navigation.getParam('vendorServiceId', 0);
    const invoiceId = this.props.navigation.getParam('invoiceId', 'no data');
    const scheduleDate = this.props.navigation.getParam('scheduleDate', 'no data');
    const totalAmount = this.props.navigation.getParam('totalAmount', '0');
    const serviceType = this.props.navigation.getParam('serviceType', 'no data');
    const additionalPrice = this.props.navigation.getParam('additionalPrice', '0');
    const paymentStatus = this.props.navigation.getParam('paymentStatus', 'no data');
    const vendorName = this.props.navigation.getParam('vendorName', 'no data');
    const priceQuote = this.props.navigation.getParam('priceQuote', '0');
    const acType = this.props.navigation.getParam('acType', 'no data');
    const noOfAc = this.props.navigation.getParam('noOfAc', 'no data');
    const profileImage = this.props.navigation.getParam('profileImage', 'no data');
    const vendorId = this.props.navigation.getParam('vendorId', 'no data');

    return (
      <NavigationContainer>
        <Stack.Navigator mode="modal" initialRouteName="InvoiceDetailsScreen">

          <Stack.Screen name="InvoiceDetailsScreen"
            component={InvoiceDetailsScreen}
            initialParams={{
              vendorServiceId: vendorServiceId,
              invoiceId: invoiceId,
              scheduleDate: scheduleDate,
              totalAmount: totalAmount,
              serviceType: serviceType,
              additionalPrice: additionalPrice,
              paymentStatus: paymentStatus,
              vendorName: vendorName,
              priceQuote: priceQuote,
              acType: acType,
              noOfAc: noOfAc,
              profileImage: profileImage,
              vendorId: vendorId,
            }}
            options={{ headerShown: false }} />

          <Stack.Screen name="PaymentSuccessScreenD"
            component={PaymentSuccessScreenD}
            options={{ headerShown: false }}
          />

          <Stack.Screen name="MainScreen"
            component={MainScreen}
            options={{ headerShown: false }}
          />


          <Stack.Screen name="PaymentMyFTW"
            component={PaymentMyFTW}
            options={{ headerShown: false }}
          />


          <Stack.Screen
            name="MFWebView"
            component={MFWebView}
            mode="modal"
            options={MFWebView.navigationOptions}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}