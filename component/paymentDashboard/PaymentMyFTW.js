import CustToo from '../helper/CustomToolbarBackBtn';
import React, { Component } from 'react';
import { View, TouchableOpacity, Text, FlatList, ActivityIndicator, ImageBackground } from 'react-native';
import style from '../utils/style';
import colors from '../utils/colors';
import dimens, { primarySize, ternarySize, tinySize } from '../utils/diamens';
import CustomRow from '../list_row/PaymentMethodRow';

import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import ApiClient from '../utils/ApiClientService';


import {
    MFPaymentRequest,
    MFCustomerAddress,
    MFExecutePaymentRequest,
    Response,
    MFLanguage,
    MFMobileCountryCodeISO,
    MFCurrencyISO,
    MFPaymentype,
    MFInitiatePayment,
    MFKeyType
} from 'myfatoorah-reactnative';
import diamens from '../utils/diamens';

var bgImage = require('../../images/main_bg.png');


export default class PaymentMyFTW extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            vendorServiceId: 0,
            vendorId: 0,
            Authorization: 'thf',
            profileImage: 'thf',
            langValue: 'en',
            dataResponse: [],
            dataPaymentMethods: [],

        }
        this.clickItem = this.clickItem.bind(this)
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        const { vendorServiceId, vendorId, profileImage, totalAmount, additionalPrice } = this.props.route.params;

        this.setState({ languag: lan, vendorServiceId: vendorServiceId, vendorId: vendorId, profileImage: profileImage, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        var subTotal = Number(totalAmount) + Number(additionalPrice);
        this.initiatePayments(subTotal)
    }

    async hitPaymentSuccessApi(invoiceTransId) {
        this.showLoading()
        var responceData = await ApiClient.orderSuccess(this.state);
        if (responceData != undefined) {
            this.navigateToSuccessScreen(invoiceTransId)
        }
        this.hideLoading()
    };

    navigateToSuccessScreen(invoiceTransId) {
        this.props.navigation.navigate("PaymentSuccessScreenD", { invoiceTransId: invoiceTransId, vendorId: this.state.vendorId, profileImage: this.state.profileImage })
    }


    executeResquestJson(TotalAmount, paymentMethodId) {
        let request = new MFExecutePaymentRequest(parseFloat(TotalAmount), paymentMethodId);
        request.customerEmail = "rahulkumar71993@gmail.com"; // must be email
        request.customerMobile = "";
        request.customerCivilId = "";
        let address = new MFCustomerAddress("ddd", "sss", "sss", "sss", "sss");
        request.customerAddress = address;
        request.customerReference = "";
        request.language = "en";
        request.mobileCountryCode = MFMobileCountryCodeISO.SAUDIARABIA;
        request.displayCurrencyIso = MFCurrencyISO.SAUDIARABIA_SAR;
        // var productList = []
        // var product = new MFProduct("ABC", 1.887, 1)
        // productList.push(product)
        // request.invoiceItems = productList
        return request;
    }

    executePayment(TotalAmount, paymentMethodId) {
        let request = this.executeResquestJson(TotalAmount, paymentMethodId);
        this.showLoading()
        // console.log(request)
        MFPaymentRequest.sharedInstance.executePayment(this.props.navigation, request, MFLanguage.ENGLISH, (response: Response) => {
            this.hideLoading()
            if (response.getError()) {
                // alert('error: ' + response.getError().error);
                console.log(response.getError())
            }
            else {
                var bodyString = response.getBodyString();
                var invoiceId = response.getInvoiceId();
                var paymentStatusResponse = response.getBodyJson().Data;
                // alert('success with Invoice Id: ' + invoiceId + ', Invoice status: ' + paymentStatusResponse.InvoiceStatus);
                console.log(response)
                this.setState({ dataResponse: paymentStatusResponse })
                this.hitPaymentSuccessApi(invoiceId)
            }
        });
    }

    initiatePayments(subtotal) {
        this.showLoading()
        let initiateRequest = new MFInitiatePayment(parseFloat(subtotal), MFCurrencyISO.SAUDIARABIA_SAR)
        MFPaymentRequest.sharedInstance.initiatePayment(initiateRequest, MFLanguage.ENGLISH, (response: Response) => {
            if (response.getError()) {
                alert('error: ' + response.getError().error);
            }
            else {
                // executePayment(response.getPaymentMethods())
                console.log(response.getPaymentMethods())
                this.setState({ dataPaymentMethods: response.getPaymentMethods() })
            }
            this.hideLoading()
        });
    }


    showLoading() {
        this.setState({ loading: true })
    }
    hideLoading() {
        this.setState({ loading: false })
    }

    clickItem(value) {
        this.executePayment(value.TotalAmount, value.PaymentMethodId)
    }

    render() {

        let styles;
        let paymentMethods;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (this.state.loading) {
            paymentMethods = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            paymentMethods = (
                <FlatList
                    numColumns={3}
                    data={this.state.dataPaymentMethods}
                    renderItem={({ item }) => {
                        return <CustomRow item={item} styles={styles} props={this.props} state={this.state} click={this.clickItem} />
                    }} />

            );
        }


        // var subTotal = Number(totalAmount) + Number(additionalPrice);

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.selectPaymentMethod} />

                <ImageBackground style={{ flex: 1, padding: 10 }} source={bgImage}>
                    {paymentMethods}
                </ImageBackground>
            </View>
        );
    }
}