
import React, { Component } from 'react';
import { View, FlatList, Image, Text, TouchableOpacity, ActivityIndicator, ImageBackground, Alert } from 'react-native';
import colors from '../utils/colors';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import CustToo from '../helper/CustomToolbarBackBtn';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';
import CustomRow from '../list_row/ChooseAddressRow';
import Geocoder from 'react-native-geocoding';
import Geolocation from '@react-native-community/geolocation';

var bgImage = require('../../images/main_bg.png');
var logoutImage = require('../../images/logout.png');
var gps = require('../../images/gps.png');
var plus = require('../../images/plus.png');

export default class ChooseAdressScreen extends Component {


    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            name: '',
            email: '',
            mobile_number: '',
            mobile_code: '',
            address: '',
            latitude: 37.785834,
            longitude: -122.406417,
            addressData: [],
            profile_pic: '',
            Authorization: '',
            nameEnabled: false,
            emailEnabled: false,
            addressEnabled: false,
            isChecked: false,
            isLocatio: false
        }

    }

    async componentDidMount() {
        await this.getAddress()
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan, })

        this.showDataFromLocal()
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.showDataFromLocal()
        });
    }

    async showDataFromLocal() {
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({
            name: dataFromLocal.name,
            email: dataFromLocal.email,
            mobile_number: dataFromLocal.mobile_number,
            mobile_code: dataFromLocal.mobile_code,
            addressData: dataFromLocal.address,
            profile_pic: dataFromLocal.profile_pic,
            Authorization: "Bearer " + dataFromLocal.token,
        });
    }

    async getAddress() {
        Geocoder.init(StringsOfLanguages.googleApiKey);
        Geolocation.getCurrentPosition(info => {
            Geocoder.from(info.coords.latitude, info.coords.longitude)
                .then(json => {
                    var addressComponent = json.results[0].formatted_address;
                    this.setState({ latitude: info.coords.latitude, longitude: info.coords.longitude, address: addressComponent })
                    this.setState({ isLocatio: true })
                    console.log("after location......",this.state)
                })
                .catch(error => console.warn(error));
        });
    }


    navigateToMap() {
        this.props.navigation.navigate("MapViewM", { title: this.state.address, latitude: this.state.latitude, longitude: this.state.longitude })
    }
    navigateToProfile() {
        this.props.navigation.navigate("EditProfileScreen");
    }


    render() {
        let styles;
        let addressView;

        const { addressData, isLocatio } = this.state;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }
        addressView = (
            <FlatList
                data={addressData}
                // keyExtractor={item => item.key.toString()}
                renderItem={({ item }) => {
                    return <CustomRow item={item} state={this.state} right_icon={right_icon} styles={styles} props={this.props} />
                }} />
        )

        return (

            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.chooseAddress} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>
                    <ScrollView>
                        <View style={{ flex: 1, margin: 15 }}>

                            <TouchableOpacity style={{ marginTop: 10 }}
                                disabled={!isLocatio}
                                onPress={() => { this.navigateToMap() }}>
                                <View style={[styles.flexDirect, { padding: 0 }]}>
                                    <Image source={gps} style={{ tintColor: isLocatio ? colors.blue : colors.grey, alignSelf: 'center', marginStart: 0, height: 13, width: 13 }} />
                                    <Text style={{ fontFamily: 'SFUIText-medium', color: isLocatio ? colors.blue : colors.grey, marginStart: 10,marginEnd:10, fontSize: diamens.ternarySize }}>{StringsOfLanguages.useMyCurrentLocation}</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ marginTop: 15 }}
                                onPress={() => { this.navigateToProfile() }}>
                                <View style={[styles.flexDirect, { padding: 0 }]}>
                                    <Image source={plus} style={{ tintColor: colors.blue, alignSelf: 'center', marginStart: 0, height: 13, width: 13 }} />
                                    <Text style={{ fontFamily: 'SFUIText-medium', color: colors.blue, marginStart: 10,marginEnd:10, fontSize: diamens.ternarySize }}>{StringsOfLanguages.addNewAddress}</Text>
                                </View>
                            </TouchableOpacity>

                            <Text style={[styles.textAlignn,{ marginTop: 15, fontFamily: 'SFUIText-Bold', color: colors.blackLight, fontSize: diamens.secondrySize }]}>{StringsOfLanguages.savedAddress}</Text>


                            <View style={[{ marginTop: 2, height: .5, backgroundColor: colors.blackLight }]}></View>

                            {addressView}
                        </View>
                    </ScrollView>

                </ImageBackground>

            </View>

        );
    }
}

const data = [
    { key: 'Android' }, { key: 'iOS' },
]

