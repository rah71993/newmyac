
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { View, ImageBackground, Image, Text, TouchableOpacity } from 'react-native';
import style from '../utils/style';
import StringsOfLanguages from '../utils/StringsOfLanguages';
import colors from '../utils/colors';
import CommanUtils from '../utils/CommanUtils';
import GeneralStatusBarColor from "react-native-general-statusbar";
import diamens from '../utils/diamens';
var bgImage = require('../../images/main_bg.png');
var logoImage = require('../../images/logo.png');
var iconUser = require('../../images/icon_user.png');
var iconVendor = require('../../images/icon_vendor.png');
import CustToo from '../helper/CustomToolbarLoginBackBtn';

export default class ChooseUserVender extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
        }
    }


    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }

    async navigateToLogin(loginType) {
        await CommanUtils.saveLoginType(loginType)
        this.props.navigation.navigate("LoginScreen")
    }

    render() {

        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            // alert("english")
        } else {
            // alert("arabic "+this.state.languag)
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }


        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={""} />

                <ImageBackground style={[styles.container,{justifyContent:'center'}]} source={bgImage}>

                    <Image source={logoImage} style={styles.logo_style} />

                    <Text style={[styles.wordNormal, { marginTop: 40, marginBottom: 20 }]}>
                        {StringsOfLanguages.signInAsa}
                    </Text>

                    <TouchableOpacity style={[styles.btnPress]} onPress={() =>
                        this.navigateToLogin('user')
                    }>
                        <Image source={iconUser}
                            style={styles.ImageIconStyle}
                        />
                        <Text style={[styles.textButtonPress, { marginStart: 10, marginEnd: 10 }]}>{StringsOfLanguages.userCap}   </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.btnBorder, { marginTop: 20 }]} onPress={() =>
                        this.navigateToLogin('vendor')
                    }>
                        <Image source={iconVendor} style={styles.ImageIconStyle} />
                        <Text style={{ fontFamily: 'SFUIText-Bold', color: colors.colorPrimaryDark, fontSize: diamens.ternarySize, marginStart: 10, marginEnd: 10 }}>{StringsOfLanguages.vendorCap}</Text>
                    </TouchableOpacity>

                </ImageBackground>

            </View>
        );
    }
}




