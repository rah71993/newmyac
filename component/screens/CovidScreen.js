
import React, { Component } from 'react';
import { View, ImageBackground, Image, Text, TouchableOpacity } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import style from '../utils/style';
import StringsOfLanguages from '../utils/StringsOfLanguages';
import colors from '../utils/colors';
import CommanUtils from '../utils/CommanUtils';
import GeneralStatusBarColor from "react-native-general-statusbar";
import diamens from '../utils/diamens';
import { ScrollView } from 'react-native';
var bgImage = require('../../images/main_bg.png');
var wearMask = require('../../images/wearMask.png');
var handSenitizer = require('../../images/handSenitizer.png');
var socialDistance = require('../../images/socialDistance.png');

export default class CovidScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loginType: 'user',
        }
    }



    async componentDidMount() {
        var lan = await CommanUtils.getLanguage();
        var loginType = await CommanUtils.getLoginType();
        this.setState({ languag: lan, loginType: loginType });
    }

    navigateToMain() {
        const { loginType } = this.state;
        if (loginType == 'user') {
            this.props.navigation.navigate("SuccessRequestScreen");
        } else {
            this.props.navigation.navigate("SuccessQuoteScreen");
        }
    }

    render() {

        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }


        return (
            <View style={{ flex: 1 }}>

                <GeneralStatusBarColor
                    backgroundColor={colors.statusColor}
                    barStyle="light-content" />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    <ScrollView >

                        <View style={{ flex: 1,marginBottom:100, alignItems: 'center' }}>

                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, marginTop: 50, textAlign: 'center' }]}>
                                {StringsOfLanguages.Followthisstepstoensuresafety}
                            </Text>

                            <View style={{ marginTop: 30 }}>
                                <Image source={handSenitizer} style={[styles.logo_style, { height: 125, width: 154, resizeMode: 'contain', }]} />
                                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'center', marginTop: 0, height: 20, width: 20, borderRadius: 10, backgroundColor: 'white' }}>
                                    <Text >1</Text>
                                </View>
                            </View>
                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, marginTop: 20, textAlign: 'center' }]}>
                                {StringsOfLanguages.SantizeyourHands}
                            </Text>

                            <View style={{ marginTop: 20 }}>
                                <Image source={wearMask} style={[styles.logo_style, { height: 139, width: 165, resizeMode: 'contain', }]} />
                                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'center', marginTop: 0, height: 20, width: 20, borderRadius: 10, backgroundColor: 'white' }}>
                                    <Text >2</Text>
                                </View>
                            </View>
                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, marginTop: 20, textAlign: 'center' }]}>
                                {StringsOfLanguages.wearMask}
                            </Text>

                            <View style={{ marginTop: 20 }}>
                                <Image source={socialDistance} style={[styles.logo_style, { height: 138, width: 227, resizeMode: 'contain', }]} />
                                <View style={{ position: 'absolute', alignItems: 'center', justifyContent: 'center', marginTop: 0, height: 20, width: 20, borderRadius: 10, backgroundColor: 'white' }}>
                                    <Text >3</Text>
                                </View>
                            </View>
                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, marginTop: 20, textAlign: 'center' }]}>
                                {StringsOfLanguages.socialDistancing}
                            </Text>
                        </View>

                    </ScrollView>

                    <TouchableOpacity style={[styles.btnPress, { marginTop: 10, alignSelf: 'center', position: 'absolute', bottom: 20 }]} onPress={() =>
                        this.navigateToMain()
                    }>
                        <Text style={[styles.textButtonPress, { marginStart: 10, marginEnd: 10 }]}>{StringsOfLanguages.YESIWILLFOLLOWTHEGUIDELINES}   </Text>
                    </TouchableOpacity>

                </ImageBackground>

            </View>
        );
    }
}




