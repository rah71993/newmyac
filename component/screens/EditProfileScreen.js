import React, { Component } from 'react';
import { View, FlatList, Image, Text, KeyboardAvoidingView, TouchableOpacity, ActivityIndicator, Platform, ImageBackground, Modal, Alert } from 'react-native';
import colors, { colorPrimary } from '../utils/colors';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import CustToo from '../helper/CustomToolbarBackBtn';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import StringsOfLanguages from '../utils/StringsOfLanguages';
import ImagePicker from 'react-native-image-crop-picker';
import diamens from '../utils/diamens';
import { NavigationActions, StackActions } from 'react-navigation';
import CustomRow from '../list_row/AddressRow';
import CardView from 'react-native-cardview';
import PlaceAutoCompleteScreen from './PlaceAutoCompleteScreen';


var upload = require('../../images/upload.png');
var plus = require('../../images/plus.png');
var minus = require('../../images/minus.png');
var editImage = require('../../images/edit.png');
var bgImage = require('../../images/main_bg.png');
var logoutImage = require('../../images/logout.png');

export default class EditProfileScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            name: '',
            email: '',
            mobile_number: '',
            mobile_code: '',
            address: StringsOfLanguages.selectAddress,
            houseNo: "",
            latitude: 37.785834,
            longitude: -122.406417,
            addressData: [],
            profile_pic: '',
            Authorization: '',
            nameEnabled: false,
            emailEnabled: false,
            addressEnabled: false,
            modalVisible: false,
            langValue: "en",
        }
        this.onUpdateClick = this.onUpdateClick.bind(this)
        this.refreshD = this.refreshD.bind(this)
        this.handlePlaceCallback = this.handlePlaceCallback.bind(this)
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan, })

        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.showDataFromLocal()
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.showDataFromLocal()
        });
    }

    async showDataFromLocal() {
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({
            name: dataFromLocal.name,
            email: dataFromLocal.email,
            mobile_number: dataFromLocal.mobile_number,
            mobile_code: dataFromLocal.mobile_code,
            addressData: dataFromLocal.address,
            profile_pic: dataFromLocal.profile_pic,
            Authorization: "Bearer " + dataFromLocal.token,
        });
        console.log("dataFromLocal....", dataFromLocal)
    }

    async onUpdateClick() {
        //below code is to add address if enabled
        if (this.state.addressEnabled) {
            if (this.state.houseNo === "") {
                CommanUtils.showAlert("", "" + StringsOfLanguages.enter + " " + StringsOfLanguages.houseNo)
                return
            }
            if (this.state.address === StringsOfLanguages.selectAddress) {
                CommanUtils.showAlert("", "" + StringsOfLanguages.select + " " + StringsOfLanguages.address)
                return
            }
    
            let addressData = []
            if (this.state.addressData != null && this.state.addressData.length > 0) {
                addressData = this.state.addressData;
                addressData.push({
                    houseNo: this.state.houseNo,
                    address: this.state.address,
                    latitude: this.state.latitude,
                    longitude: this.state.longitude
                })
            } else {
                addressData = [{
                    houseNo: this.state.houseNo,
                    address: this.state.address,
                    latitude: this.state.latitude,
                    longitude: this.state.longitude
                }]
            }
    
            this.setState({ addressData: addressData, address: StringsOfLanguages.selectAddress, addressEnabled: false })
        }
        //upper code is for address part
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.updateUserProfile(this.state);
            this.setState({ loading: false })
            if (responceData != undefined) {
                if (responceData.status) {
                    CommanUtils.saveLoginDetails(responceData);
                    this.showDataFromLocal()
                    CommanUtils.showAlert(StringsOfLanguages.success, '' + responceData.message)
                } else if (responceData.message != undefined) {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                }
                else {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData)
                }
            }
        }
    }

   

    async refreshD(valu) {
        let allItems = [...this.state.addressData];
        let filteredItems = allItems.filter(item => item.address !== valu.address);
        this.setState({ addressData: filteredItems })
        this.onUpdateClick()
    }

    pickImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            console.log(image);
            this.setState({ profile_pic: image.path })
        });

    }

    onChangeText = ({ dialCode, unmaskedPhoneNumber, phoneNumber, isVerified }) => {
        this.setState({ mobile_code: dialCode, mobile_number: unmaskedPhoneNumber, isValidPhone: isVerified })
        console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
    };


    showLogoutAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: StringsOfLanguages.cancel,
                    onPress: () =>
                        console.log('Cancel Pressed'),
                },
                {
                    text: StringsOfLanguages.logout,
                    onPress: () => {
                        CommanUtils.saveLoginStatus(false);
                        this.gotoLogin()
                    }
                }
            ],
            { cancelable: false }
        );

    }
    gotoLogin() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'App', params: { 'initialScreen': 'ChooseUserVender' } }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }

    handlePlaceCallback(details) {
        console.log("Details......", details)
        var addressFormated = details.formatted_address
        var lat = details.geometry.location.lat
        var lng = details.geometry.location.lng
        this.setState({ address: addressFormated, latitude: lat, longitude: lng })

    }
    render() {
        let styles;
        let progressBar;
        let nameView;
        let emailView;
        let addressView;
        let addressAdd;

        const { loading, nameEnabled, modalVisible, emailEnabled, addressEnabled, name, email, mobile_number, mobile_code, address, addressData, profile_pic } = this.state;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            backIcon = require('../../images/back.png');
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
            backIcon = require('../../images/right-arrow.png');
        }

        if (loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            <View></View>
        }

        //Enable disable work for Text Input field
        if (nameEnabled) {
            nameView = (
                <View>
                    <Text style={[styles.smallTextStyle, { marginTop: 0 }]}>{StringsOfLanguages.name}</Text>
                    <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                        <TextInput
                            style={[styles.inputTxt]}
                            value={name}
                            onChangeText={text => this.setState({ name: text })} />
                    </View>
                </View>
            )
        } else {
            nameView = (
                <View style={[styles.flexDirect, { justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <Text style={{ flex: 1, marginStart: 15, marginEnd: 15, textAlign: 'center', fontFamily: 'SFUIText-Bold', fontSize: 16, marginTop: 10, color: 'black' }}>{name}</Text>
                    <TouchableOpacity style={{}} onPress={() => { this.setState({ nameEnabled: !nameEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, styles.alignSelfFlexEnd, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>)
        }

        if (emailEnabled) {
            emailView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={email}
                        onChangeText={text => this.setState({ email: text })} />
                </View>
            )
        } else {
            emailView = (
                <View style={[styles.flexDirect, styles.textAlignn, { justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 5, flex: 1, fontSize: diamens.secondrySize, }]}>{email}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ emailEnabled: !emailEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (addressEnabled) {
            addressAdd = (
                <View style={[styles.flexDirect, { padding: 10, alignItems: 'center', backgroundColor: colors.greyLight, justifyContent: 'center' }]}>

                    <View style={{ flex: 1 }}>
                        <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                            <TextInput
                                style={[styles.inputTxt]}
                                placeholder={StringsOfLanguages.houseNo}
                                placeholderTextColor={colors.black}
                                onChangeText={text => this.setState({ houseNo: text })} />
                        </View>

                        <TouchableOpacity style={{ height: 50, marginTop: 10, flex: 1, marginLeft: 0, marginRight: 0 }} onPress={() => {
                            this.props.navigation.navigate("PlaceAutoCompleteScreen", { handlePlaceCallback: this.handlePlaceCallback })
                        }}>
                            <CardView
                                style={{ justifyContent: 'center', backgroundColor: '#fff', padding: 0, flex: 1 }}
                                cardElevation={2}
                                cardMaxElevation={2}
                                cornerRadius={7}>
                                <Text numberOfLines={2} style={[styles.textAlignn, { textAlignVertical: 'center', marginLeft: 10, marginRight: 10 }]}>{address}</Text>
                            </CardView>
                        </TouchableOpacity>

                    </View>
                    {/* <TouchableOpacity style={{ marginLeft: 10, marginRight: 10 }} onPress={() => { this.addNewAddress() }}>
                        <CardView
                            style={{ backgroundColor: '#fff', height: 25, width: 25 }}
                            cardElevation={2}
                            cardMaxElevation={2}
                            cornerRadius={12}>
                            <View style={[styles.flexDirect, { padding: 0, flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
                                <Image source={upload} style={{ tintColor: colors.blue, height: 13, width: 12, }} />
                            </View>
                        </CardView>
                    </TouchableOpacity> */}

                    <TouchableOpacity style={{marginLeft:10}} onPress={() => { this.setState({ addressEnabled: !addressEnabled }) }}>
                        <CardView
                            style={{ backgroundColor: '#fff', height: 25, width: 25 }}
                            cardElevation={2}
                            cardMaxElevation={2}
                            cornerRadius={12}>
                            <View style={[styles.flexDirect, { padding: 0, flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
                                <Image source={minus} style={{ tintColor: colors.blue, height: 12, width: 12, }} />
                            </View>
                        </CardView>
                    </TouchableOpacity>
                </View>
            )
        } else {
            addressAdd = (
                <View style={[styles.flexDirect, { justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Bold', flex: 1, color: colors.blackLight, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.addNewAddress}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ addressEnabled: !addressEnabled }) }}>
                        <CardView
                            style={{ backgroundColor: '#fff', height: 25, width: 25 }}
                            cardElevation={2}
                            cardMaxElevation={2}
                            cornerRadius={12}>
                            <View style={[styles.flexDirect, { padding: 0, flex: 1, alignItems: 'center', justifyContent: 'center' }]}>
                                <Image source={plus} style={{ tintColor: colors.blue, height: 12, width: 12, }} />
                            </View>
                        </CardView>
                    </TouchableOpacity>
                </View>
            )
        }

        addressView = (
            <FlatList
                data={addressData}
                // keyExtractor={item => item.key.toString()}
                renderItem={({ item }) => {
                    return <CustomRow item={item} right_icon={right_icon} styles={styles} props={this.props} click={this.refreshD} />
                }} />
        )

        return (

            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.myAccount} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>
                    <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', }} behavior="padding" enabled keyboardVerticalOffset={100}>

                        <ScrollView  >

                            <View style={{ flex: 1, margin: 15 }}>

                                <View style={[{ alignSelf: 'center', marginTop: 20, }]}
                                    onStartShouldSetResponder={
                                        () => this.pickImage()
                                    }>
                                    <View style={[styles.profileImgBackground, { alignSelf: 'center' }]}>
                                        <Image source={{ uri: profile_pic }} style={[styles.profileImg, {}]} />

                                        <View style={{ position: 'absolute', alignSelf: 'flex-end', bottom: 0, height: 30, width: 30, borderRadius: 15, backgroundColor: colors.colorPrimaryDark, justifyContent: 'center', alignItems: 'center' }}>
                                            <Image source={plus} style={[{ tintColor: 'white', height: 15, width: 15, }]} />
                                        </View>
                                    </View>
                                </View>

                                {nameView}

                                <Text style={[styles.smallTextStyle, { fontFamily: 'SFUIText-Regular', marginTop: 20 }]}>{StringsOfLanguages.mobileNumber}</Text>
                                <View style={[styles.flexDirect, { width: '100%' }]}>
                                    <Text style={[styles.textAlignn, { flex: 1, marginTop: 5, fontFamily: 'SFUIText-Regular', fontSize: diamens.secondrySize }]}>{mobile_code}-{mobile_number}</Text>
                                </View>

                                <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.email}</Text>
                                {emailView}

                                <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.address}</Text>
                                {addressView}

                                {addressAdd}


                                {progressBar}
                                <TouchableOpacity style={[styles.btnPress, { marginTop: 30, alignSelf: 'center' }]} onPress={() => {
                                    this.onUpdateClick()
                                }}>
                                    <Text style={styles.textButtonPress}>{StringsOfLanguages.saveCap}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={{ marginTop: 10, marginBottom: 10, alignSelf: 'baseline', alignSelf: 'center' }}
                                    onPress={() => { this.showLogoutAlert(StringsOfLanguages.alert, StringsOfLanguages.areSureWantToLogout); }}>
                                    <View style={[styles.flexDirect, { alignSelf: 'center', padding: 7 }]}>
                                        <Image source={logoutImage} style={{ alignSelf: 'center', marginStart: 0, height: 21, width: 20 }} />
                                        <Text style={{ fontFamily: 'SFUIText-Semibold', marginStart: 10, marginEnd: 10, fontSize: diamens.ternarySize }}>{StringsOfLanguages.signOut}</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </ImageBackground>

            </View>

        );
    }
}

const data = [
    { key: 'Android' }, { key: 'iOS' },
]

