import CustToo from '../helper/CustomToolbar';
import colors from '../utils/colors';
import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, StatusBar, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import CommanUtils from '../utils/CommanUtils';
import style from '../utils/style';
import StringsOfLanguages from '../utils/StringsOfLanguages'

var heart = require('../../images/heart.png');

export default class FavoriteVendorsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      languag: true,
    }
  }

  static navigationOptions = {
    drawerIcon: (
      <Image source={heart} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
    )
  };

  title = "Favorite Vender";

  async componentDidMount() {
    var lan = await CommanUtils.getLanguage()
    this.setState({ languag: lan });
  }

  render() {

    let styles;
    if (this.state.languag) {
      styles = style.getStyle(true);
      StringsOfLanguages.setLanguage("en")
    } else {
      styles = style.getStyle(false);
      StringsOfLanguages.setLanguage("ar")
    }

    return (
      <View style={{ flex: 1 }}>

        <StatusBar backgroundColor='#4384FF' />

        <CustToo valueFromParent={this.title} />

        <Text style={[styles.wordNormal, { marginTop: 20, marginBottom: 20 }]}>
          Favorite Vendors
                </Text>

      </View>
    );
  }
}


