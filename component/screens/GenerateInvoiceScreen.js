import CustToo from '../helper/CustomToolbarBackBtn';

import React, { Component } from 'react';
import { View, TouchableOpacity,ScrollView, Text, TextInput, ActivityIndicator, ImageBackground } from 'react-native';
import style from '../utils/style';
import colors from '../utils/colors';
import diamens from '../utils/diamens';
import ApiClient from '../utils/ApiClientService';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
var bgImage = require('../../images/main_bg.png');

var settingIcon = require('../../images/setting.png');

export default class GenerateInvoiceScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            langValue: "en",
            additionalCost: '',
            isInvoiceSent: 'true',
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
            hours_Counter: '00',
            vendorServiceId: 0,
            startPauseToggle: true,
            Authorization: 'thf',
            priceQuote: '0',
            invoiceId: 'nnnn',
            scheduleDate: 'nnnn',
            scheduleTime: 'nnnn',
            serviceType: 'nnnn',
            acType: 'nnnn',
            noOfAc: 1,
            vendorName: 'nnnn',
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        const dataFromPrev = this.props.navigation.getParam('data', 'no data');
        this.setState({
            languag: lan,
            minutes_Counter: dataFromPrev[0].totalMins,
            hours_Counter: dataFromPrev[0].totalHrs,
            vendorServiceId: dataFromPrev[0].vendorServiceId,
            priceQuote: dataFromPrev[0].priceQuote,
            invoiceId: dataFromPrev[0].invoiceId,
            scheduleDate: dataFromPrev[0].scheduleDate,
            scheduleTime: dataFromPrev[0].scheduleTime,
            serviceType: dataFromPrev[0].serviceType,
            acType: dataFromPrev[0].acType,
            noOfAc: dataFromPrev[0].noOfAc,
            vendorName: dataFromPrev[0].vendorName,
            Authorization: "Bearer " + dataFromLocal.token

        });
        console.log("no of ac......", dataFromPrev)
    }


    async hitSendGenerateInvoice() {

        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.generateInvoice(this.state);
            if (responceData != undefined) {
                if (responceData.status) {
                    // await CommanUtils.showAlert(StringsOfLanguages.success, '' + responceData.message)
                    this.goToJobHistory()
                }
                else {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                }
            }
        }
        this.setState({ loading: false })
    };

    goToJobHistory() {
        this.props.navigation.navigate("SuccessInvoiceSendScreen");
    }

    render() {
        const { languag, noOfAc, priceQuote, additionalCost, invoiceId, scheduleDate, serviceType, loading, acType, vendorName } = this.state;
        let styles;
        let progressBar;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            // alert("english")
        } else {
            // alert("arabic "+this.state.languag)
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }


        var subTotal = Number(priceQuote) + Number(additionalCost);


        if (loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }


        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.invoiceDetails} />


                <ImageBackground source={bgImage} style={{ flex: 1 }}>
                    <ScrollView>
                    <View>
                        <View style={{ margin: 15 }}>

                            <View style={[styles.flexDirect, { alignItems: 'center', }]}>

                                <View style={{ marginLeft: 0, flex: 1 }}>
                                    <Text style={[styles.title, styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, }]}>{invoiceId}</Text>
                                    <Text style={[styles.title, styles.textAlignn, { fontFamily: 'SFUIText-Bold', marginTop: 5, fontSize: diamens.ternarySize, }]}>{vendorName}</Text>

                                </View>

                                <View style={{ marginLeft: 0, }}>
                                    <Text style={[styles.title, { fontFamily: 'SFUIText-Regular', }]}>{StringsOfLanguages.date} :</Text>
                                    <Text style={[styles.title], { fontFamily: 'SFUIText-Bold', }}>{scheduleDate}</Text>

                                </View>

                            </View>

                            <View style={[styles.flexDirect, { marginTop: 10 }]}>
                                <View style={{ flex: 1 }}>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF', fontSize: 25, marginTop: 10 }]}>SAR {subTotal}</Text>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF' }]}>{StringsOfLanguages.totalAmount}</Text>
                                </View>
                                <View>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: diamens.tinySize }]}>{StringsOfLanguages.typeOfAc} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }]}>{acType}</Text>
                                    <Text numberOfLines={1} style={[{ fontFamily: 'SFUIText-Regular', textAlign: 'center', marginTop: 10, fontSize: diamens.tinySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                                    <Text numberOfLines={1} style={[{ textAlign: 'center', fontFamily: 'SFUIText-Bold', fontSize: diamens.tinySize }]}> {noOfAc} </Text>
                                </View>
                            </View>


                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF', fontSize: diamens.ternarySize, marginTop: 25 }]}>{StringsOfLanguages.serviceType}</Text>
                            <View style={[styles.flexDirect, { marginTop: 5 }]}>
                                <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, }]}>{serviceType}</Text>
                                <Text numberOfLines={1} style={{ fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize, }}>SAR {priceQuote}</Text>
                            </View>

                            <View style={[styles.flexDirect, { marginTop: 20 }]}>
                                <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', alignSelf: 'center' }]}>{StringsOfLanguages.additionalCost}</Text>

                                <View style={[styles.flexDirect, { alignItems: 'center', marginTop: 0, }]}>

                                    <Text style={{ fontFamily: 'SFUIText-Bold', marginStart: 5, marginEnd: 5 }}>SAR</Text>

                                    <TextInput
                                        style={[{ height: 40, borderRadius: 10, paddingStart: 20, paddingEnd: 20, marginStart: 0, marginEnd: 0, borderColor: '#4384FF', borderWidth: 1 }]}
                                        placeholder="0"
                                        keyboardType="numeric"
                                        backgroundColor='white'
                                        placeholderTextColor="#000000"
                                        onChangeText={text => this.setState({ additionalCost: text })} />

                                </View>

                            </View>

                        </View>

                        <View style={{ height: 1, backgroundColor: '#000', marginTop: 30 }}></View>

                        <View style={[styles.flexDirect, { margin: 20 }]}>
                            <Text numberOfLines={1} style={[styles.textAlignn, { fontSize: diamens.ternarySize, flex: 1, fontFamily: 'SFUIText-Bold', }]}>{StringsOfLanguages.totalAmount}</Text>
                            <Text numberOfLines={1} style={{ fontSize: diamens.ternarySize, fontFamily: 'SFUIText-Bold', color: colors.colorPrimaryDark }}>{StringsOfLanguages.currencySymbol} {subTotal}</Text>
                        </View>

                        {progressBar}

                        <TouchableOpacity style={[styles.btnPress, { marginTop: 80, width: '92%', alignSelf: 'center' }]}
                            onPress={() => {
                                this.hitSendGenerateInvoice()
                            }}>
                            <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 12 }}>{StringsOfLanguages.sendInvoiceCap}</Text>
                        </TouchableOpacity>
                    </View>
                    </ScrollView>
                </ImageBackground>

            </View>
        );
    }
}



