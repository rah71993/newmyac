import CustToo from '../helper/CustomToolbar';

import React, { Component } from 'react';
import { View, Image, FlatList, StatusBar, ActivityIndicator, ImageBackground } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import ApiClient from '../utils/ApiClientService';
import CommanUtils from '../utils/CommanUtils';
import CustomRow from '../list_row/HelpPageRow';
import StringsOfLanguages from '../utils/StringsOfLanguages'

var bgImage = require('../../images/main_bg.png');

export default class HelpScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            languagValue: "en",
            loading: false,
            pageListData: [],
        }
    }
    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.hitPageList()
    }

    async hitPageList() {

        this.setState({ loading: true })
        var responceData = await ApiClient.getPageList(this.state);
        if (responceData != undefined) {
            this.setState({ pageListData: responceData.data })
        }
        this.setState({ loading: false })
    };


    render() {
        let styles;
        let pageList;
        let right_icon;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            right_icon = require('../../images/arrow-right.png')
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
            right_icon = require('../../images/left-arrow.png')
        }

        if (this.state.loading) {
            pageList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            pageList = (
                <FlatList
                    style={{ paddingBottom: 50 }}
                    data={this.state.pageListData}
                    // keyExtractor={item => item.key.toString()}
                    renderItem={({ item }) => {
                        return <CustomRow item={item} right_icon={right_icon} styles={styles} props={this.props} />
                    }
                    }
                />
            );
        }

        return (
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor='#4384FF' />

                <CustToo valueFromParent={StringsOfLanguages.help} />
                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    {pageList}

                </ImageBackground>
            </View>
        );
    }
}

