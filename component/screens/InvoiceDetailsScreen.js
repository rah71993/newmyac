import CustToo from '../helper/CustomToolbarBackBtn';
import React, { Component } from 'react';
import { View, TouchableOpacity, Text, ScrollView, ActivityIndicator, ImageBackground } from 'react-native';
import style from '../utils/style';
import colors from '../utils/colors';
import dimens, { primarySize, ternarySize, tinySize } from '../utils/diamens';

import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import ApiClient from '../utils/ApiClientService';


import {
    MFPaymentRequest,
    MFCustomerAddress,
    MFExecutePaymentRequest,
    Response,
    MFLanguage,
    MFMobileCountryCodeISO,
    MFCurrencyISO,
    MFPaymentype,
    MFInitiatePayment,
    MFKeyType
} from 'myfatoorah-reactnative';
import diamens from '../utils/diamens';

var bgImage = require('../../images/main_bg.png');


export default class InvoiceDetailsScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            vendorServiceId: 0,
            vendorId: 0,
            Authorization: 'thf',
            profileImage: 'thf',
            dataResponse: [],
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        const { vendorServiceId, vendorId, profileImage } = this.props.route.params;

        this.setState({ languag: lan, vendorServiceId: vendorServiceId, vendorId: vendorId, profileImage: profileImage, Authorization: "Bearer " + dataFromLocal.token });

    }


    navigateToSuccessScreen() {
        this.props.navigation.navigate("PaymentMyFTW",this.props.route.params)
    }

    showLoading() {
        this.setState({ loading: true })
    }
    hideLoading() {
        this.setState({ loading: false })
    }

    render() {
        const { vendorServiceId, invoiceId, scheduleDate, totalAmount, serviceType, additionalPrice, paymentStatus, vendorName, priceQuote, acType, noOfAc } = this.props.route.params;

        let styles;
        let progressBar;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (this.state.loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }


        var subTotal = Number(totalAmount) + Number(additionalPrice);

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.invoiceDetails} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>
                    <ScrollView>
                        <View>
                            <View style={{ margin: 15 }}>

                                <View style={[styles.flexDirect, { alignItems: 'center', }]}>

                                    <View style={{ marginLeft: 0, flex: 1 }}>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }]}>{invoiceId}</Text>
                                    </View>

                                    <View style={{ marginLeft: 0, }}>
                                        <Text style={{ fontFamily: 'SFUIText-Regular', fontSize: diamens.ternarySize }}>{StringsOfLanguages.date} :</Text>
                                        <Text style={{ fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }}>{scheduleDate}</Text>
                                    </View>

                                </View>

                                <View style={[styles.flexDirect, { alignItems: 'center', }]}>

                                    <View style={{ flex: 1 }}>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 10 }]}>{vendorName}</Text>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF', fontSize: 25, marginTop: 10 }]}>SAR {subTotal}</Text>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', }]}>{StringsOfLanguages.totalAmount}</Text>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF', marginTop: 25 }]}>{StringsOfLanguages.serviceType}</Text>
                                    </View>

                                    <View>
                                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: diamens.tinySize }]}>{StringsOfLanguages.typeOfAc} :</Text>
                                        <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: dimens.tinySize }]}>{acType}</Text>
                                        <Text numberOfLines={1} style={[{ fontFamily: 'SFUIText-Regular', textAlign: 'center', marginTop: 10, fontSize: dimens.tinySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                                        <Text numberOfLines={1} style={[{ textAlign: 'center', fontFamily: 'SFUIText-Bold', fontSize: dimens.tinySize }]}> {noOfAc} </Text>
                                    </View>
                                </View>

                                <View style={[styles.flexDirect, { marginTop: 10 }]}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }]}>{serviceType}</Text>
                                    <Text numberOfLines={1} style={{ fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }}>{StringsOfLanguages.currencySymbol} {priceQuote}</Text>
                                </View>

                                <View style={[styles.flexDirect, { marginTop: 15, fontSize: diamens.ternarySize }]}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }]}>{StringsOfLanguages.additionalCost}</Text>
                                    <Text numberOfLines={1} style={{ fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }}>{StringsOfLanguages.currencySymbol} {additionalPrice}</Text>
                                </View>

                            </View>

                            <View style={{ height: 1, backgroundColor: '#000', marginTop: 30 }}></View>

                            <View style={[styles.flexDirect, { margin: 10 }]}>
                                <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }]}>{StringsOfLanguages.totalAmount}</Text>
                                <Text numberOfLines={1} style={{ fontFamily: 'SFUIText-Bold', color: colors.colorPrimaryDark, fontSize: diamens.ternarySize }}>{StringsOfLanguages.currencySymbol} {subTotal}</Text>
                            </View>

                            {progressBar}

                            {paymentStatus === "Pending" &&
                                <TouchableOpacity style={[styles.btnPress, { marginTop: 80, width: '92%', alignSelf: 'center' }]}
                                    onPress={() => {
                                        // alert('Alert with one button');
                                        // this.executePayment(subTotal)
                                        this.navigateToSuccessScreen()
                                    }}>
                                    <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: tinySize }}>{StringsOfLanguages.payNowCap}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>
        );
    }
}


