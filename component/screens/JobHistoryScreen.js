import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { View, Image, FlatList, Text, ActivityIndicator, ImageBackground,Platform, TouchableOpacity, Share } from 'react-native';

import CommanUtils from '../utils/CommanUtils';
import NewJobsRow from '../list_row/NewJobsRow';
import UpcomingJobsRow from '../list_row/UpcomingJobsRow';
import CompletedJobsRow from '../list_row/CompletedJobsRow';
import ApiClient from '../utils/ApiClientService';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import CardView from 'react-native-cardview'
import diamens, { tinySize } from '../utils/diamens';

import CustToo from '../helper/CustomToolbar';
import style from '../utils/style';
import colors from '../utils/colors';
import { ScrollView } from 'react-native-gesture-handler';

var logoImage = require('../../images/logo.png');
var bgImage = require('../../images/main_bg.png');
var header_bg = require('../../images/header_bg.png');
var rateUs = require('../../images/rate_white.png');
var shareApp = require('../../images/share_app.png');


let styles = style.getStyle(true);

export default class JobHistoryScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            status: true,
            languag: true,
            langValue: "en",
            loginType:"vendor",
            newJobList: true,
            upcomingJobList: false,
            completedJobList: false,
            loading: false,
            styleNew: styles.enableTab,
            textNew: styles.enableText,
            isNew: true,
            styleScheduled: styles.disableTab,
            textScheduled: styles.disableText,
            isScheduled: false,
            styleFulfilled: styles.disableTab,
            textFulfilled: styles.disableText,
            isFulfilled: false,
            Authorization: 'thf',
            orderType: 'new',
            data: []
        }
        this.hitJobsHistory = this.hitJobsHistory.bind(this)
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({ languag: lan, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }

        this.allDataWork()

        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
        this.allDataWork()
        ApiClient.getNotification(this.state);
        });
    }

    async allDataWork(){
        const orderType = this.props.navigation.getParam('jobType', 'new');
        if (orderType === 'new') {
            this.onNewPress()
        }else if (orderType === 'schedule') {
            this.onScheduledPress()
        }else if (orderType === 'completed') {
            this.onFulfiledPress()
        }
    }

    async hitJobsHistory() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.jobHistory(this.state);
            if (responceData != undefined) {
                this.setState({ data: responceData.data })
            }
            this.setState({ loading: false })
        }
    };

    async refreshD(valu) {
        let allItems = [...this.state.data];
        let filteredItems = allItems.filter(item => item.vendorServiceId !== valu.vendorServiceId);
        this.setState({ data: filteredItems })
    }

    onNewPress(orderType) {
        this.setState({
            newJobList: true, upcomingJobList: false, completedJobList: false,
            styleNew: styles.enableTab,
            textNew: styles.enableText,
            isNew: true,
            styleScheduled: styles.disableTab,
            textScheduled: styles.disableText,
            isScheduled: false,
            styleFulfilled: styles.disableTab,
            textFulfilled: styles.disableText,
            isFulfilled: false,
            orderType: 'new',
        })
        this.hitJobsHistory();
    }

    onScheduledPress(orderType) {
        this.setState({
            newJobList: false, upcomingJobList: true, completedJobList: false,
            styleNew: styles.disableTab,
            textNew: styles.disableText,
            isNew: false,
            styleScheduled: styles.enableTab,
            textScheduled: styles.enableText,
            isScheduled: true,
            styleFulfilled: styles.disableTab,
            textFulfilled: styles.disableText,
            isFulfilled: false,
            orderType: 'schedule',
        })
        this.hitJobsHistory();
    }

    onFulfiledPress(orderType) {
        this.setState({
            newJobList: false, upcomingJobList: false, completedJobList: true,
            styleNew: styles.disableTab,
            textNew: styles.disableText,
            isNew: false,
            styleScheduled: styles.disableTab,
            textScheduled: styles.disableText,
            isScheduled: false,
            styleFulfilled: styles.enableTab,
            textFulfilled: styles.enableText,
            isFulfilled: true.langu,
            orderType: 'completed',
        })
        this.hitJobsHistory();
    }
    navigateToRateApp() {
        this.props.navigation.navigate("RateUs")
    }



    render() {
        let showlist = null;
        let styles;
        const { languag, loading, newJobList, upcomingJobList, completedJobList, data, styleNew, styleScheduled, styleFulfilled, textNew, textScheduled, textFulfilled, isNew, isScheduled, isFulfilled } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }
        if (loading) {
            showlist = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else if (newJobList && data && data.length) {
            showlist = (
                <View style={{ marginTop: 15 }}>
                    <FlatList
                        data={data}
                        // keyExtractor={item => item.key.toString()}
                        renderItem={({ item }) => {
                            return <NewJobsRow item={item} styles={styles} langu={this.state.languag} props={this.props} />
                        }} />
                </View>
            );
        }
        else if (upcomingJobList && data && data.length) {
            showlist = (
                <View style={{ marginTop: 15 }}>
                    <FlatList
                        data={data}
                        // keyExtractor={item => item.key.toString()}
                        renderItem={({ item }) => {
                            return <UpcomingJobsRow item={item} styles={styles} props={this.props} state={this.state} click={this.hitJobsHistory} />
                        }} />
                </View>
            );
        } else if (completedJobList && data && data.length) {
            showlist = (
                <View style={{ marginTop: 15 }}>
                    <FlatList
                        data={data}
                        // keyExtractor={item => item.key.toString()}
                        renderItem={({ item }) => {
                            return <CompletedJobsRow item={item} styles={styles} props={this.props} state={this.state} />
                        }} />
                </View>
            );
        } else {
            showlist = (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>{StringsOfLanguages.noResultFound}</Text>
                </View>
            );
        }

        return (
            <View style={{ flex: 1 }}>
                <CustToo valueFromParent={StringsOfLanguages.jobHistory} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>
                    <ScrollView>
                        <View style={{ flex: 1 }}>
                            <Image source={logoImage} style={[styles.logo_style, { marginTop: 20, alignSelf: 'center' }]} />


                            <View style={[styles.acTypeContainer, { marginTop: 10 }]}>

                                <View style={{ flex: 1 }}>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.onNewPress()
                                        }}>
                                        <CardView
                                            style={[styleNew, { flex: 0, width: '95%', borderRadius: 5, height: 45, alignSelf: 'center' }]}
                                            cardElevation={1}
                                            cardMaxElevation={1}
                                            cornerRadius={10}>

                                            <Text style={[textNew, {textTransform: 'uppercase', padding: 0, fontSize: 10, textAlign: 'center' }]}>{StringsOfLanguages.newJobs}</Text>
                                        </CardView>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.onScheduledPress()
                                        }}>
                                        <CardView
                                            style={[styleScheduled, { flex: 0, width: '95%', padding: 0, borderRadius: 5, height: 45, alignSelf: 'center' }]}
                                            cardElevation={1}
                                            cardMaxElevation={1}
                                            cornerRadius={10}>
                                            <Text style={[textScheduled, {textTransform: 'uppercase', padding: 0, fontSize: 10, textAlign: 'center' }]}>{StringsOfLanguages.scheduledJobs}</Text>
                                        </CardView>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.onFulfiledPress()
                                        }}>
                                        <CardView
                                            style={[styleFulfilled, { flex: 0, width: '95%', borderRadius: 5, height: 45, alignSelf: 'center' }]}
                                            cardElevation={1}
                                            cardMaxElevation={1}
                                            cornerRadius={10}>
                                            <Text style={[textFulfilled, {textTransform: 'uppercase', padding: 0, fontSize: 10, textAlign: 'center' }]}>{StringsOfLanguages.completedJobs} </Text>
                                        </CardView>
                                    </TouchableOpacity>
                                </View>

                            </View>

                            {showlist}
                        </View>

                    </ScrollView>
                </ImageBackground>

                <ImageBackground style={[styles.navBar,{justifyContent:'flex-start',alignItems:'flex-start'}]} source={header_bg}>

                    <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                        <View style={{ alignItems: 'center' }} onStartShouldSetResponder={() => this.navigateToRateApp()
                        }>
                            <Image source={rateUs} style={[styles.ImageIconStyle]} />
                            <Text style={{fontFamily: 'SFUIText-Semibold', padding: 3, fontSize: diamens.tinySize, color: 'white' }}>{StringsOfLanguages.rateUs}</Text>
                        </View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                        <View style={{ alignItems: 'center' }} onStartShouldSetResponder={
                            () => {
                                onShare()
                            }
                        }>
                            <Image source={shareApp} style={[styles.ImageIconStyle], { resizeMode: 'contain' }} />
                            <Text style={{fontFamily: 'SFUIText-Semibold', padding: 3, fontSize: diamens.tinySize, color: 'white' }}>{StringsOfLanguages.shareOurApp}</Text>
                        </View>
                    </View>

                </ImageBackground>

            </View>
        );
    }
}

const onShare = () =>{

    let  text = 'Please install this app '
    if(Platform.OS === 'android')
        text = text.concat('https://play.google.com/store/apps/details?id=com.newmyac&hl=en')
    else
        text = text.concat('https://apps.apple.com/us/app/myac-app/id1564790015')

    Share.share({
        subject: 'Download MyAc App Now',
        title: 'Download MyAc App Now',
        message: text,
        url:'app://MyAc',

    }, {
        // Android only:
        dialogTitle: 'Share MyAc App',
        // iOS only:
        excludedActivityTypes: []
    })
}