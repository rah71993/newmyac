
import React, { Component } from 'react';
import { ImageBackground, Image, Text, TouchableOpacity, StatusBar, View } from 'react-native';
import style from '../utils/style';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import colors from '../utils/colors';
import dimens from '../utils/diamens';
import GeneralStatusBarColor from "react-native-general-statusbar";
import CommanUtils from '../utils/CommanUtils';

var bgImage = require('../../images/main_bg.png');
var logoImage = require('../../images/logo.png');

export default class LanguageScreen extends Component {


    constructor(props) {
        super(props)
        this.state = {
            languag: true,
        }
    }


    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }

    navigateToUserVendor() {
        this.props.navigation.navigate("ChooseUserVender")
    }

    render() {
        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            // alert("english")
        } else {
            // alert("arabic "+this.state.languag)
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        return (
            <View style={{ flex: 1 }}>

                <GeneralStatusBarColor
                    backgroundColor={colors.statusColor}
                    barStyle="light-content"
                />


                {/* <View style={styles.status_styl}></View> */}

                <ImageBackground style={[styles.container,{justifyContent:'center'}]} source={bgImage}>

                    {/* <StatusBarBackground style={{ backgroundColor: 'midnightblue' }} /> */}

                
                    <Image source={logoImage} style={styles.logo_style} />


                    <Text style={[styles.wordNormal, { marginTop: 40, marginBottom: 20 }]}>
                        {StringsOfLanguages.chooseLanguage}
                    </Text>

                    <TouchableOpacity style={[styles.btnPress]}
                        onPress={() => {
                            this.navigateToUserVendor();
                            CommanUtils.setLanguage("en");
                        }}>
                        <Text style={styles.textButtonPress}>{StringsOfLanguages.englishCap}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.btnBorder, { marginTop: 20 }]}
                        onPress={() => {
                            this.navigateToUserVendor();
                            CommanUtils.setLanguage("ar");
                        }}>
                        <Text style={{ fontFamily: 'SFUIText-Bold', fontSize: dimens.ternarySize, color: colors.colorPrimaryDark, }}>{StringsOfLanguages.arabic}</Text>
                    </TouchableOpacity>


                </ImageBackground>
            </View>
        );
    }
}


