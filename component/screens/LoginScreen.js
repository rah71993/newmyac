import React, { Component } from 'react';
import { View, ActivityIndicator, Image, Text, TouchableOpacity, ScrollView, ImageBackground ,Platform} from 'react-native';
import style from '../utils/style';
import colors from '../utils/colors';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import GeneralStatusBarColor from "react-native-general-statusbar";
import StringsOfLanguages from '../utils/StringsOfLanguages';
import IntlPhoneInput from 'react-native-intl-phone-input';
import Geolocation from '@react-native-community/geolocation';
import CustToo from '../helper/CustomToolbarLoginBackBtn';

var bgImage = require('../../images/main_bg.png');
var logoImage = require('../../images/logo.png');

export default class LoginScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            phoneNumber: "",
            phoneCode: "966",
            isValidPhone: false,
            loading: false,
            languag: true,
            signInAs: "user",
            loginTypeId: 2,
            latitude: 0.0,
            longitude: 0.0,
            id: 0,
            token: 'fdghdgh',
            deviceType:"ios",
            langValue:"en",
        }
    }

    async componentDidMount() {
        var signInAs = await CommanUtils.getLoginType();
        var lan = await CommanUtils.getLanguage()
        var token = await CommanUtils.getToken()
        if (token == null) {
            token = 'jkhkjh'
        }
        var deviceTypep= Platform.OS === 'android' ? 'android' : 'ios'
        this.setState({ signInAs: signInAs, languag: lan, token: token,deviceType:deviceTypep });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        if (signInAs == 'user') { this.setState({ loginTypeId: 2 }) } else { this.setState({ loginTypeId: 3 }) }
        this.showLocation();
    }

    navigateSecondScreen() {
        this.props.navigation.navigate("OtpScreen", { id: this.state.id, });
    }


    async onSignInClick() {
        const { isValidPhone, phoneNumber } = this.state;
        if (phoneNumber != undefined && phoneNumber.trim()) {
            var isConnected = await CommanUtils.checkInternet()
            if (isConnected) {
                this.setState({ loading: true })
                var responceData = await ApiClient.getLogin(this.state);
                this.setState({ loading: false })
                if (responceData != undefined) {
                    if (responceData.status) {
                        this.setState({ id: responceData.id })
                        this.navigateSecondScreen()
                    }
                    else {
                        CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                    }
                }
            }
        }
        else {
            CommanUtils.showAlert(StringsOfLanguages.error, StringsOfLanguages.doesNotEmpty)
        }

    };


    onChangeText = ({ dialCode, unmaskedPhoneNumber, phoneNumber, isVerified }) => {
        this.setState({ phoneCode: dialCode, phoneNumber: unmaskedPhoneNumber, isValidPhone: isVerified })
        // console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
    };


    showLocation() {
        Geolocation.getCurrentPosition(info => {
            console.log(info)
            this.setState({ latitude: info.coords.latitude, longitude: info.coords.longitude, })
        }
        );
    }

    render() {
        let styles;
        let progressBar = null;

        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (this.state.loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }





        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={""} />


                <ImageBackground style={styles.container} source={bgImage}>
                    <ScrollView style={{ width: '100%' }} contentContainerStyle={{ marginBottom: 100, justifyContent: "center", alignItems: 'center', flex: 1 }}>
                        <View style={{ alignItems: 'center' }}>
                            <Image source={logoImage} style={styles.logo_style} />

                            <Text style={[styles.wordNormal, { marginTop: 40, marginBottom: 20 }]}>{StringsOfLanguages.signInUsingPhoneNumber} </Text>


                            <View style={[styles.inputView, { paddingEnd: 0, paddingStart: 0 }]} >

                                <IntlPhoneInput
                                    style={[styles.inputTxt, { fontFamily: 'SFUIText-Regular', }]}
                                    placeholder="05XXXXXXXX"
                                    placeholderTextColor={colors.black}
                                    defaultCountry="SA"
                                    onChangeText={this.onChangeText} />
                            </View>

                            {progressBar}

                            <TouchableOpacity style={[styles.btnPress, { marginTop: 20 }]} onPress={() => {
                                this.onSignInClick()
                            }}>
                                <Text style={[styles.textButtonPress, { width: '100%', textAlign: 'center' }]}>{StringsOfLanguages.nextCap}</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>
        );
    }
}

