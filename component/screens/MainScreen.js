import 'react-native-gesture-handler';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// import { NavigationContainer } from "@react-navigation/native";
// import { createStackNavigator } from "@react-navigation/stack";
// import { createDrawerNavigator } from "@react-navigation/drawer";

import CustomMenuSidebar from '../helper/CustomMenuSidebar';
import CommanUtils from '../utils/CommanUtils';
import MyOrder from './MyOrdersScreen';
import RateUs from './RateUsScreen';
import Help from './HelpScreen';
import Welcome from './WelcomeScreen';
import TermAndConditionScreen from './TermAndConditionScreen';
import PrivacyPolicyScreen from './PrivacyPolicyScreen';
import Profile from './ProfileScreen';
import ServiceTypeScreen from './ServiceTypeScreen';
import ServiceRequestScreen from './ServiceRequestScreen';
import VendorQuotesScreen from './VendorQuotesScreen';
import WebViewScreen from './WebViewScreen';
import InvoiceDetailsScreen from './InvoiceDetailsScreen';
import ScheduleServiceScreen from './ScheduleServiceScreen';
import EditProfileScreen from './EditProfileScreen';
import PaymentDash from '../paymentDashboard/PaymentDash';
import PlaceAutoCompleteScreen from './PlaceAutoCompleteScreen';
import ApiClient from '../utils/ApiClientService';

import React, { Component } from 'react';
import { View, Image } from 'react-native';
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import SuccessRequestScreen from './SuccessRequestScreen';
import SuccessScheduleScreen from './SuccessScheduleScreen';
import NotificationScreen from './NotificationScreen';
import MapViewM from './MapViewPickLocation';
import LanguageScreen from './LanguageScreen';
import SettingsScreen from './SettingsScreen';
import App from '../../App';
import CovidScreen from './CovidScreen';
import ChooseAdressScreen from './ChooseAddressScreen';

var homeIcon = require('../../images/home.png');
var help = require('../../images/help.png');
var rate = require('../../images/rate.png');
var orderIcon = require('../../images/order_history.png');
var settingIcon = require('../../images/setting.png');
var privacyPolicyIcon = require('../../images/privacy_policy.png');
var terms_and_conditionsIcon = require('../../images/terms_and_conditions.png');


export default class MainScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
        }
    }


    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan })
    }



    render() {
        let styles;
        const { languag } = this.state;

        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        const initialRoutNam = this.props.navigation.getParam('foo', 'Welcome');
        const orderType = this.props.navigation.getParam('orderType', 'new');



        const MyDrawerNavigator = createDrawerNavigator(
            {
                "Welcome": {
                    navigationOptions: {
                        drawerLabel: languag ? "Welcome" : "أهلا بك",
                        drawerIcon: (
                            <Image source={homeIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
                        ),
                    },
                    screen: Welcome
                },
                "MyOrders": {
                    navigationOptions: {
                        drawerLabel: languag ? "My Orders" : "طلباتي",
                        drawerIcon: (
                            <Image source={orderIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', }}></Image>
                        ),
                    },
                    screen: MyOrder
                },
                "RateUs": {
                    navigationOptions: {
                        drawerLabel: languag ? "RateUs" : "قيمنا",
                        drawerIcon: (
                            <Image source={rate} style={{ width: 20, height: 20, tintColor: '#4384FF' }}></Image>
                        ),
                    },
                    screen: RateUs
                },
                "Help": {
                    navigationOptions: {
                        drawerLabel: languag ? "Help" : "مساعدة",
                        drawerIcon: (
                            <Image source={help} style={{ width: 20, height: 20, tintColor: '#4384FF' }}></Image>
                        ),
                    },
                    screen: Help
                },
                "Settings": {
                    navigationOptions: {
                        drawerLabel: languag ? "Settings" : "إعدادات",
                        drawerIcon: (
                            <Image source={settingIcon} style={{ width: 20, height: 20, tintColor: '#4384FF' }}></Image>
                        ),
                    },
                    screen: SettingsScreen
                },
                "TermAndCondition": {
                    navigationOptions: {
                        drawerLabel: languag ? "Terms and Condition" : "الشروط والأحكام",
                        drawerIcon: (
                            <Image source={terms_and_conditionsIcon} style={{ width: 20, height: 20, tintColor: '#4384FF' }}></Image>
                        ),
                    },
                    screen: TermAndConditionScreen
                },
                "PrivacyPolicy  ": {
                    navigationOptions: {
                        drawerLabel: languag ? "Privacy Policy" : "سياسة الخصوصية",
                        drawerIcon: (
                            <Image source={privacyPolicyIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', resizeMode: 'contain' }}></Image>
                        ),
                    },
                    screen: PrivacyPolicyScreen
                },
            },
            {
                initialRouteName: initialRoutNam,
                initialRouteParams: { orderType: orderType },
                contentComponent: CustomMenuSidebar,
                drawerPosition: languag ? 'left' : 'right',
                contentOptions: {
                    activeTintColor: colors.black,
                    itemsContainerStyle: {
                        // opacity: 1
                    },
                    iconContainerStyle: {

                        // opacity: 1
                    },
                    itemStyle: {
                        flexDirection: languag ? 'row' : 'row-reverse',
                    },
                    labelStyle: styles.drawerItemStyle
                },
            },
        );

        const DrawerNavigatorExample = createStackNavigator({
            Drawer: {
                screen: MyDrawerNavigator,
                navigationOptions: { headerShown: false, }
            },
            PrivacyPolicyScreen: {
                screen: PrivacyPolicyScreen,
                navigationOptions: { headerShown: false, },
            },
            TermAndConditionScreen: {
                screen: TermAndConditionScreen,
                navigationOptions: { headerShown: false, },
            },
            Profile: {
                screen: Profile,
                navigationOptions: { headerShown: false, },
            },
            ServiceTypeScreen: {
                screen: ServiceTypeScreen,
                navigationOptions: { headerShown: false, },
            },
            ServiceRequestScreen: {
                screen: ServiceRequestScreen,
                navigationOptions: { headerShown: false, },
            },
            VendorQuotesScreen: {
                screen: VendorQuotesScreen,
                navigationOptions: { headerShown: false, },
            },
            WebViewScreen: {
                screen: WebViewScreen,
                navigationOptions: { headerShown: false, },
            },
            InvoiceDetailsScreen: {
                screen: InvoiceDetailsScreen,
                navigationOptions: { headerShown: false, },
            },
            ScheduleServiceScreen: {
                screen: ScheduleServiceScreen,
                navigationOptions: { headerShown: false, },
            },
            EditProfileScreen: {
                screen: EditProfileScreen,
                navigationOptions: { headerShown: false, },
            },
            SuccessRequestScreen: {
                screen: SuccessRequestScreen,
                navigationOptions: { headerShown: false, },
            },
            MainScreen: {
                screen: MainScreen,
                navigationOptions: { headerShown: false, },
            },
            SuccessScheduleScreen: {
                screen: SuccessScheduleScreen,
                navigationOptions: { headerShown: false, },
            },
            PaymentDash: {
                screen: PaymentDash,
                navigationOptions: { headerShown: false, },
            },
            NotificationScreen: {
                screen: NotificationScreen,
                navigationOptions: { headerShown: false, },
            },
            MapViewM: {
                screen: MapViewM,
                navigationOptions: { headerShown: false, },
            },
            App: {
                screen: App,
                navigationOptions: { headerShown: false, },
            },
            CovidScreen: {
                screen: CovidScreen,
                navigationOptions: { headerShown: false, },
            },
            ChooseAdressScreen: {
                screen: ChooseAdressScreen,
                navigationOptions: { headerShown: false, },
            },
            PlaceAutoCompleteScreen: {
                screen: PlaceAutoCompleteScreen,
                navigationOptions: { headerShown: false, },
            },
        });
        const MyApp = createAppContainer(DrawerNavigatorExample);


        // const Drawer = createDrawerNavigator();

        // const MyDrawerNavigator = () => {
        //     return (
        //         <Drawer.Navigator
        //             initialRouteName={initialRoutNam}
        //             initialRouteParams={orderType = orderType}
        //             contentComponent={CustomMenuSidebar}
        //             drawerPosition={languag ? 'left' : 'right'}
        //             drawerContentOptions={{
        //                 activeTintColor: colors.black,
        //                 itemsContainerStyle: {
        //                     // opacity: 1
        //                 },
        //                 iconContainerStyle: {
        //                     // opacity: 1
        //                 },
        //                 itemStyle: {
        //                     flexDirection: languag ? 'row' : 'row-reverse',
        //                 }
        //             }}
        //         >
        //             <Drawer.Screen name="Welcome" component={Welcome} options={{
        //                 title: languag ? "Welcome" : "أهلا بك",
        //                 drawerIcon: ({ focused, size }) => (<Image source={homeIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>)
        //             }} />
        //             <Drawer.Screen name="MyOrders" component={MyOrder} options={{
        //                 title: languag ? "My Orders" : "طلباتي",
        //                 drawerIcon: ({ focused, size }) => (<Image source={orderIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>)
        //             }} />
        //             <Drawer.Screen name="RateUs" component={RateUs} options={{
        //                 title: languag ? "RateUs" : "قيمنا",
        //                 drawerIcon: ({ focused, size }) => (<Image source={rate} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>)
        //             }} />
        //             <Drawer.Screen name="Help" component={Help} options={{
        //                 title: languag ? "Help" : "مساعدة",
        //                 drawerIcon: ({ focused, size }) => (<Image source={help} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>)
        //             }} />

        //         </Drawer.Navigator>
        //     );
        // }

        // const Stack = createStackNavigator();

        // const MainStackNavigator = () => {
        //     return (
        //         <Stack.Navigator>
        //             <Stack.Screen name="Drawer" component={MyDrawerNavigator} options={{ headerShown: false }} />
        //             <Stack.Screen name="PrivacyPolicyScreen" component={PrivacyPolicyScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="TermAndConditionScreen" component={TermAndConditionScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
        //             <Stack.Screen name="ServiceTypeScreen" component={ServiceTypeScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="ServiceRequestScreen" component={ServiceRequestScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="VendorQuotesScreen" component={VendorQuotesScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="WebViewScreen" component={WebViewScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="InvoiceDetailsScreen" component={InvoiceDetailsScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="ScheduleServiceScreen" component={ScheduleServiceScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="EditProfileScreen" component={EditProfileScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="SuccessRequestScreen" component={SuccessRequestScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="MainScreen" component={MainScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="SuccessScheduleScreen" component={SuccessScheduleScreen} options={{ headerShown: false }} />
        //             <Stack.Screen name="PaymentSuccessScreenD" component={PaymentSuccessScreenD} options={{ headerShown: false }} />
        //             <Stack.Screen name="MFWebView" component={MFWebView} options={MFWebView.navigationOptions} />
        //         </Stack.Navigator>
        //     );
        // }


        return (

            <View style={{ flex: 1 }}>
                {/* <NavigationContainer>
                    <MainStackNavigator />
                </NavigationContainer> */}

                <MyApp />
            </View>
        );
    }
}





// export default MyApp;




