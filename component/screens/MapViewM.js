
import CustToo from '../helper/CustomToolbarBackBtn';
import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Platform, Linking } from 'react-native';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import MapView from 'react-native-maps';
import Geocoder from 'react-native-geocoding';

var rate = require('../../images/rate.png');

export default class MapViewM extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rated: false,
            languag: true,
            title: "",
            latitude: 37.785834,
            longitude: -122.406417,
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var lat = this.props.navigation.getParam('latitude', 37.78825);
        var lon = this.props.navigation.getParam('longitude', -122.4324);
        var title = this.props.navigation.getParam('title', "no data");

        this.setState({ languag: lan, title: title, latitude: lat, longitude: lon });

        console.log(lat, lon)
    }

    navigateMapDirection() {
        var lat = this.state.latitude
        var lng = this.state.longitude
        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${lat},${lng}`;
        const label = 'End Point';
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });
        Linking.openURL(url);
    }

    changeLocation(location) {
        Geocoder.from(location.latitude, location.longitude)
            .then(json => {
                var addressComponent = json.results[0].formatted_address;
                // console.log(json.results[0].address_components)
                this.setState({ title: addressComponent, latitude: location.latitude, longitude: location.longitude })
            })
            .catch(error => console.warn(error));
    }
    render() {
        let styles;
        const { latitude, longitude, title, languag } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        var markers = [
            {
                latitude: Number(latitude),
                longitude: Number(longitude),
                title: title,
                // subtitle: '1234 Foo Drive'
            }
        ];
        const LATITUDE_DELTA = 0.0043;
        const LONGITUDE_DELTA = 0.0034;

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={title} />

                <MapView
                    style={{ flex: 1 }}
                    minZoomLevel={12}
                    maxZoomLevel={15}
                    // showsUserLocation={true}
                    annotations={markers}
                    initialRegion={{
                        latitude: Number(latitude),
                        longitude: Number(longitude),
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                    }}>
                    <MapView.Marker
                        coordinate={{
                            latitude: Number(latitude),
                            longitude: Number(longitude),
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA
                        }}
                        title={title}
                        onDragEnd={e => {
                            console.log('dragEnd', e.nativeEvent.coordinate);
                            this.changeLocation(e.nativeEvent.coordinate)
                        }}
                        // draggable
                    // description={"description"}
                    />
                </MapView>

                <TouchableOpacity style={[styles.btnPress, { position: 'absolute', bottom: 0, alignSelf: 'center', marginBottom: 20, marginTop: 20 }]} onPress={() => {
                    this.navigateMapDirection()
                }}>
                    <Text style={styles.textButtonPress}>{StringsOfLanguages.directionCap}</Text>
                </TouchableOpacity>

            </View >
        );
    }
}
