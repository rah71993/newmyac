
import CustToo from '../helper/CustomToolbarBackBtn';
import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import MapView from 'react-native-maps';
import Geocoder from 'react-native-geocoding';

var rate = require('../../images/rate.png');

export default class MapViewM extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rated: false,
            languag: true,
            title: "",
            latitude: 0.0,
            longitude: 0.0,
            displayMapView: false
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var lat = this.props.navigation.getParam('latitude', 0.0);
        var lon = this.props.navigation.getParam('longitude', 0.0);
        var title = this.props.navigation.getParam('title', "no data");

        this.setState({ languag: lan, title: title, latitude: lat, longitude: lon, displayMapView: false }, () => { this.setState({ displayMapView: true }) });

    }

    navigateMapDirection() {
        this.props.navigation.navigate("Welcome", {
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            title: this.state.title,
        })
    }

    changeLocation(location) {
        Geocoder.from(location.latitude, location.longitude)
            .then(json => {
                var addressComponent = json.results[0].formatted_address;
                // console.log(json.results[0].address_components)
                this.setState({ title: addressComponent, latitude: location.latitude, longitude: location.longitude, displayMapView: false }, () => { this.setState({ displayMapView: true }) })
            })
            .catch(error => console.warn(error));
    }
    render() {
        let styles;
        const { latitude, longitude, title, languag } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        var markers = [
            {
                latitude: latitude,
                longitude: longitude,
                title: title,
                // subtitle: '1234 Foo Drive'
            }
        ];
        const LATITUDE_DELTA = 0.0043;
        const LONGITUDE_DELTA = 0.0034;

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={title} />
                {true === this.state.displayMapView &&
                    <MapView
                        style={{ flex: 1 }}
                        minZoomLevel={12}
                        maxZoomLevel={15}
                        // showsUserLocation={true}
                        onPress={(e) => this.changeLocation(e.nativeEvent.coordinate)}
                        annotations={markers}
                        initialRegion={{
                            latitude: latitude,
                            longitude: longitude,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA
                        }}>
                        <MapView.Marker
                            draggable
                            title={title}
                            coordinate={{
                                latitude: latitude,
                                longitude: longitude,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA
                            }}
                            onDragEnd={e => {
                                console.log('dragEnd', e.nativeEvent.coordinate);
                                this.changeLocation(e.nativeEvent.coordinate)

                            }}
                        // description={"description"}
                        />
                    </MapView>}

                <TouchableOpacity style={[styles.btnPress, { position: 'absolute', bottom: 0, alignSelf: 'center', marginBottom: 20, marginTop: 20 }]} onPress={() => {
                    this.navigateMapDirection()
                }}>
                    <Text style={styles.textButtonPress}>{StringsOfLanguages.selectCap}</Text>
                </TouchableOpacity>

            </View >
        );
    }
}
