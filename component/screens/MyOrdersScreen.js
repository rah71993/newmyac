
import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator, Text, ImageBackground, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import NewOrdersRow from '../list_row/NewOrdersRow';
import ScheduledOrderRow from '../list_row/ScheduledOrdersRow';
import FulfilledOrdersRow from '../list_row/FulfilledOrdersRow';
import ApiClient from '../utils/ApiClientService';
import colors from '../utils/colors';
import style from '../utils/style';
import CustToo from '../helper/CustomToolbar';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import { ternarySize, tinySize } from '../utils/diamens';
var bgImage = require('../../images/main_bg.png');
let styles = style.getStyle(true);

export default class VendorQuotesScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            langValue: "en",
            Authorization: 'bb',
            myOrderData: [],
            styleNew: styles.enableTab,
            textNew: styles.enableText,
            isNew: true,
            styleScheduled: styles.disableTab,
            textScheduled: styles.disableText,
            isScheduled: false,
            styleFulfilled: styles.disableTab,
            textFulfilled: styles.disableText,
            isFulfilled: false,
            jobType: 'new',

        }
        this.hitNewOrder = this.hitNewOrder.bind(this)
        this.hitMyOrder = this.hitMyOrder.bind(this)
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({ languag: lan, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }

        this.allDataWork()

        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.allDataWork()
        });
    }

    async allDataWork() {
        const orderType = this.props.navigation.getParam('orderType', 'new');
        if (orderType == 'new') {
            this.onNewPress()
        }
        else if (orderType == 'schedule') {
            this.onScheduledPress()
        }
        else if (orderType == 'completed') {
            this.onFulfiledPress()
        }

    }

    async hitMyOrder() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.getMyOrder(this.state);
            if (responceData != undefined) {
                this.setState({ myOrderData: responceData.order })
            }
            this.setState({ loading: false })
        }
    };
    async hitNewOrder() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.newOrder(this.state);
            if (responceData != undefined) {
                this.setState({ myOrderData: responceData.data })
            }
            this.setState({ loading: false })
        }
    };

    onNewPress() {
        this.setState({
            styleNew: styles.enableTab,
            textNew: styles.enableText,
            isNew: true,
            styleScheduled: styles.disableTab,
            textScheduled: styles.disableText,
            isScheduled: false,
            styleFulfilled: styles.disableTab,
            textFulfilled: styles.disableText,
            isFulfilled: false,
            jobType: 'new',
        })
        this.hitNewOrder()
    }

    onScheduledPress() {
        this.setState({
            styleNew: styles.disableTab,
            textNew: styles.disableText,
            isNew: false,
            styleScheduled: styles.enableTab,
            textScheduled: styles.enableText,
            isScheduled: true,
            styleFulfilled: styles.disableTab,
            textFulfilled: styles.disableText,
            isFulfilled: false,
            jobType: 'schedule',
        })
        this.hitMyOrder()
    }

    onFulfiledPress() {
        this.setState({
            styleNew: styles.disableTab,
            textNew: styles.disableText,
            isNew: false,
            styleScheduled: styles.disableTab,
            textScheduled: styles.disableText,
            isScheduled: false,
            styleFulfilled: styles.enableTab,
            textFulfilled: styles.enableText,
            isFulfilled: true,
            jobType: 'completed',
        })
        this.hitMyOrder()
    }

    render() {
        let styles;
        let myOrderList;
        const { loading, myOrderData, languag, styleNew, styleScheduled, styleFulfilled, textNew, textScheduled, textFulfilled, isNew, isScheduled, isFulfilled } = this.state;


        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            myOrderList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else if (isNew && myOrderData && myOrderData.length) {
            myOrderList = (
                <FlatList
                    data={myOrderData}
                    // keyExtractor={item => item.key.toString()}
                    renderItem={({ item }) => {
                        return <NewOrdersRow item={item} styles={styles} langu={this.state.languag} props={this.props} click={this.hitNewOrder} />
                    }
                    }
                />
            );
        } else if (isScheduled && myOrderData && myOrderData.length) {
            myOrderList = (
                <FlatList
                    data={myOrderData}
                    // keyExtractor={item => item.key.toString()}
                    renderItem={({ item }) => {
                        return <ScheduledOrderRow item={item} styles={styles} langu={this.state.languag} props={this.props} click={this.hitMyOrder} />
                    }
                    }
                />
            );
        } else if (isFulfilled && myOrderData && myOrderData.length) {
            myOrderList = (
                <FlatList
                    data={myOrderData}
                    // keyExtractor={item => item.key.toString()}
                    renderItem={({ item }) => {
                        return <FulfilledOrdersRow item={item} styles={styles} langu={this.state.languag} props={this.props} />
                    }
                    }
                />
            );
        } else {
            myOrderList = (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>{StringsOfLanguages.noResultFound}</Text>
                </View>
            );
        }

        return (
            <View style={{ flex: 1 }}>
                <CustToo valueFromParent={StringsOfLanguages.myOrders} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>


                    <View style={styles.acTypeContainer}>

                        <View style={{ flex: 1 }}>

                            <TouchableOpacity
                                onPress={() => {
                                    this.onNewPress()
                                }}>
                                <CardView
                                    style={[styleNew, { flex: 0, width: '90%', borderRadius: 10, height: 50, alignSelf: 'center' }]}
                                    cardElevation={1}
                                    cardMaxElevation={1}
                                    cornerRadius={10}>

                                    <Text style={[textNew, { padding: 7, fontSize: tinySize, textAlign: 'center' }]}>{StringsOfLanguages.newOrders}</Text>
                                </CardView>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.onScheduledPress()
                                }}>
                                <CardView
                                    style={[styleScheduled, { flex: 0, width: '90%', padding: 0, borderRadius: 10, height: 50, alignSelf: 'center' }]}
                                    cardElevation={1}
                                    cardMaxElevation={1}
                                    cornerRadius={10}>
                                    <Text style={[textScheduled, { padding: 7, fontSize: tinySize, textAlign: 'center' }]}>{StringsOfLanguages.scheduledOrders}</Text>
                                </CardView>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.onFulfiledPress()
                                }}>
                                <CardView
                                    style={[styleFulfilled, { flex: 0, width: '90%', borderRadius: 10, height: 50, alignSelf: 'center' }]}
                                    cardElevation={1}
                                    cardMaxElevation={1}
                                    cornerRadius={10}>
                                    <Text style={[textFulfilled, { padding: 5, fontSize: tinySize, textAlign: 'center' }]}>{StringsOfLanguages.fulfilledOrders}</Text>
                                </CardView>
                            </TouchableOpacity>
                        </View>

                    </View>

                    {myOrderList}


                </ImageBackground>
            </View>
        );
    }
}

