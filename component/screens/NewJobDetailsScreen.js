import CustToo from '../helper/CustomToolbarBackBtn';

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image, TextInput, StatusBar, ActivityIndicator, ImageBackground } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import { ScrollView } from 'react-native-gesture-handler';
import CommanUtils from '../utils/CommanUtils';
import colors from '../utils/colors';
import dimens, { secondrySize, ternarySize, tinySize } from '../utils/diamens';
import ApiClient from '../utils/ApiClientService';
var bgImage = require('../../images/main_bg.png');

var settingIcon = require('../../images/setting.png');
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';

export default class NewJobDetailsScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            vendorServiceId: '',
            Authorization: '',
            quotedPrice: 0,
            latitude: 0,
            longitude: 0,
            address: 'address',
            langValue: 'en'

        }
    }


    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var vendorId = this.props.navigation.getParam('vendorServiceId', 0);
        const latitude = this.props.navigation.getParam('latitude', 0.0);
        const longitude = this.props.navigation.getParam('longitude', 0.0);
        const location = this.props.navigation.getParam('location', 'no data');
        const quotedPrice = this.props.navigation.getParam('priceQuote', 'no data');
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({ languag: lan, latitude: latitude, longitude: longitude,quotedPrice:quotedPrice, address: location, Authorization: "Bearer " + dataFromLocal.token, vendorServiceId: vendorId });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        console.log(quotedPrice)
    }


    async hitSubmitPriceQuote() {
        const { quotedPrice } = this.state;
        console.log(this.state)
        if (quotedPrice == 0) {
            CommanUtils.showAlert(StringsOfLanguages.quotePrice, StringsOfLanguages.quotePrice + " " + StringsOfLanguages.doesNotEmpty)
            return;
        }
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.submitPriceQuote(this.state);
            if (responceData != undefined) {
                if (responceData.status) {
                    // await CommanUtils.showAlert(StringsOfLanguages.success, '' + responceData.message)
                    this.navigateToWelcomeScreen()
                }
                else {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                }
            }
            this.setState({ loading: false })
        }
    };

    navigateToWelcomeScreen() {
        this.props.navigation.navigate("CovidScreen");
    }
    navigateToMap() {
        this.props.navigation.navigate("MapViewM", { title: this.state.address, latitude: this.state.latitude, longitude: this.state.longitude })
    }
    render() {
        let styles;
        let progressBar;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (this.state.loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }

        const serviceType = this.props.navigation.getParam('serviceType', 'no data');
        const acType = this.props.navigation.getParam('acType', 'no data');
        const description = this.props.navigation.getParam('description', 'no data');
        const distance = this.props.navigation.getParam('distance', 'no data');
        const location = this.props.navigation.getParam('location', 'no data');
        const noOfAc = this.props.navigation.getParam('noOfAc', 'no data');
        const profileImage = this.props.navigation.getParam('profileImage', 'no data');
        const userName = this.props.navigation.getParam('userName', 'no data');
        const created_at = this.props.navigation.getParam('created_at', 'no data');



        return (
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor='#4384FF' />

                <CustToo valueFromParent={StringsOfLanguages.jobDetails} />

                <ImageBackground source={bgImage} style={{ flex: 1 }}>


                    <ScrollView>

                        <View style={{ margin: 15 }}>

                            <View style={styles.flexDirect}>
                                <Image source={{ uri: profileImage }} style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: '#000' }} />

                                <View style={{ marginLeft: 10, marginRight: 10, flex: 1, marginTop: 20 }}>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }]}>{userName}</Text>
                                </View>

                                <View style={{ marginLeft: 10, marginRight: 10, flex: 1, }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontSize: diamens.ternarySize, fontFamily: 'SFUIText-Bold', color: '#4384FF' }]}> {distance} Km </Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.ternarySize, marginTop: 7 }]}>{StringsOfLanguages.location} : </Text>

                                    <TouchableOpacity style={{}}
                                        onPress={() => {
                                            this.navigateToMap()
                                        }}>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', textDecorationLine: 'underline', color: colors.blue, fontSize: dimens.tinySize }]}>{location}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* <View style={[styles.flexDirect, { marginTop: 10 }]}>

                                <View style={{ flex: 1 }}>
                                    <Text style={[styles.textAlignn, { marginTop: 7 ,fontSize:dimens.tinySize}]}>{StringsOfLanguages.scheduledDate} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn,{fontSize:dimens.tinySize}]}>{'15/3/2021'}</Text>
                                </View>

                                <View style={{ height: 60, width: 60 }}></View>

                                <View style={{ marginLeft: 15, flex: 1, }}>
                                    <Text style={[styles.textAlignn, { marginTop: 7 ,fontSize:dimens.tinySize}]}>{StringsOfLanguages.scheduledTime} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontWeight: 'normal',fontSize:dimens.tinySize }]}>{'12:00'}</Text>
                                </View>

                            </View> */}

                            <View style={[styles.flexDirect, { marginTop: 25 }]}>
                                <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize }]}>{StringsOfLanguages.serviceType} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 5 }]}>{serviceType}</Text>
                                </View>

                                <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize }]}>{StringsOfLanguages.typeOfAc} : </Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 5 }]}> {acType} </Text>
                                </View>

                                <View style={{ marginLeft: 0, marginRight: 0, alignItems: 'center' }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 5 }]}> {noOfAc} </Text>
                                </View>
                            </View>

                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize, marginTop: 25 }]}>{StringsOfLanguages.description}</Text>

                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7, fontSize: diamens.ternarySize }]}>{description} </Text>

                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF', marginTop: 25, fontSize: diamens.ternarySize }]}>{created_at}</Text>

                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF', marginTop: 10, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.quotePrice}</Text>

                            <View style={[styles.flexDirect, { alignItems: 'center', marginTop: 20 }]}>

                                <Text style={{ fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }}>SAR</Text>

                                <TextInput
                                    style={[{ height: 40, borderRadius: 10, paddingStart: 20, paddingEnd: 20, marginStart: 10, marginEnd: 10, borderColor: '#4384FF', borderWidth: 1 }]}
                                    placeholder="0"
                                    keyboardType="numeric"
                                    placeholderTextColor="#000000"
                                    onChangeText={text => this.setState({ quotedPrice: text })} />

                            </View>

                        </View>
                    </ScrollView>

                    {progressBar}

                    <TouchableOpacity style={[styles.btnPress, { alignSelf: 'center', margin: 15 }]}
                        onPress={() => {
                            this.hitSubmitPriceQuote()
                        }}>
                        <Text style={{ color: "white", fontWeight: 'bold', fontSize: 12 }}>{StringsOfLanguages.submitCap}</Text>
                    </TouchableOpacity>

                </ImageBackground>

            </View>
        );
    }
}


