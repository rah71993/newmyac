import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator, Text, ImageBackground } from 'react-native';
import ApiClient from '../utils/ApiClientService';
import colors, { blue, colorPrimaryDark } from '../utils/colors';
import style from '../utils/style';
import CustToo from '../helper/CustomToolbarBackBtn';
import CommanUtils from '../utils/CommanUtils';
import CustomRowSchedule from '../list_row/NotificationRow';
import StringsOfLanguages from '../utils/StringsOfLanguages'
var bgImage = require('../../images/main_bg.png');

export default class NotificationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            langValue: "en",
            loginType: 'user',
            priceQuoteData: [],
            Authorization: 'bb'
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        var loginType = await CommanUtils.getLoginType();
        this.setState({ languag: lan, loginType: loginType, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }

        this.hitNotification()

        var token = await CommanUtils.getToken()
        console.log("tokennnnnn",token)
    }

    async hitNotification() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.getNotification(this.state);
            if (responceData != undefined) {
                if (responceData.status) {
                    await CommanUtils.saveNotificatCount(responceData.notificationCount)
                    this.setState({ priceQuoteData: responceData.data })
                } else {
                    await CommanUtils.saveNotificatCount(0)
                    this.setState({ priceQuoteData: responceData.data })
                }
            }
            this.setState({ loading: false })
        }
    };



    render() {
        const { languag, loading, priceQuoteData } = this.state
        let styles;
        let vendorQuotesList;
        let toolbar;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }
        if (loading) {
            vendorQuotesList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else if (priceQuoteData && priceQuoteData.length) {
            vendorQuotesList = (
                <FlatList
                    data={priceQuoteData}
                    keyExtractor={priceQuoteData.vendorServiceId}
                    renderItem={({ item }) => {
                        return <CustomRowSchedule item={item} styles={styles} props={this.props} state={this.state} />
                    }} />
            );
        } else {
            vendorQuotesList = (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>{StringsOfLanguages.noResultFound}</Text>
                </View>
            );
        }

        return (
            <View style={{ flex: 1 }}>
                <CustToo valueFromParent={StringsOfLanguages.notification} />
                <ImageBackground style={[{ flex: 1 }]} source={bgImage}>
                    <View style={{ flex: 1 }}>
                        {vendorQuotesList}
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

