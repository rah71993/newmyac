
import React, { Component } from 'react';
import { ScrollView, View, ImageBackground, Image, Text, ActivityIndicator, TouchableOpacity } from 'react-native';
import OTPTextView from 'react-native-otp-textinput';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import style from '../utils/style';
import { StackActions, NavigationActions } from 'react-navigation';
import GeneralStatusBarColor from "react-native-general-statusbar";
import StringsOfLanguages from '../utils/StringsOfLanguages'
import colors from '../utils/colors';
var bgImage = require('../../images/main_bg.png');
var logoImage = require('../../images/logo.png');
import CustToo from '../helper/CustomToolbarLoginBackBtn';

export default class OtpScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            languag: true,
            loginType: "",
            id: "",
            otp: "",
            Authorization: '',
            langValue: 'en',

        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage();
        var loginType = await CommanUtils.getLoginType();
        const id = this.props.navigation.getParam('id', 'no data');

        this.setState({ loginType: loginType, id: id, languag: lan });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.showLocation();
    }

    navigateToMainScreen() {
        ApiClient.getNotification(this.state);
        CommanUtils.saveLoginStatus(true);
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: this.state.loginType == 'user' ? 'MainScreen' : 'VendorMainScreen' })],
        });
        this.props.navigation.dispatch(resetAction)
    }
    navigateToEditProfile() {
        this.props.navigation.navigate("VendorEditProfileScreen");
    }

    async onResendOTP() {

        const { id } = this.state;

        if (id != undefined) {
            this.setState({ loading: true })
            var responceData = await ApiClient.resendOTP(this.state);
            this.setState({ loading: false })
            if (responceData != undefined) {
                if (responceData.status) {
                    CommanUtils.showAlert(StringsOfLanguages.otp, responceData.message)
                }
                else {
                    CommanUtils.showAlert(StringsOfLanguages.invalidlogincredentials, '' + responceData.message)
                }
            }
        }
        else {
            CommanUtils.showAlert(StringsOfLanguages.invalidlogincredentials, StringsOfLanguages.enterOTP)        }

    };



    async onVerifyOTP() {
        const { otp, loginType } = this.state;

        if (otp != undefined && otp.length == 4) {
            var isConnected = await CommanUtils.checkInternet()
            if (isConnected) {
                this.setState({ loading: true })
                var responceData = await ApiClient.verifyOTP(this.state);
                this.setState({ loading: false })
                if (responceData != undefined) {
                    if (responceData.status) {
                        CommanUtils.saveLoginDetails(responceData);
                        this.setState({ Authorization: "Bearer " + responceData.token })
                        if (loginType == 'vendor') {
                            console.log(responceData)
                            if (responceData.account_verified === '1') {
                                this.navigateToMainScreen();
                            } else {
                                this.navigateToEditProfile();
                            }
                        } else {
                            this.navigateToMainScreen();
                        }

                    }
                    else {
                        CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                    }
                }
            }
        }
        else {
            CommanUtils.showAlert(StringsOfLanguages.invalidlogincredentials, StringsOfLanguages.enterOTP)
        }

    };


    render() {

        let styles;
        let progressBar = null;

        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (this.state.loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }


        return (
            <View style={{ flex: 1 }}>


                <CustToo valueFromParent={""} />


                <ImageBackground style={styles.container} source={bgImage}>
                    <ScrollView style={{ width: '100%' }} contentContainerStyle={{ marginBottom: 100, justifyContent: "center", alignItems: 'center', flex: 1 }}>
                        <View style={{ alignItems: 'center' }}>

                            <Image source={logoImage} style={styles.logo_style} />

                            <Text style={[styles.wordNormal, { marginTop: 40, marginBottom: 20 }]}>{StringsOfLanguages.enterThe4DigitOTP}</Text>

                            <OTPTextView
                                handleTextChange={(e) => { this.setState({ otp: e }) }}
                                textInputStyle={styles.roundedTextInput}
                                tintColor={colors.colorPrimaryDark}

                            />

                            {progressBar}

                            <TouchableOpacity style={[styles.btnPress, { marginTop: 20 }]}
                                onPress={() => { this.onVerifyOTP(); }}
                            >
                                <Text style={[styles.textButtonPress, { flex: 1, textAlign: 'center' }]}>{StringsOfLanguages.verifyOTPCap}</Text>
                            </TouchableOpacity>

                            <Text style={[styles.textUnderline, { marginTop: 20 }]}
                                onPress={() => { this.onResendOTP(); }}
                            >
                                {StringsOfLanguages.resendOTPCCap}
                            </Text>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>
        );
    }
}




