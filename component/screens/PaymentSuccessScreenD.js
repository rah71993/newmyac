import 'react-native-gesture-handler';
import CustToo from '../helper/CustomToolbarBackBtn';
import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Text, Image, ImageBackground, Modal, TextInput, ActivityIndicator } from 'react-native';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import GeneralStatusBarColor from "react-native-general-statusbar";
import colors, { colorPrimaryDark } from '../utils/colors';
import ApiClient from '../utils/ApiClientService';
import diamens, { tinySize } from '../utils/diamens';
import { Rating, AirbnbRating } from 'react-native-ratings';
import CardView from 'react-native-cardview'
import { CommonActions } from "@react-navigation/native";
import { NavigationActions, StackActions } from 'react-navigation';

var select = require('../../images/select.png');
var bgImage = require('../../images/main_bg.png');
var cross = require('../../images/cross.png');
var user = require('../../images/user_man.png');

export default class PaymentSuccessScreenD extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            vendorId: 0,
            Authorization: 'thf',
            profileImage: 'thf',
            dataResponse: [],
            modalVisible: false,
            rating: 1,
            message: '',
            langValue: "en"
        }
        this.ratingCompleted = this.ratingCompleted.bind(this)
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        // const { profileImage, vendorId } = this.props.route.params;
        this.setState({ languag: lan, vendorId: vendorId, profileImage: profileImage, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        console.log(this.state)
    }

    async hitSubmitReview() {
        const { message, modalVisible } = this.state
        if (message == '') {
            CommanUtils.showAlert("", StringsOfLanguages.writeYourReviews)
            return;
        }
        this.setState({ loading: true })
        var responceData = await ApiClient.submitReview(this.state);
        if (responceData != undefined) {
            this.setState({ loading: false })
            if (responceData.status) {
                this.setState({ modalVisible: !modalVisible }, () => { this.navigateToMainScreen() })
                CommanUtils.showAlert("", responceData.message)
            } else {
                CommanUtils.showAlert("", responceData.message)
            }
        }
        this.setState({ loading: false })
    };

    async navigateToMainScreen() {
        setTimeout(async () => {
            await CommanUtils.savePaymentStatus("done")
            CommanUtils.restartApp()
        }, 2000);
    }

    showDialogMod(item) {
        this.setState({ modalVisible: true })
    }
    ratingCompleted(rating) {
        this.setState({ rating: rating })
    }

    render() {
        let styles;
        let bannerList;
        const { modalVisible, languag, rating, loading, profileImage } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            bannerList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            bannerList = (
                <View></View>
            );
        }

        return (
            <View style={{ flex: 1 }}>
                <GeneralStatusBarColor backgroundColor={colors.statusColor} barStyle="light-content" />

                <ImageBackground style={[styles.container, {justifyContent:'center',alignItems:'center'}]} source={bgImage}>
                    <Image source={select} style={[styles.logo_style, { marginTop: 0 }]} />
                    <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', marginTop: 20, textAlign: 'center' }]}>
                        {StringsOfLanguages.paymentDone} {'\n'} {StringsOfLanguages.successfully}
                    </Text>
                    <Text style={[{ fontFamily: 'SFUIText-Medium', fontSize: diamens.tinySize, color: colors.blue, marginTop: 10, textAlign: 'center' }]}>
                        {StringsOfLanguages.yourPaymentisSuccessful}
                    </Text>
                    <TouchableOpacity style={[styles.btnPress, { marginTop: 120, alignSelf: 'center' }]}
                        onPress={() => {
                            this.showDialogMod();
                        }}>
                        <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 12 }}>{StringsOfLanguages.rateYourServiceCap}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btnBorder, { marginTop: 20, alignSelf: 'center' }]}
                        onPress={() => {
                            this.navigateToMainScreen()
                        }}>
                        <Text style={{ color: colorPrimaryDark, fontFamily: 'SFUIText-Bold', fontSize: tinySize }}>{StringsOfLanguages.myOrders}</Text>
                    </TouchableOpacity>
                </ImageBackground>

                <Modal animationType="slide" transparent={true} visible={modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: !modalVisible });
                    }}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop: 22 }}>
                            <View style={{ margin: 20, backgroundColor: colors.white, borderRadius: 10, padding: 10, alignItems: "center", shadowColor: "#000", shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5 }}>

                                <View style={[styles.flexDirect, { padding: 10 }]}>
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ modalVisible: !modalVisible })
                                            }}>
                                            <Image source={cross} style={[styles.alignSelfFlexEnd, { height: 15, width: 15 }]} />
                                        </TouchableOpacity>
                                        <Text style={[{ textAlign: 'center', color: colors.blue, fontSize: diamens.ternarySize, fontFamily: 'SFUIText-Bold', marginTop: 10 }]}>{StringsOfLanguages.rateYourServiceCap}</Text>
                                        <Image source={{ uri: profileImage }} style={[{ alignSelf: 'center', height: 50, width: 50, borderRadius: 25, marginTop: 10 }]} />

                                        <Rating type='custom'
                                            onFinishRating={this.ratingCompleted}
                                            startingValue={rating}
                                            ratingCount={5}
                                            imageSize={30}
                                            ratingColor={colors.blue}
                                            ratingBackgroundColor={colors.greyVeryLight}
                                            style={[{ alignSelf: 'center', paddingTop: 10, paddingBottom: 5 }]} />

                                        <Text style={[{ fontFamily: 'SFUIText-Semibold', color: colors.blue, marginTop: 20, paddingEnd: 0, paddingStart: 0, fontSize: diamens.tinySize }]}>{StringsOfLanguages.writeYourReviews}</Text>
                                        <CardView
                                            style={{
                                                backgroundColor: '#fff', height: 130, marginTop: 7, paddingStart: 10, paddingEnd: 10, shadowColor: "#000", shadowOffset: { width: 0, height: 0, },
                                                shadowOpacity: 0.25,
                                            }}
                                            cardElevation={3}
                                            cardMaxElevation={10}
                                            cornerRadius={5}>
                                            <TextInput
                                                style={[styles.inputTxt, { textAlign: 'center' }]}
                                                // placeholder={StringsOfLanguages.comment}
                                                placeholderTextColor="#000000"
                                                maxLength={3000}
                                                numberOfLines={10}
                                                multiline
                                                onChangeText={text => this.setState({ message: text })} />

                                        </CardView>

                                        {bannerList}

                                        <TouchableOpacity style={[styles.btnPress, { marginTop: 20, alignSelf: 'center' }]}
                                            onPress={() => {
                                                this.hitSubmitReview();
                                            }}>
                                            <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: 12 }}>{StringsOfLanguages.submitCap}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Modal>
            </View>
        );
    }
}


