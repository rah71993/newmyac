import CustToo from '../helper/CustomToolbar';

import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, ScrollView, Image } from 'react-native';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import colors from '../utils/colors';
import diamens from '../utils/diamens';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import GeneralStatusBarColor from "react-native-general-statusbar";

var bgImage = require('../../images/main_bg.png');
var header_bg = require('../../images/header_bg.png');


export default class SettingsScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            switchValue: false,
            languag: true,
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }





    render() {
        let styles;
        let leftForwardIcon;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            leftForwardIcon = require('../../images/arrow-right.png');
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
            leftForwardIcon = require('../../images/left-arrow.png');
        }

        return (
            <View style={{ flex: 1 }}>

                <GeneralStatusBarColor
                    backgroundColor={colors.statusColor}
                    barStyle="light-content"
                />


                <ImageBackground style={[styles.flexDirect, { backgroundColor: 'white' }]} >

                    <View style={styles.leftContainer}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.goBack()}>
                            <View style={{ padding: 15, alignSelf: 'flex-start', justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={backIcon}
                                    style={[styles.ImageIconStyle], { height: 15, width: 20, tintColor: colors.blue }}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <GooglePlacesAutocomplete
                        placeholder='Search'
                        fetchDetails={true}
                        autoFocus={true}
                        enablePoweredByContainer={false}

                        onPress={(data, details = null) => {
                            var navigation = this.props.navigation;
                            navigation.goBack()
                            navigation.state.params.handlePlaceCallback(details)
                        }}
                        query={{
                            key: StringsOfLanguages.googleApiKey,
                            language: 'en',
                        }}
                        styles={{ margin: 20 }}
                        GooglePlacesDetailsQuery={{ fields: ['formatted_address', 'geometry'] }}
                    />
                </ImageBackground>
                <ScrollView></ScrollView>

            </View>
        );
    }
}


