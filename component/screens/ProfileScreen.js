
import CardView from 'react-native-cardview'
import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, Alert, ImageBackground } from 'react-native';
import colors from '../utils/colors';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import CustToo from '../helper/CustomToolbarBackBtn';
import { ScrollView } from 'react-native-gesture-handler';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import { NavigationActions, StackActions } from 'react-navigation';
import diamens from '../utils/diamens';

var acMachanicImg = require('../../images/ac_mechanic.png');
var userImage = require('../../images/user_man.png');
var editImage = require('../../images/edit.png');
var plusImage = require('../../images/plus.png');
var logoutImage = require('../../images/logout.png');
var bgImage = require('../../images/main_bg.png');

export default class ProfileScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            name: '',
            email: '',
            mobile_number: '',
            mobile_code: '',
            address: '',
            profile_pic: '',
            loginType: '',
           
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var loginTyp = await CommanUtils.getLoginType()
        this.setState({ languag: lan, loginType: loginTyp });

        this.showDataFromLocal()
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.showDataFromLocal()
        });
    }

    async showDataFromLocal() {
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({
            name: dataFromLocal.name,
            email: dataFromLocal.email,
            mobile_number: dataFromLocal.mobile_number,
            mobile_code: dataFromLocal.mobile_code,
            address: dataFromLocal.address,
            profile_pic: dataFromLocal.profile_pic,
        });
    }


    navigateToEditProfile() {
        this.props.navigation.navigate(this.state.loginType == "user" ? "EditProfileScreen" : "VendorEditProfileScreen")
    }

    showLogoutAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: StringsOfLanguages.cancel,
                    onPress: () =>
                        console.log('Cancel Pressed'),
                },
                {
                    text: StringsOfLanguages.logout,
                    onPress: () => {
                        this.gotoLogin()
                        CommanUtils.saveLoginStatus(false);
                    }
                }
            ],
            { cancelable: false }
        );

    }
    gotoLogin() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'App', params: { 'initialScreen': 'ChooseUserVender' } }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }

    render() {
        let styles;
       

        const { languag, name, email, mobile_number, mobile_code, address, profile_pic } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

       

        return (

            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.myAccount} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    <ScrollView >
                        <View style={{ padding: 15 }}>

                            <View style={{ marginTop: 20 }}>


                                <View style={{ flex: 1 }} onStartShouldSetResponder={
                                    () => this.navigateToEditProfile()
                                }>
                                    <View style={[styles.profileImgBackground, { alignSelf: 'center' }]}>
                                        <Image source={{ uri: profile_pic }} style={[styles.profileImg, {}]} />
                                    </View>
                                </View>

                                <View style={[styles.flexDirect, {justifyContent:'center',alignItems:'center', width: '100%' }]}>
                                    <Text style={{ flex: 1,marginStart:15,marginEnd:15,  textAlign: 'center',fontFamily: 'SFUIText-Bold', fontSize: 16, marginTop: 10, color: 'black' }}>{name}</Text>
                                    <TouchableOpacity style={{}} onPress={() => { this.navigateToEditProfile() }}>
                                        <Image source={editImage} style={[styles.ImageIconStyle, styles.alignSelfFlexEnd, { height: 15, width: 15 }]} />
                                    </TouchableOpacity>
                                </View>

                                <Text style={[styles.textAlignn, { fontSize: diamens.tinySize, color: colors.smallTextColor, marginTop: 20, width: '100%' }]}>{StringsOfLanguages.phoneNumber}</Text>
                                <View style={[styles.flexDirect, { width: '100%' }]}>
                                    <Text style={[styles.textAlignn, { flex: 1, marginTop: 5, color: 'black', fontSize: diamens.secondrySize }]}>{mobile_code}-{mobile_number}</Text>
                                </View>


                                <Text style={[styles.textAlignn, { fontSize: diamens.tinySize, color: colors.smallTextColor, marginTop: 20, width: '100%' }]}>{StringsOfLanguages.email}</Text>
                                <View style={[styles.flexDirect, styles.textAlignn, {justifyContent:'center',alignItems:'center', width: '100%' }]}>
                                    <Text style={[styles.textAlignn, { marginTop: 5, color: 'black', flex: 1, fontSize: diamens.secondrySize, }]}>{email}</Text>
                                    <TouchableOpacity onPress={() => { this.navigateToEditProfile() }}>
                                        <Image source={editImage} style={[styles.ImageIconStyle, {height: 15, width: 15}]} />
                                    </TouchableOpacity>
                                </View>

                                <Text style={[styles.textAlignn, { fontSize: diamens.tinySize, color: colors.smallTextColor, marginTop: 20, width: '100%' }]}>{StringsOfLanguages.address}</Text>
                                <View style={[styles.flexDirect, {justifyContent:'center',alignItems:'center', width: '100%' }]}>
                                    <Text style={[styles.textAlignn, { marginTop: 5, color: 'black', flex: 1, fontSize: diamens.secondrySize }]}>{address}</Text>
                                    <TouchableOpacity onPress={() => { this.navigateToEditProfile() }}>
                                        <Image source={editImage} style={[styles.ImageIconStyle, {height: 15, width: 15}]} />
                                    </TouchableOpacity>
                                </View>

                                <TouchableOpacity style={{ marginTop: 30, marginBottom: 30, alignSelf: 'baseline', alignSelf: 'center' }}
                                    onPress={() => { this.showLogoutAlert(StringsOfLanguages.alert, StringsOfLanguages.areSureWantToLogout); }}>
                                    <View style={[styles.flexDirect, { alignSelf: 'center', padding: 7, }]}>
                                        <Image source={logoutImage} style={{ alignSelf: 'center', marginStart: 30, height: 21, width: 20 }} />
                                        <Text style={{ marginStart: 10, fontSize: 16 }}>{StringsOfLanguages.logout}</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </View>


        );
    }
}


