import CustToo from '../helper/CustomToolbar';

import React, { Component } from 'react';
import { View, Image, Text, Linking, TouchableOpacity, ImageBackground } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
var logoImage = require('../../images/logo.png');

var rate = require('../../images/rate.png');
var bgImage = require('../../images/main_bg.png');

export default class RateUsScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rated: false,
            languag: true,
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }
    render() {
        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.rateUs} />


                <ImageBackground source={bgImage} style={{ flex: 1, padding: 15 }}>

                    <Image source={logoImage} style={[styles.logo_style, { marginTop: 20, alignSelf: 'center' }]} />

                    <TouchableOpacity style={[styles.btnPress, { marginTop: 100, alignSelf: 'center' }]} onPress={() => {
                        openStore()
                    }}>
                        <Text style={styles.textButtonPress}>{StringsOfLanguages.rateUsCap}</Text>
                    </TouchableOpacity>

                </ImageBackground>
            </View>
        );
    }
}

const GOOGLE_PACKAGE_NAME = 'com.newmyac';
const APPLE_STORE_ID = 'id1564790015';

const openStore = () => {
    // alert("cli")
    //This is the main trick
    if (Platform.OS != 'ios') {
        Linking.openURL(
            `market://details?id=${GOOGLE_PACKAGE_NAME}`,
        ).catch(
            (err) => alert('Please check for Google Play Store')
        );
    } else {
        Linking.openURL(
            `itms://itunes.apple.com/in/app/apple-store/${APPLE_STORE_ID}`,
        ).catch((err) => alert('Please check for the App Store'));
    }
};