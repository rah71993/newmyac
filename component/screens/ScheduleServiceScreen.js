
import React, { Component } from 'react';
import { View, ActivityIndicator, Image, Text, ScrollView, TouchableOpacity, Button } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import CustToo from '../helper/CustomToolbarBackBtn';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import StringsOfLanguages from '../utils/StringsOfLanguages';
import diamens from '../utils/diamens';

var dateIcon = require('../../images/date.png');
var down_arrow = require('../../images/down_arrow.png');


export default class ScheduleServiceScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            languag: true,
            visibility: false,
            mod: 'date',
            dateDisplay: "",
            timeSlot: "",
            vendorServiceId: '',
            Authorization: '',
            langValue: 'en',

        }
        this.setDefaultTimeSlot = this.setDefaultTimeSlot.bind(this)
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        var vendorId = this.props.navigation.getParam('vendorServiceId', 0);
        this.setState({ languag: lan, dateDisplay: formatedDate(new Date), Authorization: "Bearer " + dataFromLocal.token, vendorServiceId: vendorId });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.setDefaultTimeSlot()
    }


    setDefaultTimeSlot() {
        const { dateDisplay } = this.state;
        var today = new Date
        if (dateDisplay === formatedDate(today)) {
            if (today.getHours() >= 8 && today.getHours() < 10) {
                this.setState({ timeSlot: timeSlotData[1].title })
            } else if (today.getHours() >= 10 && today.getHours() < 12) {
                this.setState({ timeSlot: timeSlotData[2].title })
            } else if (today.getHours() >= 12 && today.getHours() < 14) {
                this.setState({ timeSlot: timeSlotData[3].title })
            } else if (today.getHours() >= 14 && today.getHours() < 16) {
                this.setState({ timeSlot: timeSlotData[4].title })
            } else if (today.getHours() >= 16 && today.getHours() < 18) {
                this.setState({ timeSlot: timeSlotData[5].title })
            } else if (today.getHours() >= 18) {
                this.setState({ timeSlot: "" })
            } else {
                this.setState({ timeSlot: timeSlotData[0].title })
            }
        } else {
            this.setState({ timeSlot: timeSlotData[0].title })
        }
    }

    async hitScheduleService() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.scheduleService(this.state);
            if (responceData != undefined) {
                if (responceData.status) {
                    // await CommanUtils.showAlert(StringsOfLanguages.success, '' + responceData.message)
                    this.navigateToWelcomeScreen()
                }
                else {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                }
            }
            this.setState({ loading: false })
        }
    };

    navigateToWelcomeScreen() {
        const { dateDisplay, timeSlot } = this.state;
        this.props.navigation.navigate("SuccessScheduleScreen", { 'date': dateDisplay, 'time': timeSlot });
    }

    handleConfirm = (today) => {
        if (this.state.mod == 'date') {
            this.setState({ dateDisplay: formatedDate(today), visibility: false }, () => { this.setDefaultTimeSlot() })
        } else {
            // time = formatAMPM(today);
            // this.setState({ timeDisplay: time, visibility: false })
        }
    }
    onPressCancel = () => {
        this.setState({ visibility: false })
    }

    onPressButton = () => {
        this.setState({ visibility: true })
    }

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = (index) => {
        this._menu.hide();
        this.setState({ timeSlot: timeSlotData[index].title })
        console.log(timeSlotData)
    };

    showMenu = () => {
        this._menu.show();
    };


    render() {
        let styles;
        let progressBar = null;

        const { languag, loading, dateDisplay, timeSlot } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }

        let menuItemTimeSlot = timeSlotData.map((item, index) => {
            var today = new Date
            if (dateDisplay == formatedDate(new Date)) {
                if (today.getHours() >= 8 && today.getHours() < 10) {
                    if (index > 0) {
                        return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
                    }
                } else if (today.getHours() >= 10 && today.getHours() < 12) {
                    if (index > 1) {
                        return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
                    }
                } else if (today.getHours() >= 12 && today.getHours() < 14) {
                    if (index > 2) {
                        return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
                    }
                } else if (today.getHours() >= 14 && today.getHours() < 16) {
                    if (index > 3) {
                        return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
                    }
                } else if (today.getHours() >= 16 && today.getHours() < 18) {
                    if (index > 4) {
                        return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
                    }
                } else if (today.getHours() >= 18) {

                } else {
                    return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
                }
            } else {
                return <MenuItem key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
            }
        })

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.scheduleService} />
                <ScrollView >

                    <CardView style={{ backgroundColor: '#fff', margin: 20, padding: 10 }}
                        cardElevation={2}
                        cardMaxElevation={2}
                        cornerRadius={10}>
                        <View style={{ padding: 10 }}>

                            <Text style={{ textTransform: 'uppercase', color: "#4384FF", fontSize: 20, alignSelf: 'center', fontFamily: 'SFUIText-Bold', marginStart: 10, marginEnd: 10 }}>{StringsOfLanguages.scheduleNow}</Text>

                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Semibold', marginTop: 30 }]}>{StringsOfLanguages.selectDate}</Text>
                            <CardView style={{ justifyContent: 'center', backgroundColor: '#fff', marginTop: 5, height: 50 }}
                                cardElevation={3}
                                cardMaxElevation={2}
                                cornerRadius={5}>
                                <View style={[styles.flexDirect, { alignItems: 'center', marginLeft: 10, marginRight: 10 }]}
                                    onStartShouldSetResponder={
                                        () => { this.setState({ mod: 'date' }); this.onPressButton() }
                                    }>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginStart: 0, flex: 1 }]}>{dateDisplay}</Text>
                                    <Image source={dateIcon} style={{ height: 20, width: 20 }} />
                                </View>
                            </CardView>

                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', marginTop: 20, }]}>{StringsOfLanguages.selectTimeSlot}</Text>
                            <CardView style={{ justifyContent: 'center', backgroundColor: '#fff', marginTop: 5, height: 50 }}
                                cardElevation={3}
                                cardMaxElevation={2}
                                cornerRadius={5}>
                                <Menu style={{ width: '85%' }} ref={this.setMenuRef}
                                    button={
                                        <View style={[styles.flexDirect, styles.alignSelfFlexEnd, { width: '100%', alignItems: 'center', padding: 10 }]}
                                            onStartShouldSetResponder={() => { this.showMenu() }}>
                                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', flex: 1, padding: 3, fontSize: diamens.ternarySize }]}>{timeSlot}</Text>
                                            <Image source={down_arrow} style={[styles.ImageIconStyle], { height: 20, width: 20, resizeMode: 'contain' }} />
                                        </View>
                                    }>
                                    <ScrollView>
                                        {menuItemTimeSlot}
                                    </ScrollView>
                                </Menu>
                            </CardView>

                            {progressBar}

                            <TouchableOpacity style={[styles.btnPress, { alignSelf: 'center', marginTop: 30 }]} onPress={() => {
                                this.hitScheduleService()
                            }}>
                                <Text style={styles.textButtonPress}>{StringsOfLanguages.scheduleNowCap}</Text>
                            </TouchableOpacity>

                            <DateTimePickerModal
                                isVisible={this.state.visibility}
                                minimumDate={new Date()}
                                mode={this.state.mod}
                                onConfirm={this.handleConfirm}
                                onCancel={this.onPressCancel}
                            ></DateTimePickerModal>

                        </View>
                    </CardView>
                </ScrollView>
            </View>
        );
    }
}



const timeSlotData = [
    {

        id: 1,
        title: '08:00 - 10:00',
    },
    {
        id: 2,
        title: '10:00 - 12:00',
    },
    {
        id: 3,
        title: '12:00 - 14:00',
    },
    {
        id: 4,
        title: '14:00 - 16:00',
    },
    {
        id: 5,
        title: '16:00 - 18:00',
    },
    {
        id: 6,
        title: '18:00 - 20:00',
    },
];

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}
function formatedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;
    var formatedDate = day + "-" + month + "-" + year
    return formatedDate;
}
