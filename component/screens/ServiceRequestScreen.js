
import CardView from 'react-native-cardview'
import React, { Component } from 'react';
import { View, Image, Text, StatusBar, TextInput, ActivityIndicator, Slider, ImageBackground } from 'react-native';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import CommanUtils from '../utils/CommanUtils';
import style from '../utils/style';
import CustToo from '../helper/CustomToolbarBackBtn';
import Geolocation from '@react-native-community/geolocation';
import ApiClient from '../utils/ApiClientService';
import colors from '../utils/colors';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import diamens from '../utils/diamens';
var homeIcon = require('../../images/home.png');
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

var bgImage = require('../../images/main_bg.png');
var down_arrow = require('../../images/down_arrow.png');


export default class ServiceRequestScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            latitude: 0.0,
            longitude: 0.0,
            address: '',
            message: '',
            langValue: "en",
            Authorization: 'thf',
            acType: 1,
            serviceTypeId: 1,
            serviceType: "",
            serviceTypeData: [],
            serviceTypeDataTitle: [],
            noOfAc: 1,
            noOfAcData: [],
            distanceValue: 1,
            langValue: "en",

        }
        this.onClickServiceTypeDropdown = this.onClickServiceTypeDropdown.bind(this)
        this.onClickNoOfAcDropdown = this.onClickNoOfAcDropdown.bind(this)
    }

    static navigationOptions = {
        drawerIcon: (
            <Image source={homeIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
        )
    };

    async componentDidMount() {
        // this.showLocation()
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        const acType = this.props.navigation.getParam('acTypeId', 0);
        const address = this.props.navigation.getParam('address', "no data");
        const latitude = this.props.navigation.getParam('latitude', 0);
        const longitude = this.props.navigation.getParam('longitude', 0);
        this.setState({ languag: lan, acType: acType, address: address, latitude: latitude, longitude: longitude, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.hitServiceType();

        const { noOfAcData } = this.state;
        for (var i = 1; i <= 100; i++) {
            noOfAcData.push({
                value: i,
            });
        };
    }

    async hitRequestService() {
        const { message } = this.state;
        if (message != undefined && message.trim()) {
            var isConnected = await CommanUtils.checkInternet()
            if (isConnected) {
                this.setState({ loading: true })
                var responceData = await ApiClient.requestService(this.state);
                if (responceData != undefined) {
                    if (responceData.status) {
                        this.navigateToSuccess()
                    }
                    else {
                        CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                    }
                }
            }
        } else {
            CommanUtils.showAlert(StringsOfLanguages.empty, StringsOfLanguages.commentDoesNotEmpty)
        }
        this.setState({ loading: false })
    };

    async hitServiceType() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            // this.setState({ loading: true })
            var responceData = await ApiClient.getServiceType(this.state);
            if (responceData != undefined) {
                this.setState({ serviceTypeData: responceData.data })
                const responsed = responceData.data;
                if (responsed != undefined) {
                    let data = responsed.map(item => {
                        return { "value": item.title };
                    })
                    this.setState({ serviceTypeDataTitle: data, serviceType: responsed[0].title, serviceTypeId: responsed[0].serviceId })
                }
            }
            // this.setState({ loading: false })
        }
    };

    async onClickServiceTypeDropdown(value, index) {
        const { serviceTypeData } = this.state;
        this.setState({ serviceType: serviceTypeData[index].title, serviceTypeId: serviceTypeData[index].serviceId })
    }
    async onClickNoOfAcDropdown(value, index) {
        this.setState({ noOfAc: value })
    }

    navigateToWelcomeScreen() {
        this.props.navigation.navigate("Welcome");
    }

    navigateToSuccess() {
        this.props.navigation.navigate("CovidScreen");
    }

    showLocation() {
        Geolocation.getCurrentPosition(info => {
            // console.log(info)
            this.setState({ latitude: info.coords.latitude, longitude: info.coords.longitude, })
        }
        );
    }

    _menu = null;
    _menuN = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = (index) => {
        this._menu.hide();
        const { serviceTypeData } = this.state;
        this.setState({ serviceType: serviceTypeData[index].title, serviceTypeId: serviceTypeData[index].serviceId })
        console.log(serviceTypeData)
    };

    showMenu = () => {
        this._menu.show();
    };

    setMenuRefN = ref => {
        this._menuN = ref;
    };

    hideMenuN = (index) => {
        this._menuN.hide();
        const { noOfAcData } = this.state;
        this.setState({ noOfAc: noOfAcData[index].value })
    };

    showMenuN = () => {
        this._menuN.show();
    };

    render() {
        let styles;
        let progressBar;
        const { serviceType, noOfAc, noOfAcData, distanceValue, serviceTypeData } = this.state;

        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            // alert("english")
        } else {
            // alert("arabic "+this.state.languag)
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (this.state.loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View></View>
            );
        }


        let menuItemServiceType = serviceTypeData.map((item, index) => {
            return <MenuItem style={{maxWidth: 'auto'}} textStyle={styles.textAlignn} key={index} onPress={() => { this.hideMenu(index) }}>{item.title}</MenuItem>
        })
        let menuItemNoOfAc = noOfAcData.map((item, index) => {
            return <MenuItem style={{maxWidth: 'auto'}} textStyle={styles.textAlignn} key={index} onPress={() => { this.hideMenuN(index) }}>{item.value}</MenuItem>
        })

        return (

            <View style={{ flex: 1 }}>
                <StatusBar hidden={false} />
                <CustToo valueFromParent={StringsOfLanguages.serviceRequest} />

              
                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    <ScrollView >
                        <View style={{ padding: 15 }}>
                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', paddingEnd: 0, paddingStart: 0, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.serviceType}</Text>

                            <CardView
                                style={{ backgroundColor: '#fff', marginEnd: 0, marginStart: 0, marginTop: 5, marginBottom: 0, paddingStart: 10, paddingEnd: 10 }}
                                cardElevation={1} cardMaxElevation={1} cornerRadius={5}>
                                <Menu style={[{ width: '85%' }]} ref={this.setMenuRef}
                                    button={
                                        <View style={[styles.flexDirect, styles.alignSelfFlexEnd, { width: '100%', alignItems: 'center', padding: 10 }]}
                                            onStartShouldSetResponder={() => { this.showMenu() }}>
                                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', flex: 1, padding: 3, fontSize: diamens.ternarySize }]}>{serviceType}</Text>
                                            <Image source={down_arrow} style={[styles.ImageIconStyle], { height: 20, width: 20, resizeMode: 'contain' }} />
                                        </View>
                                    }>
                                    <ScrollView>
                                        {menuItemServiceType}
                                    </ScrollView>
                                </Menu>
                            </CardView>

                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', marginTop: 20, paddingEnd: 0, paddingStart: 0, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.noOfAc}</Text>
                            <CardView
                                style={{ backgroundColor: '#fff', marginEnd: 0, marginStart: 0, marginTop: 5, marginBottom: 0, paddingStart: 10, paddingEnd: 10 }}
                                cardElevation={1} cardMaxElevation={1} cornerRadius={5}>
                                <Menu style={{ width: '85%', height: 300 }} ref={this.setMenuRefN}
                                    button={
                                        <View style={[styles.flexDirect, styles.alignSelfFlexEnd, { width: '100%', alignItems: 'center', padding: 10 }]}
                                            onStartShouldSetResponder={() => { this.showMenuN() }}>
                                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', flex: 1, padding: 3, fontSize: diamens.ternarySize }]}>{noOfAc}</Text>
                                            <Image source={down_arrow} style={[styles.ImageIconStyle], { height: 20, width: 20, resizeMode: 'contain' }} />
                                        </View>
                                    }>
                                    <ScrollView>
                                        {menuItemNoOfAc}
                                    </ScrollView>
                                </Menu>
                            </CardView>

                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', marginTop: 20, paddingEnd: 0, paddingStart: 0, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.distanceRange} = {distanceValue} km</Text>
                            <View style={[styles.flexDirect, { marginTop: 10, alignItems: 'center' }]}>

                                <Text style={[{ fontFamily: 'SFUIText-Medium', marginTop: 0, fontSize: diamens.ternarySize }]}>1 km</Text>

                                <Slider
                                    step={1}
                                    minimumValue={1}
                                    maximumValue={50}
                                    minimumTrackTintColor={colors.colorPrimaryDark}
                                    onValueChange={(ChangedValue) => this.setState({ distanceValue: ChangedValue })}
                                    style={{ flex: 1, marginStart: 5, marginEnd: 5 }}
                                />

                                <Text style={[{ fontFamily: 'SFUIText-Medium', marginTop: 0, fontSize: diamens.ternarySize }]}>50 km</Text>

                            </View>


                            <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', marginTop: 20, paddingEnd: 0, paddingStart: 0, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.writeYourQuerytoUs}</Text>
                            <CardView
                                style={{ backgroundColor: '#fff', marginTop: 7 }}
                                cardElevation={1}
                                cardMaxElevation={2}
                                cornerRadius={5}>
                                <TextInput
                                    style={[styles.inputTxt, { height: 130, margin: 10, }]}
                                    // placeholder={StringsOfLanguages.comment}
                                    placeholderTextColor="#000000"
                                    maxLength={3000}
                                    multiline={true}
                                    onChangeText={text => this.setState({ message: text })} />

                                <Text style={[styles.textAlignn, styles.alignSelfFlexEnd, { fontFamily: 'SFUIText-Bold', marginEnd: 5, marginStart: 5, marginBottom: 5, fontSize: diamens.tinySize, color: colors.blue }]}>3000</Text>

                            </CardView>
                        </View>

                        {progressBar}

                        <TouchableOpacity style={[styles.btnPress, { marginTop: 30, alignSelf: 'center' }]}
                            onPress={() => { this.hitRequestService() }}>
                            <Text style={styles.textButtonPress}>{StringsOfLanguages.submitCap}</Text>
                        </TouchableOpacity>



                    </ScrollView>

                </ImageBackground>


            </View>
        );
    }
}


