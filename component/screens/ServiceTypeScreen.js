
import React, { Component } from 'react';
import { View, Image, Text, StatusBar, FlatList, ActivityIndicator } from 'react-native';
import CustomRow from '../list_row/ServiceType';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import CommanUtils from '../utils/CommanUtils';
import style from '../utils/style';
import ApiClient from '../utils/ApiClientService';
import CustToo from '../helper/CustomToolbarBackBtn';
import colors from '../utils/colors';
import { ScrollView } from 'react-native-gesture-handler';

var acMachanicImg = require('../../images/ac_mechanic.png');
var homeIcon = require('../../images/home.png');
var logoImage = require('../../images/logo.png');

export default class ServiceTypeScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            langValue: "en",
            serviceTypeData: [],
            acTypeId: 1,
            address: "",
        }
    }

    static navigationOptions = {
        drawerIcon: (
            <Image source={homeIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
        )
    };

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        const acTypeId = this.props.navigation.getParam('acTypeId', 0);
        const address = this.props.navigation.getParam('address', "no data");
        this.setState({ languag: lan, acTypeId: acTypeId, address: address });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.hitServiceType()
    }

    async hitServiceType() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.getServiceType(this.state);
            if (responceData != undefined) {
                this.setState({ serviceTypeData: responceData.data })
            }
            this.setState({ loading: false })
        }
    };



    render() {
        let styles;
        let serviceTypeList;
        const { loading, languag, serviceTypeData } = this.state;
        let right_icon;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            right_icon = require('../../images/arrow-right.png')
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
            right_icon = require('../../images/left-arrow.png')
        }
        if (loading) {
            serviceTypeList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else if (serviceTypeData && serviceTypeData.length) {
            serviceTypeList = (
                <FlatList style={{ marginBottom: 10 }}
                    data={serviceTypeData}
                    // keyExtractor={this.getData.key}
                    renderItem={({ item }) => {
                        return <CustomRow item={item} right_icon={right_icon} styles={styles} state={this.state} props={this.props} />
                    }} />

            );
        } else {
            serviceTypeList = (
                <View style={{ flex: 1, marginTop: 130, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>{StringsOfLanguages.noResultFound}</Text>
                </View>
            );
        }
        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.chooseServiceType} />
                <ScrollView >

                    <View>
                        <View style={{ height: 250 }}>
                            <Image source={acMachanicImg} style={[styles.transparentImageBottomRadious, { height: 250 }]} />

                            <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', position: 'absolute', marginTop: 0 }}>
                                <Image source={logoImage} style={[styles.logo_style, { marginTop: 50 }]} />
                            </View>

                        </View>

                        <Text style={[styles.homeBlueHead, { paddingStart: 20, paddingTop: 15, color: '#4384FF', fontSize: 16, fontFamily: 'SFUIText-Bold', }]}>{StringsOfLanguages.selectTheTypeOfAC}</Text>

                        {serviceTypeList}
                    </View>
                </ScrollView>
            </View>
        );
    }
}

