import CustToo from '../helper/CustomToolbar';

import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, StatusBar, Switch, Alert, ImageBackground } from 'react-native';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import colors from '../utils/colors';
import diamens from '../utils/diamens';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import { NavigationActions, StackActions } from 'react-navigation';

var bgImage = require('../../images/main_bg.png');


export default class SettingsScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            switchValue: false,
            languag: true,
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }

    async navigateMainDrawer() {
        var loginType = await CommanUtils.getLoginType();
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: loginType === 'user' ? 'MainScreen' : 'VendorMainScreen', params: { screen: 'MainScreen' } }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }


    showLanguageAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: StringsOfLanguages.english,
                    onPress: () => {
                        CommanUtils.setLanguage("en");
                        this.navigateMainDrawer();
                    }
                },
                {
                    text: StringsOfLanguages.arabic,
                    onPress: () => {
                        CommanUtils.setLanguage("ar");
                        this.navigateMainDrawer();
                    }
                }
            ],
            { cancelable: true }
        );

    }

    render() {
        let styles;
        let leftForwardIcon;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            leftForwardIcon = require('../../images/arrow-right.png');
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
            leftForwardIcon = require('../../images/left-arrow.png');
        }

        return (
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor='#4384FF' />

                <CustToo valueFromParent={StringsOfLanguages.settings} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    {/* <View style={[styles.flexDirect, { paddingStart: 15, paddingEnd: 15, paddingTop: 10, paddingBottom: 10, alignItems: 'center', justifyContent: 'center' }]}>
                        <Text style={[styles.textAlignn, { flex: 1, marginTop: 0, marginBottom: 0, fontSize: diamens.ternarySize }]}>{StringsOfLanguages.notification}</Text>
                        <Switch trackColor={{ true: colors.colorPrimaryDark, false: 'grey' }} value={this.state.switchValue} onValueChange={(switchValue) => {
                            console.log("" + switchValue);
                            this.setState({ switchValue })
                        }}></Switch>
                    </View>
                    <View style={{ height: 1, backgroundColor: "#CDCACA" }} /> */}

                    <TouchableOpacity
                        onPress={() => {
                            this.showLanguageAlert(StringsOfLanguages.language, StringsOfLanguages.chooseYourLanguage)
                        }}>
                        <View style={[styles.flexDirect, { paddingStart: 15, paddingEnd: 15, paddingTop: 15, paddingBottom: 15, alignItems: 'center', justifyContent: 'center' }]}>
                            <Text style={[styles.textAlignn, {fontFamily: 'SFUIText-Semibold', flex: 1, fontSize: diamens.ternarySize }]}> {StringsOfLanguages.currentLanguage}</Text>
                            <Image source={leftForwardIcon} style={{ height: 15, width: 15 }} />
                        </View>
                    </TouchableOpacity>

                    <View style={{ height: 1, backgroundColor: "#CDCACA" }} />
                </ImageBackground>
            </View>
        );
    }
}


