import messaging from '@react-native-firebase/messaging';
import ApiClient from '../utils/ApiClientService';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";
import React, { Component } from 'react';
import { View, LogBox, Platform } from 'react-native';
import CommanUtils from '../utils/CommanUtils';
import Video from 'react-native-video';
import { NavigationActions, StackActions } from 'react-navigation';

var logoImage = require('../../images/logo.png');
var splashVideo = require('../../video/splash.mp4');

LogBox.ignoreAllLogs();

export default class SplashScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            languag: true,
            isStartSplash: false,
            loginType: 'user',
            loginStatus: false,
            Authorization: '',
            apnsToken: 'hkljhhlkjh'
        }
    }

    async componentDidMount() {
        // this.apnsTokenToFcmToken()
        this.notificationWork()



        var type = await CommanUtils.getLoginType();
        var loginStatus = await CommanUtils.getLoginStatus();

        this.setState({ loginType: type, loginStatus: loginStatus })

        // messaging().onMessage(onMessageReceived);
        // messaging().setBackgroundMessageHandler(onMessageReceived);

        if (loginStatus) {
            var dataFromLocal = await CommanUtils.getLoginDetails();
            this.setState({ Authorization: "Bearer " + dataFromLocal.token })
            ApiClient.getNotification(this.state);
        }

        var isRestart = await CommanUtils.isRestartApp();
        if (isRestart) {
            this.setState({ isStartSplash: false })
            this.proceedForNextScreen();
            CommanUtils.setIsRestartApp(false);
        } else {
            this.setState({ isStartSplash: true })
            setTimeout(async () => {
                this.proceedForNextScreen();
            }, 7000);
        }
    }


    async apnsTokenToFcmToken() {
        const { apnsToken } = this.state;
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            var resdata = await ApiClient.getFcmTokenFromApns(this.state);
            var tokennn = resdata.results[0].registration_token
            CommanUtils.saveToken(tokennn)
            console.log("saved token is ==== ",tokennn)
        }
    }



    async notificationWork() {
        PushNotification.configure({
            onRegister: function (token) {
                console.log("TOKEN:", token);

                if (Platform.OS === 'android') {
                    CommanUtils.saveToken(token.token)
                }
                else {
                    this.setState({ apnsToken: token.token })
                    this.apnsTokenToFcmToken()
                }
            }.bind(this),
            onNotification: function (notification) {
                console.log("NOTIFICATION:", notification);
                try {
                    PushNotification.localNotification({
                        message: "" + notification.data.body,
                        title: "" + notification.data.title,
                        smallIcon: logoImage,
                        allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
                    });
                } catch (error) {
                    console.log("eeeeeeeeeeee", error)
                }

                notification.finish(PushNotificationIOS.FetchResult.NoData);
            },

            onAction: function (notification) {
                console.log("ACTION:", notification.action);
                console.log("NOTIFICATION:", notification);

            },
            onRegistrationError: function (err) {
                console.error(err.message, err);
            },

            // IOS ONLY (optional): default: all - Permissions to register.
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },
            popInitialNotification: true,
            requestPermissions: true,
        });
    }


    // getFcmToken = async () => {
    //     console.log("inside app");

    //     try {
    //         await messaging().registerDeviceForRemoteMessages();

    //         const fcmToken = await messaging().getToken();
    //         console("after")
    //         if (fcmToken) {
    //             console.log("tokennnnnn-----   ", fcmToken);
    //             CommanUtils.saveToken(fcmToken)
    //         } else {
    //             CommanUtils.saveToken("xyz")
    //             console.log("xyz token");
    //         }
    //     } catch (error) {
    //         console.log("00000   ", error)
    //     }

    // }

    async proceedForNextScreen() {
        const { loginStatus } = this.state;
        if (loginStatus && loginStatus != undefined) {
            this.navigateToMainScreen();
        } else {
            this.navigateToLoginScreen();
        }
    }

    async navigateToMainScreen() {
        const { loginType } = this.state;
        var isPaymentSuccess = await CommanUtils.getPaymentStatus();
        // console.log(" payment status " + isPaymentSuccess)
        if (loginType == "user") {
            if (isPaymentSuccess == 'done') {
                this.userHomePaymentSuccess()
            } else {
                this.userHome()
            }
        } else {
            this.vendorHome()
        }

    }


    async userHomePaymentSuccess() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'MainScreen', params: { foo: 'MyOrders', orderType: "completed" } }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
        await CommanUtils.savePaymentStatus("notDone");
    }
    async userHome() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'MainScreen' }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }
    async vendorHome() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'VendorMainScreen' }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }

    navigateToLoginScreen() {
        this.props.navigation.navigate("LanguageScreen");
    }

    render() {
        let videoPlay;
        if (this.state.isStartSplash) {
            videoPlay = (
                <Video source={splashVideo}
                    style={{ flex: 1 }}
                    resizeMode="cover" />
            );
        } else {
            videoPlay = (
                <View></View>
            );
        }
        return (
            <View style={{ flex: 1, backgroundColor: '#000' }}>
                {videoPlay}
            </View>
        );
    }
}


async function onMessageReceived(notification) {
    // Do something
    console.log("notititititiitiiiiii", notification)

    PushNotification.localNotification({
        message: "" + notification.data.body,
        title: "" + notification.data.title,
        smallIcon: logoImage,
        allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    });
}

