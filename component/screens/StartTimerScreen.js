
import React, { Component } from 'react';
import { View, ActivityIndicator, Text, ScrollView, TouchableOpacity } from 'react-native';
import CardView from 'react-native-cardview'
import style from '../utils/style';
import colors from '../utils/colors';
import CustToo from '../helper/CustomToolbarBackBtn';
import { Stopwatch, Timer } from 'react-native-stopwatch-timer'

import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import StringsOfLanguages from '../utils/StringsOfLanguages';
import diamens from '../utils/diamens';

var date = require('../../images/date.png');
var down_arrow = require('../../images/down_arrow.png');



export default class StartTimerScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            languag: true,
            langValue: "en",
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
            hours_Counter: '00',
            vendorServiceId: 0,
            startPauseToggle: true,
            Authorization: 'thf',
            isInvoiceSent: 'false',
            additionalCost: '0',
            priceQuote: '0',
        }
        this.toggleStopwatch = this.toggleStopwatch.bind(this);
    }



    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        const vendorServiceI = this.props.navigation.getParam('vendorServiceId', 0);
        const priceQuote = this.props.navigation.getParam('priceQuote', 0);
        this.setState({ languag: lan, vendorServiceId: vendorServiceI, priceQuote: priceQuote, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
    }


    toggleStopwatch() {
        if (this.state.startPauseToggle) {
            this.startTimmer()
        }
        else {
            this.pauseTimmer()
        }
    }


    pauseTimmer() {
        clearInterval(this.state.timer);
        this.setState({ startPauseToggle: true })
    }

    startTimmer() {

        let timer = setInterval(() => {

            var num = (Number(this.state.seconds_Counter) + 1).toString(),
                count = this.state.minutes_Counter,
                hours = this.state.hours_Counter;

            if (Number(this.state.seconds_Counter) == 59) {
                count = (Number(this.state.minutes_Counter) + 1).toString();
                num = '00';

                if (Number(this.state.minutes_Counter) == 59) {
                    hours = (Number(this.state.hours_Counter) + 1).toString();
                    num = '00';
                    count = '00';
                }

            }
            this.setState({
                hours_Counter: hours.length == 1 ? '0' + hours : hours,
                minutes_Counter: count.length == 1 ? '0' + count : count,
                seconds_Counter: num.length == 1 ? '0' + num : num,
            });
        }, 1000);

        this.setState({ timer });
        this.setState({ startPauseToggle: false })
    }



    async hitGenerateInvoice() {

        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.generateInvoice(this.state);
            if (responceData != undefined) {
                if (responceData.status) {
                    // await CommanUtils.showAlert(StringsOfLanguages.success, '' + responceData.message)
                    this.navigateToGenerateInvoice(responceData.data)
                }
                else {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                }
            }
        }
        this.setState({ loading: false })
    };

    navigateToGenerateInvoice(data) {
        this.props.navigation.navigate("GenerateInvoiceScreen", {
            data: data,
        })
    }

    render() {
        let styles;
        let progressBar = null;

        const { languag, loading, hours_Counter, minutes_Counter, seconds_Counter } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            progressBar = (
                <View />
            );
        }




        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.startJob} />
                <ScrollView >

                    <CardView style={{ backgroundColor: '#fff', margin: 20, padding: 10 }}
                        cardElevation={2}
                        cardMaxElevation={2}
                        cornerRadius={10}>

                        <View style={{ padding: 10 }}>

                            <Text style={{ color: "#4384FF", fontSize: 20, alignSelf: 'center', fontFamily: 'SFUIText-Bold', marginStart: 10, marginEnd: 10 }}>{StringsOfLanguages.job} {!this.state.startPauseToggle ? StringsOfLanguages.started : StringsOfLanguages.stopped}</Text>

                            <View style={[styles.flexDirect, { marginTop: 20, width: 185, alignSelf: 'center' }]}>
                                <Text style={{ flex: 1, color: "#707070", fontSize: 12, fontFamily: 'SFUIText-Bold', textAlign: 'center' }}> hours </Text>
                                <Text style={{ flex: 1, color: "#707070", fontSize: 12, fontFamily: 'SFUIText-Bold', textAlign: 'center' }}> min </Text>
                                <Text style={{ flex: 1, color: "#707070", fontSize: 12,fontFamily: 'SFUIText-Bold', textAlign: 'center' }}> sec </Text>
                            </View>

                            <Text style={{ color: "black", fontWeight: 'bold', fontSize: 35, fontFamily: 'SFUIText-Bold', alignSelf: 'center', marginTop: 5 }}> {hours_Counter} : {minutes_Counter} : {seconds_Counter} </Text>

                            {progressBar}

                            <View style={[styles.flexDirect, { marginTop: 30 }]}>
                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity style={[styles.btnPressSmall, { width: 130, marginBottom: 5, alignSelf: 'center' }]}
                                        onPress={() => {
                                            this.toggleStopwatch();
                                        }}>
                                        <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: diamens.veryTinySize }}>{this.state.startPauseToggle ? StringsOfLanguages.startCap : StringsOfLanguages.stopCap}</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity style={[styles.btnPressSmall, { width: 130, marginBottom: 5, alignSelf: 'center' }]}
                                        onPress={() => {
                                            this.pauseTimmer()
                                            this.hitGenerateInvoice()
                                        }}>
                                        <Text style={{ color: "white", fontFamily: 'SFUIText-Bold',fontSize: diamens.veryTinySize}}>{StringsOfLanguages.generateInvoiceCap}</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>

                        </View>
                    </CardView>
                </ScrollView>
            </View>
        );
    }
}
