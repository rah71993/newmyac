
import React, { Component } from 'react';
import { View, ImageBackground, Image, Text, TouchableOpacity, Dimensions } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import style from '../utils/style';
import StringsOfLanguages from '../utils/StringsOfLanguages';
import colors from '../utils/colors';
import CommanUtils from '../utils/CommanUtils';
import GeneralStatusBarColor from "react-native-general-statusbar";
import diamens from '../utils/diamens';
var bgImage = require('../../images/main_bg.png');
var man_with_mask = require('../../images/man_with_mask.png');
var iconUser = require('../../images/icon_user.png');
var iconVendor = require('../../images/icon_vendor.png');
var select = require('../../images/select.png');

export default class SuccessQuoteScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            languag: true,
        }
    }


    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }

    navigateToMain() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'VendorMainScreen', params: { jobType: "new" } }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }

    render() {

        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
            // alert("english")
        } else {
            // alert("arabic "+this.state.languag)
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }


        return (
            <View style={{ flex: 1 }}>
                {/* <View style={styles.status_styl}></View> */}

                <GeneralStatusBarColor
                    backgroundColor={colors.statusColor}
                    barStyle="light-content" />

                <ImageBackground style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} source={bgImage}>


                    <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>

                        <Image source={select} style={[styles.logo_style, { marginTop: 0 }]} />

                        <Text style={[styles.homeBlueHead, { fontFamily: 'SFUIText-Bold', fontSize: diamens.primarySize, marginTop: 20, textAlign: 'center' }]}>
                            {StringsOfLanguages.quotationSubmitted} {'\n'} {StringsOfLanguages.successfully}
                        </Text>

                        <Text style={[{ fontFamily: 'SFUIText-Medium', fontSize: diamens.tinySize, color: colors.blue, marginTop: 10, textAlign: 'center' }]}>
                            {StringsOfLanguages.yourquotationissuccessfullysenttotheuser}
                        </Text>

                    </View>

                    <TouchableOpacity style={[styles.btnPress, { marginTop: 10, position: 'absolute', bottom: 20 }]} onPress={() =>
                        this.navigateToMain()
                    }>
                        <Text style={[styles.textButtonPress, { marginStart: 10, marginEnd: 10 }]}>{StringsOfLanguages.myOrdersCap}   </Text>
                    </TouchableOpacity>

                </ImageBackground>

            </View>
        );
    }
}




