import CustToo from '../helper/CustomToolbar';

import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, StatusBar, ImageBackground } from 'react-native';
import { WebView } from 'react-native-webview';
import CardView from 'react-native-cardview'
import ApiClient from '../utils/ApiClientService';
import CommanUtils from '../utils/CommanUtils';
import style from '../utils/style';
import colors from '../utils/colors';
import StringsOfLanguages from '../utils/StringsOfLanguages'
var bgImage = require('../../images/main_bg.png');
var help = require('../../images/help.png');
var right_icon = require('../../images/arrow-right.png');


export default class TermAndConditionScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            languagValue: "en",
            loading: false,
            id: 8,
            title: StringsOfLanguages.termsAndCondition,
            description: '',
        }
    }

    async  componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.hitPageDetails()
    }
    async hitPageDetails() {

        this.setState({ loading: true })
        var responceData = await ApiClient.getPageDetails(this.state);
        if (responceData != undefined) {
            this.setState({ description: responceData.data.description })
        }
        this.setState({ loading: false })
    };

    render() {
        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }


        const ActivityIndicatorElement = () => {
            //making a view to show to while loading the webpage
            return (
                <ActivityIndicator
                    color="#000"
                    size="large"
                    style={{ flex: 1, alignSelf: 'center', marginTop: 250, position: 'absolute' }}
                />
            );
        }

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={this.state.title} />

                <ImageBackground source={bgImage} style={{ flex: 1, padding: 15 }}>
                    <WebView
                    style={{backgroundColor: 'transparent',}}
                        source={{ html: this.state.description }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        renderLoading={ActivityIndicatorElement}
                        startInLoadingState={true} />

                </ImageBackground>


            </View>
        );
    }
}


