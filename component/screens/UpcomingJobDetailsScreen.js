import CustToo from '../helper/CustomToolbarBackBtn';

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Image, StatusBar, ImageBackground, Linking } from 'react-native';
import style from '../utils/style';
import { ScrollView } from 'react-native-gesture-handler';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens, { ternarySize } from '../utils/diamens';
import colors, { blue } from '../utils/colors';
var bgImage = require('../../images/main_bg.png');


export default class UpcomingJobDetailsScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            description: '',
            address: 'address',
            latitude: 0.0,
            longitude: 0.0,
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        const latitude = this.props.navigation.getParam('latitude', 0.0);
        const longitude = this.props.navigation.getParam('longitude', 0.0);
        const location = this.props.navigation.getParam('location', 'no data');

        this.setState({ languag: lan, latitude: latitude, longitude: longitude, address: location });
        console.log(this.state)
    }

    navigateToMap() {
        this.props.navigation.navigate("MapViewM", { title: this.state.address, latitude: this.state.latitude, longitude: this.state.longitude })
    }

    dialogWrongDate(scheduleDate, today) {
        CommanUtils.showAlert(StringsOfLanguages.alert, StringsOfLanguages.scheduled + " " + StringsOfLanguages.date + " " + scheduleDate + " " + StringsOfLanguages.today + " " + today)
    }

    render() {
        let styles;
        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        const imageUser = this.props.navigation.getParam('image_url', 'no data');
        const title = this.props.navigation.getParam('title', 'no data');
        const serviceType = this.props.navigation.getParam('serviceType', 'no data');
        const distance = this.props.navigation.getParam('distance', 'no data');
        const location = this.props.navigation.getParam('location', 'no data');
        const description = this.props.navigation.getParam('description', 'no data');
        const priceQuote = this.props.navigation.getParam('priceQuote', 0);
        const vendorServiceId = this.props.navigation.getParam('vendorServiceId', 0);
        const scheduleDate = this.props.navigation.getParam('scheduleDate', 'no data');
        const scheduleTime = this.props.navigation.getParam('scheduleTime', 'no data');
        const acType = this.props.navigation.getParam('acType', 'no data');
        const noOfAc = this.props.navigation.getParam('noOfAc', 'no data');
        const customerContact = this.props.navigation.getParam('customerContact', '1234567890');

        return (
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor='#4384FF' />

                <CustToo valueFromParent={StringsOfLanguages.jobDetails} />

                <ImageBackground source={bgImage} style={{ flex: 1 }}>

                    <ScrollView>

                        <View style={{ margin: 15, flex: 1 }}>

                            <View style={{ flexDirection: 'row' }}>
                                <Image source={{ uri: imageUser }} style={{ height: 60, width: 60, borderRadius: 30, backgroundColor: '#000' }} />

                                <View style={{ marginLeft: 15, flex: 1, marginTop: 20 }}>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', }]}>{title}</Text>
                                </View>

                                <View style={{ marginLeft: 15, flex: 1, }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: '#4384FF' }]}> {distance} Km away</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.ternarySize, marginTop: 7 }]}>{StringsOfLanguages.location}: </Text>
                                    <TouchableOpacity style={{}}
                                        onPress={() => {
                                            this.navigateToMap()
                                        }}>
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', textDecorationLine: 'underline', fontSize: diamens.tinySize, color: blue }]}>{location}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={[styles.flexDirect, { marginTop: 10 }]}>
                                <View style={{ marginLeft: 10, marginRight: 10, flex: 1 }}>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7 }]}>{StringsOfLanguages.scheduledDate} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', }]}>{scheduleDate}</Text>
                                </View>

                                <View style={{ height: 60, width: 60 }}></View>

                                <View style={{ marginLeft: 15, flex: 1, }}>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', marginTop: 7 }]}>{StringsOfLanguages.scheduledTime} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', }]}>{scheduleTime}</Text>
                                </View>
                            </View>

                            <View style={[styles.flexDirect, { marginTop: 15 }]}>
                                <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize }]}>{StringsOfLanguages.serviceType} :</Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 5 }]}>{serviceType}</Text>
                                </View>

                                <View style={{ marginLeft: 0, marginRight: 0, flex: 1 }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize }]}>{StringsOfLanguages.typeOfAc} : </Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 5 }]}> {acType} </Text>
                                </View>

                                <View style={{ marginLeft: 0, marginRight: 0, alignItems: 'center' }}>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize }]}>{StringsOfLanguages.noOfAc} : </Text>
                                    <Text numberOfLines={1} style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', fontSize: ternarySize, marginTop: 5 }]}> {noOfAc} </Text>
                                </View>
                            </View>



                            <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize, marginTop: 25 }]}>{StringsOfLanguages.description}</Text>

                            <Text style={[styles.textAlignn, { marginTop: 7 }]}>{description} </Text>



                        </View>
                    </ScrollView>

                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Bold', color: colors.blue, fontSize: ternarySize, marginTop: 10, marginEnd: 10, marginStart: 10 }]}>{StringsOfLanguages.priceQuoted}</Text>

                    <View style={[styles.flexDirect, { alignItems: 'center', marginTop: 5, marginEnd: 10, marginStart: 10 }]}>
                        <Text numberOfLines={1} style={[styles.textAlignn, { flex: 1, fontFamily: 'SFUIText-Bold', marginStart: 5, marginEnd: 5 }]}>{priceQuote} {StringsOfLanguages.currencySymbol}</Text>
                    </View>

                    <TouchableOpacity style={[styles.btnPress, { marginTop: 10, alignSelf: 'center' }]}
                        onPress={() => {
                            if (scheduleDate === formatedDate(new Date)) {
                                this.props.navigation.navigate("StartTimerScreen", {
                                    title: title,
                                    serviceType: serviceType,
                                    distance: distance,
                                    location: location,
                                    image_url: imageUser,
                                    description: description,
                                    priceQuote: priceQuote,
                                    scheduleDate: scheduleDate,
                                    scheduleTime: scheduleTime,
                                    vendorServiceId: vendorServiceId,
                                });
                            } else {
                                this.dialogWrongDate(scheduleDate, formatedDate(new Date))
                                return
                            }
                        }}>
                        <Text style={{ color: "white", fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }}>{StringsOfLanguages.startJobCap}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={[styles.btnBorder, { marginTop: 10, alignSelf: 'center', marginBottom: 10 }]}
                        onPress={() => {
                            Linking.openURL(`tel:${customerContact}`)
                        }}>
                        <Text style={{ color: colors.colorPrimaryDark, fontFamily: 'SFUIText-Bold', fontSize: diamens.ternarySize }}>{StringsOfLanguages.contactCustomerCap}</Text>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
        );
    }
}
function formatedDate(date) {
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    day = day < 10 ? '0' + day : day;
    month = month < 10 ? '0' + month : month;
    var formatedDate = day + "-" + month + "-" + year
    return formatedDate;
}


