
import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity, ActivityIndicator, ImageBackground, Alert } from 'react-native';
import colors from '../utils/colors';
import style from '../utils/style';
import diamens from '../utils/diamens';
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import CustToo from '../helper/CustomToolbarBackBtn';
import { NavigationActions, StackActions } from 'react-navigation';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import ImagePicker from 'react-native-image-crop-picker';
import CardView from 'react-native-cardview'
import DocumentPicker from 'react-native-document-picker'
var documentAttach = require('../../images/document.png');

var plus = require('../../images/plus.png');
var editImage = require('../../images/edit.png');
var bgImage = require('../../images/main_bg.png');
var logoutImage = require('../../images/logout.png');


export default class VendorEditProfileScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            name: '',
            email: '',
            mobile_number: '',
            mobile_code: '',
            companyName: '',
            crNumber: '',
            vatNumber: '',
            city: '',
            stateProvisionRegion: '',
            zipPostalCode: '',
            address: '',
            iqmah: '',
            profile_pic: '',
            attachDocuments: [],
            Authorization: '',

            nameEnabled: true,
            emailEnabled: true,
            companyNameEnabled: true,
            crNumberEnabled: true,
            vatEnabled: true,
            iqmahEnabled: true,
            addressEnabled: true,
            cityEnabled: true,
            stateEnabled: true,
            zipEnabled: true,
            langValue: "en",
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan, })
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.showDataFromLocal()
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.showDataFromLocal()
        });
    }

    async showDataFromLocal() {
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({
            name: dataFromLocal.name,
            email: dataFromLocal.email,
            mobile_number: dataFromLocal.mobile_number,
            mobile_code: dataFromLocal.mobile_code,
            companyName: dataFromLocal.vendorName,
            crNumber: dataFromLocal.crn,
            vatNumber: dataFromLocal.vatNo,
            city: dataFromLocal.city,
            stateProvisionRegion: dataFromLocal.state,
            zipPostalCode: dataFromLocal.zipcode,
            address: dataFromLocal.address,
            profile_pic: dataFromLocal.profile_pic,
            iqmah: dataFromLocal.iqmah,
            Authorization: "Bearer " + dataFromLocal.token,
        });


        var loginStatus = await CommanUtils.getLoginStatus();
        console.log(loginStatus);
        if (loginStatus) {
            this.setState({
                nameEnabled: false,
                emailEnabled: false,
                companyNameEnabled: false,
                crNumberEnabled: false,
                vatEnabled: false,
                iqmahEnabled: false,
                addressEnabled: false,
                cityEnabled: false,
                stateEnabled: false,
                zipEnabled: false,
            })
        }
    }

    async onUpdateClick() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.updateVendorProfile(this.state);
            this.setState({ loading: false })
            if (responceData != undefined) {
                if (responceData.status) {
                    CommanUtils.saveLoginDetails(responceData);
                    var loginStatus = await CommanUtils.getLoginStatus();
                    CommanUtils.showAlert(StringsOfLanguages.success, '' + responceData.message)
                    if (loginStatus) {
                        this.showDataFromLocal()
                    } else {
                        this.navigateToMainScreen()
                    }
                } else {
                    CommanUtils.showAlert(StringsOfLanguages.error, '' + responceData.message)
                }
            }
        }
    }

    pickImage() {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            console.log(image);
            this.setState({ profile_pic: image.path })
        });

    }

  async  multipleImagePicker() {
       
        try {
            const results = await DocumentPicker.pickMultiple({
              type: [DocumentPicker.types.pdf,DocumentPicker.types.images]
            })
            this.setState({ attachDocuments: results })
           console.log("results data ....",results)
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              // User cancelled the picker, exit any dialogs or menus and move on
            } else {
              throw err
            }
          }
    }

    onChangeText = ({ dialCode, unmaskedPhoneNumber, phoneNumber, isVerified }) => {
        this.setState({ mobile_code: dialCode, mobile_number: unmaskedPhoneNumber, isValidPhone: isVerified })
        console.log(dialCode, unmaskedPhoneNumber, phoneNumber, isVerified);
    };

    navigateToMainScreen() {
        CommanUtils.saveLoginStatus(true);
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'VendorMainScreen' })],
        });
        this.props.navigation.dispatch(resetAction)
    }

    showLogoutAlert = (title, message) => {
        Alert.alert(
            title,
            message,
            [
                {
                    text: StringsOfLanguages.cancel,
                    onPress: () =>
                        console.log('Cancel Pressed'),
                },
                {
                    text: StringsOfLanguages.logout,
                    onPress: () => {
                        CommanUtils.saveLoginStatus(false);
                        this.gotoLogin()
                    }
                }
            ],
            { cancelable: false }
        );

    }
    gotoLogin() {
        const loginScreenAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'App', params: { 'initialScreen': 'ChooseUserVender' } }),
            ],
        });
        this.props.navigation.dispatch(loginScreenAction)
    }

    render() {
        let styles;
        let progressBar;
        let nameView;
        let emailView;
        let companyNameView;
        let crNumberView;
        let vatView;
        let iqmahView;
        let addressView;
        let cityView;
        let stateView;
        let zipView;

        const { loading, nameEnabled, emailEnabled, companyNameEnabled, crNumberEnabled, vatEnabled, iqmahEnabled, addressEnabled, cityEnabled, stateEnabled, zipEnabled,
            name, email, mobile_number, mobile_code, address, profile_pic, companyName, crNumber, vatNumber, iqmah, city, stateProvisionRegion, zipPostalCode
        } = this.state;

        if (this.state.languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            progressBar = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else {
            <View></View>
        }

        //Enable disable work for Text Input field
        if (nameEnabled) {
            nameView = (
                <View>
                    <Text style={[styles.smallTextStyle, {  marginTop: 0 }]}>{StringsOfLanguages.name}</Text>
                    <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                        <TextInput
                            style={[styles.inputTxt]}
                            value={name}
                            onChangeText={text => this.setState({ name: text })} />
                    </View>
                </View>
            )
        } else {
            nameView = (
                <View style={[styles.flexDirect, { justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <Text style={{ flex: 1, marginStart: 15, marginEnd: 15, textAlign: 'center',fontFamily: 'SFUIText-Bold', fontSize: 16, marginTop: 10, color: 'black' }}>{name}</Text>
                    <TouchableOpacity style={{}} onPress={() => { this.setState({ nameEnabled: !nameEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, styles.alignSelfFlexEnd, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>)
        }

        if (emailEnabled) {
            emailView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={email}
                        onChangeText={text => this.setState({ email: text })} />
                </View>
            )
        } else {
            emailView = (
                <View style={[styles.flexDirect, styles.textAlignn, { justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize, }]}>{email}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ emailEnabled: !emailEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }
        if (companyNameEnabled) {
            companyNameView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={companyName}
                        onChangeText={text => this.setState({ companyName: text })} />
                </View>
            )
        } else {
            companyNameView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{companyName}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ companyNameEnabled: !companyNameEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (crNumberEnabled) {
            crNumberView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={crNumber}
                        maxLength={10}
                        onChangeText={text => this.setState({ crNumber: text })} />
                </View>
            )
        } else {
            crNumberView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5,fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{crNumber}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ crNumberEnabled: !crNumberEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (vatEnabled) {
            vatView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={vatNumber}
                        maxLength={15}
                        onChangeText={text => this.setState({ vatNumber: text })} />
                </View>
            )
        } else {
            vatView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5,fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{vatNumber}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ vatEnabled: !vatEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (iqmahEnabled) {
            iqmahView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={iqmah}
                        maxLength={10}
                        onChangeText={text => this.setState({ iqmah: text })} />
                </View>
            )
        } else {
            iqmahView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular',flex: 1, fontSize: diamens.secondrySize }]}>{iqmah}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ iqmahEnabled: !iqmahEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (addressEnabled) {
            addressView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0, paddingBottom: 0, paddingTop: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt, {}]}
                        value={address}
                        onChangeText={text => this.setState({ address: text })} />
                </View>
            )
        } else {
            addressView = (
                <View style={[styles.flexDirect, { justifyContent: 'center', alignItems: 'center', width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{address}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ addressEnabled: !addressEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (cityEnabled) {
            cityView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={city}
                        onChangeText={text => this.setState({ city: text })} />
                </View>
            )
        } else {
            cityView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{city}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ cityEnabled: !cityEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (stateEnabled) {
            stateView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={stateProvisionRegion}
                        onChangeText={text => this.setState({ stateProvisionRegion: text })} />
                </View>
            )
        } else {
            stateView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{stateProvisionRegion}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ stateEnabled: !stateEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }

        if (zipEnabled) {
            zipView = (
                <View style={[styles.inputView, { paddingEnd: 0, width: '100%', paddingStart: 0 }]}>
                    <TextInput
                        style={[styles.inputTxt]}
                        value={zipPostalCode}
                        onChangeText={text => this.setState({ zipPostalCode: text })} />
                </View>
            )
        } else {
            zipView = (
                <View style={[styles.flexDirect, { width: '100%' }]}>
                    <Text style={[styles.textAlignn, { marginTop: 5, fontFamily: 'SFUIText-Regular', flex: 1, fontSize: diamens.secondrySize }]}>{zipPostalCode}</Text>
                    <TouchableOpacity onPress={() => { this.setState({ zipEnabled: !zipEnabled }) }}>
                        <Image source={editImage} style={[styles.ImageIconStyle, { height: 15, width: 15 }]} />
                    </TouchableOpacity>
                </View>
            )
        }



        return (

            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.myAccount} />

                <ImageBackground source={bgImage} style={{ flex: 1 }}>

                    <ScrollView>
                        <View style={{ flex: 1, margin: 15 }}>

                            <View style={[{ alignSelf: 'center', marginTop: 20, }]}
                                onStartShouldSetResponder={
                                    () => this.pickImage()
                                }>
                                <View style={[styles.profileImgBackground, { alignSelf: 'center' }]}>
                                    <Image source={{ uri: profile_pic }} style={[styles.profileImg, {}]} />

                                    <View style={{ position: 'absolute', alignSelf: 'flex-end', bottom: 0, height: 30, width: 30, borderRadius: 15, backgroundColor: colors.colorPrimaryDark, justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={plus} style={[{ tintColor: colors.white, height: 15, width: 15, }]} />
                                    </View>
                                </View>
                            </View>

                            {nameView}

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.email}</Text>
                            {emailView}

                            <Text style={[styles.smallTextStyle, {marginTop: 20 }]}>{StringsOfLanguages.mobileNumber}</Text>
                            <View style={[styles.flexDirect, { width: '100%' }]}>
                                <Text style={[styles.textAlignn, { flex: 1, marginTop: 5, fontFamily: 'SFUIText-Regular', fontSize: diamens.secondrySize }]}>{mobile_code}-{mobile_number}</Text>
                            </View>

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.companyName}</Text>
                            {companyNameView}

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.crNumber}</Text>
                            {crNumberView}

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.vatNumber}</Text>
                            {vatView}

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.iqmah}</Text>
                            {iqmahView}

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.address}</Text>
                            {addressView}


                            <Text style={[styles.smallTextStyle, {marginTop: 20 }]}>{StringsOfLanguages.city}</Text>
                            {cityView}

                            <Text style={[styles.smallTextStyle, {marginTop: 20 }]}>{StringsOfLanguages.stateProvisionRegion}</Text>
                            {stateView}

                            <Text style={[styles.smallTextStyle, { marginTop: 20 }]}>{StringsOfLanguages.zipPostalCode}</Text>
                            {zipView}

                            {progressBar}

                            <CardView
                                style={{ backgroundColor: '#fff', marginBottom: 1, marginTop: 20, paddingStart: 10, paddingEnd: 10, width: '80%', alignSelf: 'center' }}
                                cardElevation={2}
                                cardMaxElevation={2}
                                cornerRadius={5}>
                                <View style={[styles.inputView, { paddingEnd: 0, marginTop: 0, width: '100%', backgroundColor: 'white', paddingStart: 0 }]}
                                    onStartShouldSetResponder={
                                        () => this.multipleImagePicker()
                                    }>
                                    <Image source={documentAttach} style={{ tintColor: colors.colorPrimaryDark, marginLeft: 10, marginRight: 10, height: 15, width: 15 }} />
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Medium',color: colors.colorPrimaryDark, flex: 1 }]}> {StringsOfLanguages.attachDocuments}</Text>
                                </View>
                            </CardView>


                            <TouchableOpacity style={[styles.btnPress, { marginTop: 20, alignSelf: 'center' }]} onPress={() => {
                                this.onUpdateClick()
                            }}>
                                <Text style={styles.textButtonPress}>{StringsOfLanguages.saveCap}</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={{ marginTop: 30, marginBottom: 30, alignSelf: 'baseline', alignSelf: 'center' }}
                                onPress={() => { this.showLogoutAlert(StringsOfLanguages.alert, StringsOfLanguages.areSureWantToLogout); }}>
                                <View style={[styles.flexDirect, { alignSelf: 'center', padding: 7, }]}>
                                    <Image source={logoutImage} style={{ alignSelf: 'center', marginStart: 0, height: 21, width: 20 }} />
                                    <Text style={{ fontFamily: 'SFUIText-Semibold',marginStart: 10, fontSize: 16 }}>{StringsOfLanguages.signOut}</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </ScrollView>

                </ImageBackground>

            </View>

        );
    }
}

