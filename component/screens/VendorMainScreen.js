import 'react-native-gesture-handler';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import CustomMenuSidebar from '../helper/VendorCustomMenuSidebar';
import JobHistoryScreen from './JobHistoryScreen';
import GenerateInvoiceScreen from './GenerateInvoiceScreen';
import RateUs from './RateUsScreen';
import Help from './HelpScreen';
import Settings from './SettingsScreen';
import TermAndConditionScreen from './TermAndConditionScreen';
import PrivacyPolicyScreen from './PrivacyPolicyScreen';
import Profile from './VendorProfileScreen';
import WebViewScreen from './WebViewScreen';
import NewJobDetailsScreen from './NewJobDetailsScreen';
import UpcomingJobDetailsScreen from './UpcomingJobDetailsScreen';
import StartTimerScreen from './StartTimerScreen';
import VendorEditProfileScreen from './VendorEditProfileScreen';
import React, { Component } from 'react';
import { View, Image } from 'react-native';
import style from '../utils/style';
import CommanUtils from '../utils/CommanUtils';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import colors from '../utils/colors';
import HelpScreen from './HelpScreen';
import SuccessQuoteScreen from './SuccessQuoteScreen';
import SuccessInvoiceSendScreen from './SuccessInvoiceSendScreen';
import PaymentDash from '../paymentDashboard/PaymentDash';
import InvoiceDetailsScreen from './InvoiceDetailsScreen';
import NotificationScreen from './NotificationScreen';
import LanguageScreen from './LanguageScreen';
import SettingsScreen from './SettingsScreen';
import App from '../../App';
import CovidScreen from './CovidScreen';
import MapViewM from './MapViewM';
var orderIcon = require('../../images/order_history.png');
var rate = require('../../images/rate.png');
var help = require('../../images/help.png');
var settingIcon = require('../../images/setting.png');
var privacyPolicyIcon = require('../../images/privacy_policy.png');
var terms_and_conditionsIcon = require('../../images/terms_and_conditions.png');


export default class VendorMainScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        this.setState({ languag: lan });
    }

    render() {
        let styles;
        const { languag } = this.state;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        const jobType = this.props.navigation.getParam('jobType', 'new');


        const MyDrawerNavigator = createDrawerNavigator(
            {
                "JobHistory": {
                    navigationOptions: {
                        drawerLabel: languag ? "Job History" : "تفاصيل الأعمال",
                        drawerIcon: (
                            <Image source={orderIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
                        ),
                    },
                    screen: JobHistoryScreen
                },
                "RateUs": {
                    navigationOptions: {
                        drawerLabel: languag ? "Rate Us" : "قيمنا",
                        drawerIcon: (
                            <Image source={rate} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
                        ),
                    },
                    screen: RateUs
                },
                "Help": {
                    navigationOptions: {
                        drawerLabel: languag ? "Help" : "مساعدة",
                        drawerIcon: (
                            <Image source={help} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
                        ),
                    },
                    screen: HelpScreen
                },
                "Settings": {
                    navigationOptions: {
                        drawerLabel: languag ? "Settings" : "إعدادات",
                        drawerIcon: (
                            <Image source={settingIcon} style={{ width: 20, height: 20, tintColor: '#4384FF'}}></Image>
                        ),
                    },
                    screen: SettingsScreen
                },
                "TermAndCondition": {
                    navigationOptions: {
                        drawerLabel: languag ? "Terms and Condition" : "الشروط والأحكام",
                        drawerIcon: (
                            <Image source={terms_and_conditionsIcon} style={{ width: 20, height: 20, tintColor: '#4384FF'}}></Image>
                        ),
                    },
                    screen: TermAndConditionScreen
                },
                "PrivacyPolicy  ": {
                    navigationOptions: {
                        drawerLabel: languag ? "Privacy Policy" : "سياسة الخصوصية",
                        drawerIcon: (
                            <Image source={privacyPolicyIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', resizeMode: 'contain' }}></Image>
                        ),
                    },
                    screen: PrivacyPolicyScreen
                },
            },
            {
                initialRouteName: 'JobHistory',
                initialRouteParams: { jobType: jobType },
                contentComponent: CustomMenuSidebar,
                drawerPosition: languag ? 'left' : 'right',
                contentOptions: {
                    activeTintColor: colors.black,
                    itemsContainerStyle: {
                        // opacity: 1
                    },
                    iconContainerStyle: {
                        // opacity: 1
                    },
                    itemStyle: {
                        flexDirection: languag ? 'row' : 'row-reverse',
                    },
                    labelStyle: styles.drawerItemStyle,
                },
            });

        const DrawerNavigatorExample = createStackNavigator({
            Drawer: {
                screen: MyDrawerNavigator,
                navigationOptions: { headerShown: false, }
            },
            PrivacyPolicyScreen: {
                screen: PrivacyPolicyScreen,
                navigationOptions: { headerShown: false, },
            },
            TermAndConditionScreen: {
                screen: TermAndConditionScreen,
                navigationOptions: { headerShown: false, },
            },
            Profile: {
                screen: Profile,
                navigationOptions: { headerShown: false, },
            },
            NewJobDetailsScreen: {
                screen: NewJobDetailsScreen,
                navigationOptions: { headerShown: false, },
            },
            UpcomingJobDetailsScreen: {
                screen: UpcomingJobDetailsScreen,
                navigationOptions: { headerShown: false, },
            },
            WebViewScreen: {
                screen: WebViewScreen,
                navigationOptions: { headerShown: false, },
            },

            StartTimerScreen: {
                screen: StartTimerScreen,
                navigationOptions: { headerShown: false, },
            },
            VendorEditProfileScreen: {
                screen: VendorEditProfileScreen,
                navigationOptions: { headerShown: false, },
            },
            GenerateInvoiceScreen: {
                screen: GenerateInvoiceScreen,
                navigationOptions: { headerShown: false, },
            },
            VendorMainScreen: {
                screen: VendorMainScreen,
                navigationOptions: { headerShown: false, },
            },
            SuccessQuoteScreen: {
                screen: SuccessQuoteScreen,
                navigationOptions: { headerShown: false, },
            },
            SuccessInvoiceSendScreen: {
                screen: SuccessInvoiceSendScreen,
                navigationOptions: { headerShown: false, },
            },
            PaymentDash: {
                screen: PaymentDash,
                navigationOptions: { headerShown: false, },
            },
            NotificationScreen: {
                screen: NotificationScreen,
                navigationOptions: { headerShown: false, },
            },
            App: {
                screen: App,
                navigationOptions: { headerShown: false, },
            },
            CovidScreen: {
                screen: CovidScreen,
                navigationOptions: { headerShown: false, },
            },
            MapViewM: {
                screen: MapViewM,
                navigationOptions: { headerShown: false, },
            },
           
        });
        const MyApp = createAppContainer(DrawerNavigatorExample);

        return (

            <View style={{ flex: 1 }}>
                <MyApp />
            </View>
        );
    }
}




