
import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator, Text, Image, ImageBackground } from 'react-native';
import ApiClient from '../utils/ApiClientService';
import colors, { blue, colorPrimaryDark } from '../utils/colors';
import style from '../utils/style';
import CustToo from '../helper/CustomToolbarBackBtn';
import CommanUtils from '../utils/CommanUtils';
import CustomRow from '../list_row/VendorQuotesRow';
import CustomRowSchedule from '../list_row/VendorQuotesScheduleRow';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import diamens from '../utils/diamens';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';

var sortIcon = require('../../images/sort.png');
var bgImage = require('../../images/main_bg.png');

export default class VendorQuotesScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loading: false,
            langValue: "en",
            priceQuoteData: [],
            Authorization: 'bb',
            serviceRequestId: 0,
            shortBy:''
        }
    }

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        const orderType = this.props.navigation.getParam('serviceRequestId', 0);

        this.setState({ languag: lan, serviceRequestId: orderType, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        this.hitVendorQuotes()
    }

    async hitVendorQuotes() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.getVendorQuotes(this.state);
            if (responceData != undefined) {
                this.setState({ priceQuoteData: responceData.vendorPriceQuote })
            }
            this.setState({ loading: false })
        }
    };

    _menu = null;

    setMenuRef = ref => {
        this._menu = ref;
    };

    hideMenu = (valu) => {
        this._menu.hide();
        if (valu === 1) {
            this.setState({shortBy:'rating'})
            this.hitVendorQuotes()
        }
        else if (valu === 2) {
            this.setState({shortBy:'priceAsc'})
            this.hitVendorQuotes()
        }
        else if (valu === 3) {
            this.setState({shortBy:'priceDesc'})
            this.hitVendorQuotes()
        }
        else if (valu === 4) {
            this.setState({shortBy:'distance'})
            this.hitVendorQuotes()
        }
    };

    showMenu = () => {
        this._menu.show();
    };

    render() {
        const { languag, loading, priceQuoteData } = this.state
        let styles;
        let vendorQuotesList;
        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            vendorQuotesList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else if (priceQuoteData && priceQuoteData.length) {
            vendorQuotesList = (
                <FlatList
                    data={priceQuoteData}
                    keyExtractor={priceQuoteData.vendorServiceId}
                    renderItem={({ item }) => {
                        if (item.isInvoiceSent === 'true') {
                            return <CustomRowSchedule item={item} styles={styles} langu={this.state.languag} props={this.props} />
                        } else {
                            return <CustomRow item={item} styles={styles} props={this.props} />
                        }
                    }} />
            );
        } else {
            vendorQuotesList = (
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>{StringsOfLanguages.noResultFound}</Text>
                </View>
            );
        }

        return (
            <View style={{ flex: 1 }}>

                <CustToo valueFromParent={StringsOfLanguages.vendorQuotes} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    <View style={styles.alignSelfFlexEnd}>
                        <Menu
                            ref={this.setMenuRef}
                            button={
                                <View style={[styles.flexDirect, styles.alignSelfFlexEnd, { alignItems: 'center', padding: 10 }]}
                                    onStartShouldSetResponder={
                                        () => {
                                            this.showMenu()
                                            //    CommanUtils.showAlert("Under progress", "coming soon")
                                        }}   >
                                    <Image source={sortIcon} style={[styles.ImageIconStyle], { resizeMode: 'contain' }} />
                                    <Text style={{ padding: 3,fontFamily: 'SFUIText-Semibold', fontSize: diamens.ternarySize, color: blue }}>{StringsOfLanguages.sortCap}</Text>
                                </View>
                            } >
                            <MenuItem onPress={() => { this.hideMenu(1) }}>{StringsOfLanguages.rating}</MenuItem>
                            <MenuItem onPress={() => { this.hideMenu(2) }}>{StringsOfLanguages.priceLowToHigh}</MenuItem>
                            <MenuItem onPress={() => { this.hideMenu(3) }}>{StringsOfLanguages.priceHighToLow}</MenuItem>
                            <MenuItem onPress={() => { this.hideMenu(4) }}>{StringsOfLanguages.distance}</MenuItem>
                        </Menu>
                    </View>

                    {vendorQuotesList}
                </ImageBackground>
            </View>
        );
    }
}

