
import CardView from 'react-native-cardview'
import React, { Component } from 'react';
import { View, Image, Text, ImageBackground, ActivityIndicator, FlatList, Modal, TouchableOpacity, Platform, Share } from 'react-native';
import { FlatListSlider } from 'react-native-flatlist-slider';
import StringsOfLanguages from '../utils/StringsOfLanguages'
import CommanUtils from '../utils/CommanUtils';
import ApiClient from '../utils/ApiClientService';
import style from '../utils/style';
import CustToo from '../helper/CustomToolbarWelcome';
import CustomRow from '../list_row/RateReviewRow';
import colors from '../utils/colors';
import { ScrollView } from 'react-native-gesture-handler';
import Geocoder from 'react-native-geocoding';
import Geolocation from '@react-native-community/geolocation';
import diamens from '../utils/diamens';
import { Rating } from 'react-native-ratings';


var window_acImage = require('../../images/window_ac.png');
var rateUs = require('../../images/rate_white.png');
var shareApp = require('../../images/share_app.png');
var centralized_acImage = require('../../images/centralized_ac.png');
var splitImage = require('../../images/split_ac.png');
var homeIcon = require('../../images/home.png');
var header_bg = require('../../images/header_bg.png');
right_icon = require('../../images/arrow-right.png')
var iconMenu = require('../../images/menu.png');
var cross = require('../../images/cross.png');
var user = require('../../images/user_man.png');
var bgImage = require('../../images/main_bg.png');


export default class WelcomeScreen extends Component {



    constructor(props) {
        super(props)
        this.state = {
            languag: true,
            loginType: "user",
            bannerData: [],
            reviewData: [],
            acTypeId: 1,
            langValue: 'en',
            address: '',
            modalVisible: false,
            latitude: 0,
            longitude: 0,
            Authorization: '',
            comment: '-',
            name: '-',
            profileImage: '',
            rating: '0',
        }
        this.showDialogMod = this.showDialogMod.bind(this)
    }

    static navigationOptions = {
        title: StringsOfLanguages.welcome,
        drawerIcon: (
            <Image source={homeIcon} style={{ width: 20, height: 20, tintColor: '#4384FF', paddingEnd: 10 }}></Image>
        )
    };

    async componentDidMount() {
        var lan = await CommanUtils.getLanguage()
        var dataFromLocal = await CommanUtils.getLoginDetails();
        this.setState({ languag: lan, Authorization: "Bearer " + dataFromLocal.token });
        if (lan) { this.setState({ langValue: "en" }); } else { this.setState({ langValue: "ar" }); }
        const { navigation } = this.props;
        navigation.addListener('willFocus', () => {
            this.hitReviewList()
            this.hitBanner()
            this.getDataFromMap()
            ApiClient.getNotification(this.state);
        });

        this.hitBanner()
        this.hitReviewList()
        this.getAddress()
    }

    getDataFromMap() {
        try {
            var lat = this.props.navigation.getParam('latitude', 0);
            var lon = this.props.navigation.getParam('longitude', 0);
            var title = this.props.navigation.getParam('title', "no data");
            if (title != "no data") {
                this.setState({ address: title, latitude: lat, longitude: lon });
            }
        } catch (error) {

        }

        // console.log("from map",this.state)
    }

    async getAddress() {
        Geocoder.init(StringsOfLanguages.googleApiKey);
        Geolocation.getCurrentPosition(info => {
            this.setState({ latitude: info.coords.latitude, longitude: info.coords.longitude })
            Geocoder.from(info.coords.latitude, info.coords.longitude)
                .then(json => {
                    var addressComponent = json.results[0].formatted_address;
                    // console.log(json.results[0].address_components)
                    this.setState({ address: addressComponent })
                })
                .catch(error => console.warn(error));
        });
    }

    async hitBanner() {
        var isConnected = await CommanUtils.checkInternet()
        if (isConnected) {
            this.setState({ loading: true })
            var responceData = await ApiClient.getBanner(this.state);
            if (responceData != undefined) {
                this.setState({ bannerData: responceData.data })
            }
            this.setState({ loading: false })
        }
    };


    async hitReviewList() {

        // this.setState({ loading: true })
        var responceData = await ApiClient.getreviewList(this.state);
        if (responceData != undefined) {
            this.setState({ reviewData: responceData.reviewListing })
        }
        // this.setState({ loading: false })
    };

    navigateToServiceTypeScreen(value) {
        const { acTypeId, address, latitude, longitude } = this.state;
        this.props.navigation.navigate("ServiceRequestScreen", { acTypeId: value, address: address, latitude: latitude, longitude: longitude })
    }

    showDialogMod(item) {
        this.setState({ modalVisible: true, name: item.name, comment: item.comment, rating: item.rating, profileImage: item.profileImage })
    }

    navigateToRateApp() {
        this.props.navigation.navigate("RateUs")
    }

    render() {
        let styles;
        let bannerList = null;
        const { languag, loading, bannerData, address, modalVisible, latitude, longitude, comment, name, profileImage, rating } = this.state;

        if (languag) {
            styles = style.getStyle(true);
            StringsOfLanguages.setLanguage("en")
        } else {
            styles = style.getStyle(false);
            StringsOfLanguages.setLanguage("ar")
        }

        if (loading) {
            bannerList = (
                <ActivityIndicator
                    color={colors.black}
                    size="large"
                />
            );
        } else if (bannerData && bannerData.length) {
            bannerList = (
                <FlatListSlider
                    data={bannerData}
                    height={180}
                    timer={5000}
                    onPress={item => console.log(JSON.stringify(item))}
                    // contentContainerStyle={{paddingHorizontal: 16}}
                    indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
                    indicatorActiveColor={colors.colorPrimaryDark}
                    indicatorInActiveColor={'white'}
                    indicatorActiveWidth={30}
                    animation
                />
            );
        } else {
            <View></View>
        }
        return (
            <View style={{ flex: 1 }}>
                <CustToo valueFromParent={address} latitude={latitude} longitude={longitude} />

                <ImageBackground style={{ flex: 1 }} source={bgImage}>

                    <ScrollView nestedScrollEnabled>
                        <View>
                            {bannerList}

                            <Text style={[styles.homeBlueHead, { marginTop: 10, textAlign: 'center' }]}>{StringsOfLanguages.selectTheTypeOfAC}</Text>

                            <View style={styles.acTypeContainer}>

                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ acTypeId: 1 })
                                            this.navigateToServiceTypeScreen(1)
                                        }}>
                                        <CardView
                                            style={[styles.ac_card, { backgroundColor: '#fff', alignSelf: 'center' }]}
                                            cardElevation={2}
                                            cardMaxElevation={2}
                                            cornerRadius={5}>
                                            <Image source={window_acImage} style={styles.ac_image} />
                                            <Text style={{ fontFamily: 'SFUIText-Semibold', padding: 7, fontSize: 11 }}>{StringsOfLanguages.window}</Text>
                                        </CardView>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ acTypeId: 2 })
                                            this.navigateToServiceTypeScreen(2)
                                        }}>
                                        <CardView style={[styles.ac_card, { backgroundColor: '#fff', alignSelf: 'center' }]}
                                            cardElevation={2}
                                            cardMaxElevation={2}
                                            cornerRadius={5}>
                                            <Image source={splitImage} style={styles.ac_image} />
                                            <Text style={{ fontFamily: 'SFUIText-Semibold', padding: 7, fontSize: 11 }}>{StringsOfLanguages.split}</Text>
                                        </CardView>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1 }}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({ acTypeId: 3 })
                                            this.navigateToServiceTypeScreen(3)
                                        }}>
                                        <CardView style={[styles.ac_card, { backgroundColor: '#fff', alignSelf: 'center' }]}
                                            cardElevation={2}
                                            cardMaxElevation={2}
                                            cornerRadius={5}>
                                            <Image source={centralized_acImage} style={styles.ac_image} />
                                            <Text style={{ fontFamily: 'SFUIText-Semibold', padding: 7, fontSize: 11 }}>{StringsOfLanguages.centralized}</Text>
                                        </CardView>
                                    </TouchableOpacity>
                                </View>

                            </View>

                            <Text style={[styles.homeBlueHead, { textAlign: 'center' }]}>{StringsOfLanguages.customerReviews}</Text>

                            <FlatList
                                data={this.state.reviewData}
                                // keyExtractor={item => item.id.toString()}
                                renderItem={({ item }) => {
                                    return <CustomRow item={item} styles={styles} props={this.props} state={this.state} click={this.showDialogMod} />
                                }} />


                        </View>
                    </ScrollView>

                </ImageBackground>

                <Modal animationType="slide" transparent={true} visible={modalVisible}
                    onRequestClose={() => {
                        // Alert.alert("Modal has been closed.");
                        this.setState({ modalVisible: !modalVisible });
                    }}>
                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop: 22 }}>
                        <View style={{ margin: 20, backgroundColor: "white", borderRadius: 10, padding: 10, alignItems: "center", shadowColor: "#000", shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.25, shadowRadius: 4, elevation: 5 }}>

                            <View style={[styles.flexDirect, { padding: 10 }]}>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                    <TouchableOpacity onPress={() => {
                                        this.setState({ modalVisible: !modalVisible })
                                    }}>
                                        <Image source={cross} style={[styles.alignSelfFlexEnd, { height: 15, width: 15 }]} />
                                    </TouchableOpacity>
                                    <Image source={{ uri: profileImage }} style={[styles.alignSelf, { height: 50, width: 50, borderRadius: 25, marginTop: 10 }]} />
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Semibold', fontSize: diamens.secondrySize, marginTop: 10 }]}>{name}</Text>
                                    <View style={[styles.flexDirect, {}]}>
                                        <Rating type='custom' startingValue={rating} ratingCount={5} imageSize={15} ratingColor={colors.blue} ratingBackgroundColor={colors.greyVeryLight} style={[styles.alignSelf, { paddingTop: 5, paddingBottom: 5 }]} />
                                        <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Semibold', alignSelf: 'center', marginEnd: 5, marginStart: 5, fontSize: diamens.tinySize, color: colors.blue }]}> {rating}.0</Text>
                                    </View>
                                    <Text style={[styles.textAlignn, { fontFamily: 'SFUIText-Regular', fontSize: diamens.ternarySize, marginTop: 10 }]}>{comment}</Text>
                                </View>
                            </View>

                        </View>
                    </View>
                </Modal>
                <ImageBackground style={[styles.navBar, { justifyContent: 'flex-start', alignItems: 'flex-start' }]} source={header_bg}>

                    <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                        <View style={{ alignItems: 'center' }} onStartShouldSetResponder={() => this.navigateToRateApp()
                        }>
                            <Image source={rateUs} style={[styles.ImageIconStyle]} />
                            <Text style={{ fontFamily: 'SFUIText-Semibold', padding: 3, fontSize: diamens.tinySize, color: 'white' }}>{StringsOfLanguages.rateUs}</Text>
                        </View>
                    </View>

                    <View style={{ flex: 1, alignItems: 'center', padding: 5 }}>
                        <View style={{ alignItems: 'center' }} onStartShouldSetResponder={
                            () => {
                                onShare()
                            }
                        }>
                            <Image source={shareApp} style={[styles.ImageIconStyle], { resizeMode: 'contain' }} />
                            <Text style={{ fontFamily: 'SFUIText-Semibold', padding: 3, fontSize: diamens.tinySize, color: 'white' }}>{StringsOfLanguages.shareOurApp}</Text>
                        </View>
                    </View>

                </ImageBackground>
            </View>
        );
    }
}

const onShare = () => {

    let text = 'Please install this app '
    if (Platform.OS === 'android')
        text = text.concat('https://play.google.com/store/apps/details?id=com.newmyac&hl=en')
    else
        text = text.concat('https://apps.apple.com/us/app/myac-app/id1564790015')

    Share.share({
        subject: 'Download MyAc App Now',
        title: 'Download MyAc App Now',
        message: text,
        url: 'app://MyAc',

    }, {
        // Android only:
        dialogTitle: 'Share MyAc App',
        // iOS only:
        excludedActivityTypes: []
    })
}