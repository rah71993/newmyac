import URL from '../utils/Constraints';
import axios from 'axios';
import CommanUtils from '../utils/CommanUtils'

class ApiClientService {

  getLogin = (state) => {
    const { phoneCode, phoneNumber, latitude, longitude, loginTypeId, token, deviceType, langValue } = state;
    console.log("states....", state)
    return axios.post(
      URL.baseUrl + URL.LOGIN_USER,
      {
        "lang": langValue,
        "mobile_number": phoneNumber,
        "mobile_code": phoneCode,
        "loginType": loginTypeId,
        "latitude": latitude,
        "longitude": longitude,
        "deviceToken": token,
        "deviceType": deviceType
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        // alert("" + JSON.stringify(response.data))

        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  resendOTP = (state) => {
    const { id, langValue } = state;
    return axios.post(
      URL.baseUrl + URL.resendOTP,
      {
        "lang": langValue,
        "id": id,
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }

  verifyOTP = (state) => {
    const { id, otp, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.verifyOTP,
      {
        "lang": langValue,
        "otp": otp,
        "id": id,
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }



  getBanner = (state) => {
    // CommanUtils.checkInternet();
    return axios.get(
      URL.baseUrl + URL.banner,
      {
        // "otp": otp,
        // "id": id,
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }

  getServiceType = (state) => {
    const { langValue } = state;
    return axios.post(
      URL.baseUrl + URL.serviceType,
      {
        "lang": langValue,
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }

  getPageList = (state) => {
    const { langValue } = state;
    return axios.post(
      URL.baseUrl + URL.pageList,
      {
        "lang": langValue,
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }

  getPageDetails = (state) => {
    const { langValue, id } = state;
    return axios.post(
      URL.baseUrl + URL.pageData,
      {
        "lang": langValue,
        "pageId": id,
      },
      {
        headers: {
          //  'api-token': 'xyz',
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }



  requestService = (state) => {
    const { latitude, longitude, address, acType, serviceTypeId, message, Authorization, noOfAc, distanceValue, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.requestService,
      {
        "lang": langValue,
        "latitude": latitude,
        "longitude": longitude,
        "address": address,
        "acType": acType,
        "serviceType": serviceTypeId,
        "message": message,
        "noOfAc": noOfAc,
        "distanceRange": distanceValue,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  jobHistory = (state) => {
    const { langValue, Authorization, orderType } = state;
    console.log(state)
    return axios.post(
      URL.baseUrl + URL.jobHistory,
      {
        "lang": langValue,
        "jobType": orderType,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting job history data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  upcomingJobs = (state) => {
    const { langValue, Authorization } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.upcomingJobs,
      {
        "lang": langValue,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios upcoming', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }




  getVendorQuotes = (state) => {
    const { langValue, Authorization, serviceRequestId, shortBy } = state;
    console.log(state)
    return axios.post(
      URL.baseUrl + URL.vendorQuotes,
      {
        "lang": langValue,
        "serviceRequestId": serviceRequestId,
        "shortBy": shortBy,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting vendors data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  getMyOrder = (state) => {
    const { langValue, Authorization, jobType } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.myOrder,
      {
        "lang": langValue,
        "jobType": jobType,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting order data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  newOrder = (state) => {
    const { langValue, Authorization } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.newOrder,
      {
        "lang": langValue,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting new order data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }




  scheduleService = (state) => {
    const { vendorServiceId, dateDisplay, timeSlot, Authorization, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.scheduleService,
      {
        "lang": langValue,
        "vendorServiceId": vendorServiceId,
        "scheduleDate": dateDisplay,
        "scheduleTime": timeSlot,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }

  updateUserProfile = (state) => {
    const { name, email, mobile_number, mobile_code, addressData, profile_pic, Authorization, langValue } = state;

    var address = JSON.stringify(addressData)

    var data = new FormData();
    data.append("lang", langValue);
    data.append('name', name);
    data.append('email', email);
    data.append('mobile_number', mobile_number);
    data.append('profile_pic',
      {
        uri: profile_pic,
        name: 'userProfile.jpeg',
        type: 'image/jpeg'
      });
    data.append('mobile_code', mobile_code);
    data.append('address', address);

    console.log("Address -- == -- 00 ==",address)

    return axios.post(
      URL.baseUrl + URL.updateProfile,
      data,
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting data from axios with image', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log("eeee.....", error);
        return "error";
      });
  }




  updateVendorProfile = (state) => {
    const { name, email, mobile_number, mobile_code, companyName, crNumber, vatNumber, iqmah, city, stateProvisionRegion, zipPostalCode, attachDocuments, address, profile_pic, Authorization, langValue } = state;
    // console.log(state)
    var data = new FormData();
    data.append("lang", langValue);
    data.append('name', name);
    data.append('email', email);
    data.append('mobile_number', mobile_number);
    data.append('vendorName', companyName);
    data.append('crn', crNumber);
    data.append('vatNo', vatNumber);
    data.append('iqmah', iqmah);
    data.append('isUpdateVendorProfile', "true");
    data.append('profile_pic',
      {
        uri: profile_pic,
        name: 'userProfile.jpeg',
        type: 'image/jpeg'
      });
    data.append('mobile_code', mobile_code);
    data.append('address', address);
    data.append('city', city);
    data.append('state', stateProvisionRegion);
    data.append('zipcode', zipPostalCode);

    console.log("documents.......",attachDocuments)
    attachDocuments.forEach((element, i) => {
      const newFile = {
        name: element.name,
        uri: element.uri, 
        type: element.type
        
        // uri: element.path,
        // name: 'userProfile' + i + '.jpeg',
        // type: 'image/jpeg'
      }
      data.append('documents[]', newFile)
    
    });

    // console.log(" Request profile.......",data)
    return axios.post(
      URL.baseUrl + URL.updateProfile,
      data,
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting data from axios with image', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }





  submitPriceQuote = (state) => {
    const { vendorServiceId, quotedPrice, Authorization, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.submitPriceQuote,
      {
        "lang": langValue,
        "vendorServiceId": vendorServiceId,
        "priceQuote": quotedPrice,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios submit price quote', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }

  cancelJob = (vendorServiceId, Authorization, langValue) => {
    // console.log(vendorServiceId + "   " + Authorization)

    return axios.post(
      URL.baseUrl + URL.cancelJob,
      {
        "lang": langValue,
        "vendorServiceId": vendorServiceId,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting data from axios cancel jobs', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }






  generateInvoice = (state) => {
    const { Authorization, vendorServiceId, additionalCost, priceQuote, hours_Counter, minutes_Counter, isInvoiceSent, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.generateInvoice,
      {
        "vendorServiceId": vendorServiceId,
        "additionalPrice": additionalCost,
        "totalAmount": priceQuote,
        "totalHrs": hours_Counter,
        "totalMins": minutes_Counter,
        "isInvoiceSent": isInvoiceSent,
        "lang": langValue,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  orderSuccess = (state) => {
    const { Authorization, vendorServiceId, dataResponse, langValue } = state;

    const params = JSON.stringify({
      "lang": langValue,
      "vendorServiceId": vendorServiceId,
      "IsSuccess": true,
      "Data": dataResponse,
      "Message": "",
      "ValidationErrors": null,
    });
    // console.log(params)
    return axios.post(
      URL.baseUrl + URL.updateJobStatus,
      params,
      {
        headers: {
          "content-type": "application/json",
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }



  getreviewList = (state) => {
    const { Authorization, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.reviewList,
      {
        "lang": langValue,

      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting review data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  submitReview = (state) => {
    const { Authorization, rating, message, vendorId, langValue } = state;
    // console.log(state)
    return axios.post(
      URL.baseUrl + URL.addReview,
      {
        "lang": langValue,
        "vendor_id": vendorId,
        "rating": rating,
        "comment": message
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        // console.log('getting submit review data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }



  getNotification = (state) => {
    const { Authorization, loginType, langValue } = state;
    // console.log("notification...", state)
    return axios.post(
      URL.baseUrl + URL.notificationList,
      {
        "lang": langValue,
        "notificationFor": loginType
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then(async (responseJson) => {
        await CommanUtils.saveNotificatCount("" + responseJson.notificationCount)
        // console.log('getting notification data from axios', responseJson);
        return responseJson;
      })
      .catch(async (error) => {
        await CommanUtils.saveNotificatCount("")
        console.log(error);
        return "error";
      });
  }


  readNotification = (state, notificationId) => {
    const { Authorization, loginType, langValue } = state;
    console.log(state)
    return axios.post(
      URL.baseUrl + URL.readNotification,
      {
        "lang": langValue,
        "notificationFor": loginType,
        "notificationId": notificationId,
      },
      {
        headers: {
          'Accept': 'application/json',
          'Authorization': Authorization,
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        this.getNotification()
        // console.log('getting notification data from axios', responseJson);
        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }


  getFcmTokenFromApns = (state) => {
    const {apnsToken, phoneCode, phoneNumber, latitude, longitude, loginTypeId, token, deviceType, langValue } = state;
    console.log("states....", state)
    return axios.post(
      "https://iid.googleapis.com/iid/v1:batchImport",
      {
        "application": "com.app.myac",
        "sandbox":true,
        "apns_tokens":[apnsToken]
      },
      {
        headers: {
          'Authorization': "key=AAAAlZW9jwk:APA91bGkNJnVFFACRq80irO7-2xySxJAAtwLsqEqhl82ErFUQJhX469BeB6CYfwf5OacjThRD2bQ5cSN963BOVlLmkrLHeyfeZGdONfK6cuc7Dj-8ejXDOpj16AsMzHNdeOy741aZY_0",
          "Content-Type":"application/json"
        }
      },
    ).then((response) => response.data)
      .then((responseJson) => {
        console.log('getting data from axios', responseJson);
        // alert("" + JSON.stringify(response.data))

        return responseJson;
      })
      .catch(error => {
        console.log(error);
        return "error";
      });
  }



}

const c = new ApiClientService();
export default c;