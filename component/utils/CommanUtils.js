
import AsyncStorage from '@react-native-community/async-storage';
import NetworkUtils from './NetworkUtils';
import { Alert } from 'react-native';
import StringsOfLanguages from './StringsOfLanguages';
import RNRestart from 'react-native-restart';



class CommanUtils {


  checkInternet = async () => {
    var isConnected = await NetworkUtils.isNetworkAvailable();
    if (isConnected != true) {
      this.showAlert(StringsOfLanguages.noInternetConnection, StringsOfLanguages.youAreOfflineCheckInternet)
    }
    return isConnected;
  }

  setLanguage = (value) => {
    AsyncStorage.setItem('isEnglish', value);
  }


  getLanguage = async () => {
    try {
      let user = await AsyncStorage.getItem('isEnglish');
      if (user == 'ar') {
        return false;
      } else {
        return true;
      }
    }
    catch (error) {
      alert(error)
    }
  }






  showAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: StringsOfLanguages.ok,
          onPress: () => {
          }
        }
      ],
      { cancelable: false }
    );

  };

  showLogoutAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: StringsOfLanguages.cancel,
          onPress: () =>
            console.log('Cancel Pressed'),
        },
        {
          text: StringsOfLanguages.logout,
          onPress: () => {
            console.log('OK Pressed')
            this.restartApp();
            this.saveLoginStatus(false);
          }
        }
      ],
      { cancelable: false }
    );

  }


  showLanguageAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {
          text: StringsOfLanguages.english,
          onPress: () => {
            this.setLanguage("en");
            this.navigateMainDrawer();
          }
        },
        {
          text: StringsOfLanguages.arabic,
          onPress: () => {
            this.setLanguage("ar");
            this.navigateMainDrawer();
          }
        }
      ],
      { cancelable: true }
    );

  }

  saveLoginDetails(responceData) {
    var req = JSON.stringify(responceData);
    AsyncStorage.setItem('UserDetails', req);
  }

  getLoginDetails = async () => {
    try {
      let item = await AsyncStorage.getItem("UserDetails");
      var res = await JSON.parse(item);
      return res;
    }
    catch (error) {
      alert(error)
    }
  }



  saveLoginStatus(value) {
    var req = JSON.stringify(value);
    AsyncStorage.setItem('UserStatus', req);
  }

  getLoginStatus = async () => {
    try {
      let item = await AsyncStorage.getItem("UserStatus");
      var res = await JSON.parse(item);
      return res;
    }
    catch (error) {
      alert(error)
    }
  }


  saveLoginType(value) {
    AsyncStorage.setItem('UserType', value);
  }

  getLoginType = async () => {
    try {
      let item = await AsyncStorage.getItem("UserType");
      return item;
    }
    catch (error) {
      alert(error)
    }
  }
 
 
  saveToken(value) {
    AsyncStorage.setItem('token', value);
  }

  getToken = async () => {
    try {
      let item = await AsyncStorage.getItem("token");
      return item;
    }
    catch (error) {
      alert(error)
    }
  }
 
 
  saveNotificatCount(value) {
    AsyncStorage.setItem('notifCount', ""+value);
  }

  getNotificatCount = async () => {
    try {
      let item =  AsyncStorage.getItem("notifCount");
      return item;
    }
    catch (error) {
      alert(error)
    }
  }

  async restartApp() {
    await this.setIsRestartApp(true);
    RNRestart.Restart();
  }

  setIsRestartApp = (value) => {
    AsyncStorage.setItem('IsRestartApp', JSON.stringify(value));
  }

  isRestartApp = async () => {
    try {
      let user = await AsyncStorage.getItem('IsRestartApp');
      return JSON.parse(user);
    }
    catch (error) {
      alert(error)
    }
  }


  savePaymentStatus(value) {
    AsyncStorage.setItem('paymetStat', value);
  }

  getPaymentStatus = async () => {
    try {
      let item = await AsyncStorage.getItem("paymetStat");
      return item;
    }
    catch (error) {
      alert(error)
    }
  }




}



const c = new CommanUtils();
export default c;