// module.exports = {
//   static  baseUrl="https://jsonplaceholder.typicode.com/users"
// }

class Constraints {
    // static  baseUrl="https://jsonplaceholder.typicode.com/users";
    // static  baseUrl="https://alpinecirp.in/myac"+"/";
    // static  baseUrl="http://demo12.hol.es/myac"+"/";
    static  baseUrl="https://tescheveux.com/myac/";
    static  LOGIN_USER="api/login";
    static  resendOTP="api/resendOtp";
    static  verifyOTP="api/verifyOtp";

    //Here is key for local storage

    static  banner="api/get_banner";
    static  serviceType="api/service-type";
    static  pageList="api/pageList";
    static  pageData="api/pageData";
    static  requestService="api/requestService";
    static  jobHistory="api/jobHistory";
    static  upcomingJobs="api/upcomingJobs";
    static  vendorQuotes="api/vendorPriceQuote";
    static  myOrder="api/myOrder";
    static  newOrder="api/newOrder";
    static  scheduleService="api/scheduleService";
    static  updateProfile="api/updateProfile";
    static  submitPriceQuote="api/submitPriceQuote";
    static  cancelJob="api/cancelJob";
    static  generateInvoice="api/generateInvoice";
    static  updateJobStatus="api/updateJobStatus";
    static  reviewList="api/reviewList";
    static  addReview="api/addReview";
    static  notificationList="api/notificationList";
    static  readNotification="api/readNotification";


     static  baseUrlPaymentLive = "https://api.myfatoorah.com/";
     static  tokenPaymentLive = "fCzT0dwcXWVIPILGjKMekmea0IoC-gwvD-Pm3-CMWUMcnKIZCZfDJy8dk7WQrsZEVFdqTUBNKGWs0gAPT0e7jNPRBCrc-7lKyZ2HtttjOmXVYVBSwi5p9-3UQ7YRPabWujAk-iGNWqETXQnq9CC1i01hjz-otRNbT9M-Fl7kcvirLyf0wUWsrjyzf2DM0wCkTtBMvuLpHn-2CxPfDkHOuZ2w0ltrE0BdvQyB6cyc3BGQlat-PwvHL0-c4FXPRhkMbZCR1Ye91CBRNzEPm80fN82q2URUKKVE8bbhnBDN30Oqgmdctd2bntpR8jFRQwvXZF8wN3iwWYHARtHMfodYIChI0a4BqRQQbQLOs3mmv_EPEyj87ngHCZuzL52Fp-9gB5wwArmF1otgL0BKKbtUxP3HFzZ4EiuMngF8QVRe_hWHSI5jqWtfCB99hX1Hxc1rk924KF6tR-8S1BhVgKrYsF2owIY4_D8W_yQK2nGAwrhYX6xoFGNOaYpe8F8H6pp2PEWpanZ9WX8n-ih_StvxnI3dyGby6eOOS_Bw_D_Cr3MJHyQP0KWtyEuawDdRZoy6I11bZGTNCdzYOuATxqilTHthdsWxvxW93vfRetOJcMyTxnYAXfdB-yKjyIFgzAJ4veTKt1Uf-gEn6ZgeLkEof2_cbcpQzx2Fhh1GeA3iTfbBd988";

     static  demo_base_url = "https://apitest.myfatoorah.com";
     static  demo_token = "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL";

}

export default Constraints;