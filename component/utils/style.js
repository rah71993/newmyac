import { StyleSheet, Platform, StatusBar } from 'react-native';
import { color } from 'react-native-reanimated';
import dimens from '../utils/diamens';
import colors, { green } from '../utils/colors';
import diamens from '../utils/diamens';

export default class Style {
    static getStyle(isEnglish) {
        return StyleSheet.create({

            container: {
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            },
            status_styl: {
                height: (Platform.OS === 'ios') ? 40 : 0, //this is just to test if the platform is iOS to give it a height of 18, else, no height (Android apps have their own status bar)
                backgroundColor: "#4384FF",

            },
            row_container: {
                flex: 1,
                padding: 10,
                marginLeft: 16,
                marginRight: 16,
                marginTop: 8,
                marginBottom: 8,
                borderRadius: 5,
                backgroundColor: '#FFF',
                elevation: 2,
            },

            logo: {
                fontWeight: "bold",
                fontSize: 50,
                color: "#fb5b5a",
            },
            logo_style:
            {
                height: 90,
                width: 90,
                alignItems: 'center',
                justifyContent: 'center'
            },
            ImageIconStyle:
            {
                height: 20,
                width: 20,
                alignItems: 'center',
                justifyContent: 'center'
            },
            wordNormal: {
                textAlign: "center",
                color: colors.blue,
                fontSize: dimens.ternarySize,
                fontFamily: 'SFUIText-Semibold',
            },

            btnPress: {
                width: "80%",
                flexDirection: isEnglish ? 'row' : 'row-reverse',
                backgroundColor: colors.colorPrimaryDark,
                borderRadius: 25,
                height: 50,
                alignItems: "center",
                justifyContent: "center",
            },
            btnPressBottom: {
                flexDirection: 'row',
                backgroundColor: "#4384FF",
                borderRadius: 25,
                height: 50,
                alignItems: "center",
                justifyContent: "center",
                position: 'absolute',
                bottom: 0,
                marginBottom: 15,
                alignSelf: 'center',
                width: '92%'
            },
            btnPressSmall: {
                backgroundColor: "#4384FF",
                borderRadius: 25,
                height: 30,
                alignItems: "center",
                justifyContent: "center",
            },
            scheduleLeft: {
                backgroundColor: green,
                borderBottomEndRadius: 25,
                borderTopEndRadius: 25,
                height: 30,
                alignItems: "center",
                justifyContent: "center",
                marginStart: -10,
                marginBottom: 15,
                marginTop: 5,
                paddingStart: 15,
                paddingEnd: 15,
                alignSelf: 'flex-start'
            },
            scheduleRight: {
                backgroundColor: green,
                borderBottomStartRadius: 25,
                borderTopStartRadius: 25,
                height: 30,
                alignItems: "center",
                justifyContent: "center",
                marginEnd: -10,
                marginBottom: 15,
                marginTop: 5,
                paddingStart: 15,
                paddingEnd: 15,
                alignSelf: 'flex-end'
            },

            textButtonPress:
            {
                color: "white",
                fontSize: dimens.ternarySize,
                fontFamily: 'SFUIText-Bold',
            },
            btnBorder: {
                width: "80%",
                flexDirection: isEnglish ? 'row' : "row-reverse",
                borderRadius: 25,
                borderWidth: 2,
                borderColor: "#4384FF",
                height: 50,
                alignItems: "center",
                justifyContent: "center",
                marginTop: 20
            },
            inputView: {
                width: "80%",
                backgroundColor: "#ffffff",
                borderRadius: 10,
                flexDirection: isEnglish ? 'row' : "row-reverse",
                height: 50,
                alignItems: "center",
                paddingStart: 20,
                paddingEnd: 20,
            },
            inputTxt: {
                height: 50,
                flex: 1,
                marginStart: 15,
                marginEnd: 15,
                textAlign: isEnglish ? 'left' : 'right',
            },
            textUnderline: {
                textAlign: "center",
                color: colors.colorPrimaryDark,
                fontSize: diamens.tinySize,
                textDecorationLine: 'underline',
                fontFamily: 'SFUIText-Bold',
            },

            roundedTextInput: {
                borderRadius: 10,
                // borderWidth: 4,
                elevation: 10,
                backgroundColor: 'white'
            },

            //Toolbar 

            navBar: {
                height: 54,
                flexDirection: isEnglish ? 'row' : 'row-reverse',
                justifyContent: 'space-between',
                alignItems: 'center',
                borderBottomWidth: 0,
                backgroundColor: '#4384FF',
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 4,
                elevation: 1,
            },
            leftContainer: {
                justifyContent: 'flex-start',
                flexDirection: 'row'
            },
            middleContainer: {
                flex: 1,
                flexDirection: 'row',
                fontSize: 18,
                marginLeft: 10,
                marginRight: 10,
                textAlign: isEnglish ? 'left' : 'right',
            },
            rightContainer: {
                paddingHorizontal: 20,
                flexDirection: isEnglish ? 'row' : 'row-reverse',
                justifyContent: 'center',
                alignItems: 'center',
            },
            rightIcon: {
                resizeMode: 'contain',
            },
            // Nav Header
            profileImg: {
                width: 90,
                height: 90,
                borderRadius: 45,

            },
            //Home Screen 
            transparentImageBottomRadious: {
                width: '100%',
                opacity: 0.5,
                backgroundColor: '#4384FF',
                borderBottomLeftRadius: 40,
                borderBottomRightRadius: 40,

            },
            //profile image background

            profileImgBackground: {
                height: 100,
                width: 100,
                borderRadius: 100 / 2,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center'
            },
            //border Gray welcome Screen
            ac_card: {
                height: 100,
                width: 100,
                margin: 2,
                padding: 10,
                borderRadius: 5,
                alignItems: 'center',
                justifyContent: 'center'
            },
            ac_image: {
                height: 40,
                width: 40,
                resizeMode: 'center'
            },
            //enable disable tab
            enableTab: {
                flex: 1,
                justifyContent: 'center',
                backgroundColor: '#4384FF',

            },
            disableTab: {
                flex: 1,
                justifyContent: 'center',
                backgroundColor: '#fff',

            },
            enableText: {
                fontFamily: 'SFUIText-Bold',
                alignSelf: 'center',
                color: '#fff'
            },
            disableText: {
                fontFamily: 'SFUIText-Bold',
                alignSelf: 'center',
                color: '#4384FF'
            },
            homeBlueHead: {
                paddingStart: 20,
                paddingEnd: 20,
                color: '#2363DB',
                fontSize: dimens.secondrySize,
                textAlign: isEnglish ? 'left' : 'right',
                fontFamily: 'SFUIText-Semibold',

            },
            acTypeContainer: {
                width: '100%',
                padding: 15,
                flexDirection: isEnglish ? 'row' : 'row-reverse',
            },
            flexDirect: {
                flexDirection: isEnglish ? 'row' : 'row-reverse',
            },
            textAlignn: {
                textAlign: isEnglish ? 'left' : 'right',
            },
            textAlignnEnd: {
                textAlign: isEnglish ? 'right' : 'left',
            },
            alignSelf: {
                alignSelf: isEnglish ? 'flex-start' : 'flex-end',
            },
            alignSelfFlexEnd: {
                alignSelf: isEnglish ? 'flex-end' : 'flex-start',
            },
            notifBagEnglish: {
                alignSelf: 'center', marginBottom: 15, marginLeft: -6, alignItems: 'center', justifyContent: 'center', height: 10, width: 10, borderRadius: 5, backgroundColor: colors.red
            },
            notifBagArabic: {
                alignSelf: 'center', marginBottom: 15, marginRight: -6, alignItems: 'center', justifyContent: 'center', height: 10, width: 10, borderRadius: 5, backgroundColor: colors.red
            },
            drawerItemStyle: {
                fontFamily: 'SFUIText-Semibold', marginLeft: 0, marginRight: 0
            },
            smallTextStyle: {
                fontFamily: 'SFUIText-Bold',
                textAlign: isEnglish ? 'left' : 'right',
                color: colors.smallTextColor,
            },
            container: {
                flex: 1,
                // justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#F5FCFF',
                padding: 0,
            },
            welcome: {
                fontSize: 20,
                textAlign: 'center',
                margin: 10,
            },
            instructions: {
                alignSelf: 'stretch',
                textAlign: 'left',
                color: '#333333',
                marginBottom: 5,
            },
            loading: {
                margin: 10,
                opacity: 1,
                backgroundColor: null,
            },
            flatList: {
                height: 110,
                flexGrow: 0,
            },
            disabledButtonStyle: {
                marginRight: 40,
                marginLeft: 40,
                marginTop: 10,
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: 'lightgray',
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff'
            },
            buttonStyle: {
                marginRight: 40,
                marginLeft: 40,
                marginTop: 10,
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: '#1E6738',
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#fff'
            },
            buttonText: {
                color: '#fff',
                textAlign: 'center',
                paddingLeft: 10,
                paddingRight: 10
            },
            textInputStyle: {
                borderColor: 'lightgray', borderBottomWidth: 1, borderRadius: 5
            },
            normalTextInputStyle: {
                borderColor: 'lightgray', borderWidth: 1, borderRadius: 5
            }
        });
       

    }
}